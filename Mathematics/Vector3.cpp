/* 
 * File:   Vector3.cpp
 * Author: Kevin
 * 
 * Created on 14 December 2013, 17:13
 */

#include <cmath>
#include "Mathematics/Vector3.h"
#include "Mathematics/Float.h"
#include "Funtions.h"
#include <ostream>

using namespace Candela::Mathematics;

//default constructor
template<class T>
Vector3<T>::Vector3() 
        : xyz() //will zero initialise all elements - Tested
{
}

//constructor
template<class T>
Vector3<T>::Vector3(T p_xyz) 
        : x ( p_xyz ), y ( p_xyz ), z ( p_xyz )
{}

//other constructor
template<class T>
Vector3<T>::Vector3(T p_x, T p_y, T p_z) 
        : x ( p_x ), y ( p_y ), z ( p_z )
{}

//copy constructor - makes a copy, no shit.
template<class T>
Vector3<T>::Vector3(const Vector3& orig) 
        : x ( orig.x ), y ( orig.y ), z ( orig.z )
{}

//UNARY

template<class T>
Vector3<T> Vector3<T>::operator -() const
{
    return Vector3(-x, -y, -z);
}

//magnitude of a vector
template<class T>
T Vector3<T>::operator ! () const{
    return sqrt(x * x + y * y + z * z);
}

//normalised vector
template<class T>
Vector3<T> Vector3<T>::operator ~ () const{
    T invMag = 1 / !(*this); //get inverse, multiplication is faster
    return Vector3(x * invMag, y * invMag, z * invMag);
}

template<class T>
bool Vector3<T>::isNotSet() const
{
    return (x == 0) && (y == 0) && (z == 0);
}

//returns the angle
template<class T>
T Vector3<T>::angle(const Vector3 &v2) const{
    return acos((*this * v2) / (!*this * !v2));
}

//returns the cosine of the angle
template<class T>
T Vector3<T>::cosAngle(const Vector3 &v2) const{
    return (*this * v2) / (!*this * !v2);
}

//binary operators
template<class T>
Vector3<T> Vector3<T>::operator +(const Vector3& v) const
{
    return Vector3 (x + v.x, y + v.y, z + v.z);
}

template<class T>
Vector3<T> Vector3<T>::operator - (const Vector3 &v) const{
    return Vector3 (x - v.x, y - v.y, z - v.z);
}

//cross-product which will return n^ vector
template<class T>
Vector3<T> Vector3<T>::operator ^ (const Vector3 &v) const{
    return Vector3 (y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x); //return non-dynamically constructed object
}

//Hadamard product
template<class T>
Vector3<T> Vector3<T>::hadamard (const Vector3 &v) const{
    return Vector3 (x * v.x, y * v.y, z * v.z);
}

//mag product
template<class T>
Vector3<T> Vector3<T>::operator * (T f) const{
    return Vector3 (x * f, y * f, z * f);
}

//mag product -> 1 /  f
template<class T>
Vector3<T> Vector3<T>::operator / (T f) const{
    f = 1 / f;
    return Vector3 (x * f, y * f, z * f);
}

//dot product
template<class T>
T Vector3<T>::operator * (const Vector3 &v) const{
    return x * v.x + y * v.y + z * v.z;
}

template<class T>
bool Vector3<T>::operator ==(const Vector3& p_vector) const
{
    return Float::almostEqualUlps(xyz[0], p_vector.xyz[0], 1) &&
           Float::almostEqualUlps(xyz[1], p_vector.xyz[1], 1) &&
           Float::almostEqualUlps(xyz[2], p_vector.xyz[2], 1);
}

template<class T>
Vector3<T> Vector3<T>::min(const Vector3 & a, const Vector3 & b) {
    Vector3 result;
    result.x = Functions::min(a.x, b.x);
    result.y = Functions::min(a.y, b.y);
    result.z = Functions::min(a.z, b.z);
    return result;
}

template<class T>
Vector3<T> Vector3<T>::max(const Vector3 & a, const Vector3 & b) {
    Vector3 result;
    result.x = Functions::max(a.x, b.x);
    result.y = Functions::max(a.y, b.y);
    result.z = Functions::max(a.z, b.z);
    return result;
}

//self-binary operator
template<class T>
Vector3<T>& Vector3<T>::operator += (const Vector3 &v){
    x += v.x;
    y += v.y;
    z += v.z;
    return *this; // return reference to this object
}

template<class T>
Vector3<T>& Vector3<T>::operator -= (const Vector3 &v){
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this; // return reference to this object
}

//self cross-product which will return n^ vector
template<class T>
Vector3<T>& Vector3<T>::operator ^= (const Vector3 &v) {
    const float aY = z * v.x - x * v.z;
    const float aZ = x * v.y - y * v.x;
    x = y * v.z - z * v.y;
    y = aY;
    z = aZ;
    return *this;
}

template<class T>
Vector3<T>& Vector3<T>::setHadamard (const Vector3 &v) {
    x *= v.x;
    y *= v.y;
    z *= v.z;
    return *this;
}

template<class T>
Vector3<T>& Vector3<T>::operator *= (T f){
    x *= f;
    y *= f;
    z *= f;
    return *this; // return reference to this object
}

template<class T>
Vector3<T>& Vector3<T>::operator /= (T f){
    f = 1 / f;
    x *= f;
    y *= f;
    z *= f;
    return *this; // return reference to this object
}

template <class T>
Vector3<T> &Vector3<T>::min(const Vector3 & a) {
    x = Functions::min(x, a.x);
    y = Functions::min(y, a.y);
    z = Functions::min(z, a.z);
    return *this;
}

template <class T>
Vector3<T> &Vector3<T>::max(const Vector3 & a) {
    x = Functions::max(x, a.x);
    y = Functions::max(y, a.y);
    z = Functions::max(z, a.z);
    return *this;
}

//friend definition
template<class T>
Vector3<T> Candela::Mathematics::operator* (T f, const Vector3<T> &v){
    return Vector3<T>(f * v.x, f * v.y, f * v.z);
}

template<class T>
std::ostream& Candela::Mathematics::operator<<(std::ostream& strm, const Vector3<T>& v) {
  return strm << "Vector - Address = " << &v << ", (x,y,z) = (" << v.x << ", " << v.y <<
          ", " << v.z << ")";
}

//static variables
template<class T>
const Vector3<T> Vector3<T>::UnitX = Vector3<T>(1, 0, 0);
template<class T>
const Vector3<T> Vector3<T>::UnitY = Vector3<T>(0, 1, 0);
template<class T>
const Vector3<T> Vector3<T>::UnitZ = Vector3<T>(0, 0, 1);

//initialise as float
template class Vector3<float>;
template Vector3<float> Candela::Mathematics::operator*<float> (float f, const Vector3<float> &v);
template std::ostream& Candela::Mathematics::operator<< <float> (std::ostream&, const Vector3<float>&);


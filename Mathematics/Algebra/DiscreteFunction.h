/* 
 * File:   DiscreteFunction.h
 * Author: Kevin
 *
 * Created on 15 June 2014, 20:23
 */

#ifndef DISCRETEFUNCTION_H
#define	DISCRETEFUNCTION_H

#include "IFunction.h"
#include "Mathematics/Vector2.h"
#include <vector>

namespace Candela
{
    namespace Mathematics
    {
        namespace Algebra
        {
            class DiscreteFunction
                : public IFunction<float>
            {
            public:
                DiscreteFunction();
                DiscreteFunction(std::vector<Vector2<float> > & data);

                void addPoint(float x, float y);
                
                virtual float operator ()(float x) const;
                
                virtual std::string toString() const;

                ~DiscreteFunction();
            private:
                
                //returns the index for which x is immediately greater than or equal to
                unsigned binarySearch(float x) const;
                
                std::vector<Vector2<float> > data_;
            };
        }
    }
}

#endif	/* DISCRETEFUNCTION_H */


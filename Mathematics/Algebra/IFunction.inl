#include "Mathematics/Algebra/IFunction.h"

using namespace Candela::Mathematics::Algebra;

template<class T>
IFunction<T>::~IFunction() {

}

//make final in c++11
template<class T>
std::ostream& Candela::Mathematics::Algebra::operator<<(std::ostream& stream, const IFunction<T>& fn) {
    return stream << fn.toString();
}

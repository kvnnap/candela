/* 
 * File:   DiscreteFunction.cpp
 * Author: Kevin
 * 
 * Created on 15 June 2014, 20:23
 */

#include "Mathematics/Algebra/DiscreteFunction.h"

using namespace Candela::Mathematics;
using namespace Candela::Mathematics::Algebra;

DiscreteFunction::DiscreteFunction() {
}

DiscreteFunction::~DiscreteFunction() {
}

DiscreteFunction::DiscreteFunction(std::vector<Vector2<float> >& data)
    : data_ ( data )
{
}

//adapt for sorted
void DiscreteFunction::addPoint(float x, float y) {
    data_.insert(data_.begin() + (binarySearch(x) + 1), Vector2<float>(x, y));
}

unsigned DiscreteFunction::binarySearch(float x) const {
    if(data_.size() >= 2)
    {
        int min = 0, max = data_.size() - 1;
        
        //exterior bounds - take advantage of sorted additions
        if(x >= data_[max].x){
            return max;
        }else if(x <= data_[min].x)
        {
            return -1;
        }
        
        //binary search
        while(max - min != 1)
        {
            int mid = (max + min) / 2;
            if(x < data_[mid].x)
            {
                max = mid;
            }
            else if(x > data_[mid].x)
            {
                min = mid;
            }else{
                return mid;
            }
        }
        
        //x must be within data_[min].x and data_[max].x
        return min;
    }else
    {
        return data_.size() == 1 ? x <= data_[0].x ? -1 : 0 : -1;
    }
}


//uses binary search, assumes sorted
float DiscreteFunction::operator ()(float x) const
{   
    if(data_.size() == 0)
    {
        return 0.f;
    }else if (data_.size() == 1)
    {
        return data_[0].y;
    }else
    {
        int index = binarySearch(x);
        int max = index + 1;
        return data_[index].y + (x - data_[index].x) * ((data_[max].y - data_[index].y)
               / (data_[max].x - data_[index].x));
    }
}

std::string DiscreteFunction::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "DiscreteFunction - size " << data_.size() << endl;
    for(unsigned i = 0; i < data_.size(); ++i)
    {
        sstr << data_[i] << endl;
    }
    return sstr.str();
}

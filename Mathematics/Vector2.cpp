/* 
 * File:   Vector2.cpp
 * Author: Kevin
 * 
 * Created on 25 April 2014, 21:48
 */

#include <cmath>
#include "Mathematics/Float.h"
#include <ostream>
#include "Mathematics/Vector2.h"

using namespace Candela::Mathematics;

template<class T>
Vector2<T>::Vector2()
        : xy()
{}

template<class T>
Vector2<T>::Vector2(T xy) 
        : x ( xy ), y ( xy )
{}

template<class T>
Vector2<T>::Vector2(T p_x, T p_y)
        : x ( p_x ), y ( p_y )
{
}

template<class T>
Vector2<T> Vector2<T>::operator -() const {
    return Vector2(-x, -y);
}

template<class T>
T Vector2<T>::operator !() const {
    return sqrt(x * x + y * y);
}

template<class T>
Vector2<T> Vector2<T>::operator ~() const {
    T invMag = 1 / !(*this); //get inverse, multiplication is faster
    return Vector2(x * invMag, y * invMag);
}


template<class T>
bool Vector2<T>::isNotSet() const
{
    return (x == 0) && (y == 0);
}

//returns the angle
template<class T>
T Vector2<T>::angle(const Vector2 &v2) const{
    return acos((*this * v2) / (!*this * !v2));
}

//returns the cosine of the angle
template<class T>
T Vector2<T>::cosAngle(const Vector2 &v2) const{
    return (*this * v2) / (!*this * !v2);
}

//binary operators
template<class T>
Vector2<T> Vector2<T>::operator +(const Vector2& v) const
{
    return Vector2 (x + v.x, y + v.y);
}

template<class T>
Vector2<T> Vector2<T>::operator - (const Vector2 &v) const{
    return Vector2 (x - v.x, y - v.y);
}

//cross-product which will return n^ vector
template<class T>
T Vector2<T>::operator ^ (const Vector2 &v) const{
    return x * v.y - y * v.x; //return Vector3<T>(0, 0, x * v.y - y * v.x);
}

//Hadamard product
template<class T>
Vector2<T> Vector2<T>::hadamard (const Vector2 &v) const{
    return Vector2 (x * v.x, y * v.y);
}

//mag product
template<class T>
Vector2<T> Vector2<T>::operator * (T f) const{
    return Vector2 (x * f, y * f);
}

//mag product -> 1 /  f
template<class T>
Vector2<T> Vector2<T>::operator / (T f) const{
    f = 1 / f;
    return Vector2 (x * f, y * f);
}

//dot product
template<class T>
T Vector2<T>::operator * (const Vector2 &v) const{
    return x * v.x + y * v.y;
}

template<class T>
bool Vector2<T>::operator ==(const Vector2& p_vector) const
{
    return Float::almostEqualUlps(xy[0], p_vector.xy[0], 1) &&
           Float::almostEqualUlps(xy[1], p_vector.xy[1], 1);
}

//self-binary operator
template<class T>
Vector2<T>& Vector2<T>::operator += (const Vector2 &v){
    x += v.x;
    y += v.y;
    return *this; // return reference to this object
}

template<class T>
Vector2<T>& Vector2<T>::operator -= (const Vector2 &v){
    x -= v.x;
    y -= v.y;
    return *this; // return reference to this object
}

template<class T>
Vector2<T>& Vector2<T>::setHadamard (const Vector2 &v) {
    x *= v.x;
    y *= v.y;
    return *this;
}

template<class T>
Vector2<T>& Vector2<T>::operator *= (T f){
    x *= f;
    y *= f;
    return *this; // return reference to this object
}

template<class T>
Vector2<T>& Vector2<T>::operator /= (T f){
    f = 1 / f;
    x *= f;
    y *= f;
    return *this; // return reference to this object
}

//friend definition
template<class T>
Vector2<T> Candela::Mathematics::operator* (T f, const Vector2<T> &v){
    return Vector2<T>(f * v.x, f * v.y);
}

template<class T>
std::ostream& Candela::Mathematics::operator<<(std::ostream& strm, const Vector2<T>& v) {
  return strm << "Vector2 - Address = " << &v << ", (x,y) = (" << v.x << ", " << v.y << ")";
}

//static variables
template<class T>
const Vector2<T> Vector2<T>::UnitX = Vector2<T>(1, 0);
template<class T>
const Vector2<T> Vector2<T>::UnitY = Vector2<T>(0, 1);

//initialise as float
template class Vector2<float>;
template Vector2<float> Candela::Mathematics::operator*<float> (float f, const Vector2<float> &v);
template std::ostream& Candela::Mathematics::operator<< <float> (std::ostream&, const Vector2<float>&);
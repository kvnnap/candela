/* 
 * File:   BoundedRay.h
 * Author: Kevin
 *
 * Created on 28 December 2013, 15:34
 */

#ifndef BOUNDEDRAY_H
#define	BOUNDEDRAY_H

#include "Ray.h"

namespace Candela
{
    namespace Mathematics
    {
        class BoundedRay 
                : public Ray
        {
        public:
            BoundedRay();
            BoundedRay(const Vector& position, const Vector& direction);
            BoundedRay(const Vector& position, const Vector& direction, float tMax);
            BoundedRay(const Vector& position, const Vector& direction, float tMin,
            float tMax);
            
            float getMin() const;
            float getMax() const;
            
            Vector getMinPoint() const;
            Vector getMaxPoint() const;
            
            void setMin(float tMin);
            void setMax(float tMax);

            // Only set boundaries of ray if boundaries are not crossed
            void setMinSafe(float tMin);
            void setMaxSafe(float tMax);

            void setMinAndAdvanceByEpsilon(float tMin, float factor = 1000.f);
            void advanceMinByEpsilon(float factor = 1000.f);
            
            std::string toString() const;
        private:
            /* Might put these in bounded ray subclass but probably not worth it*/
            float tMin_, tMax_;
            //Think if it is worth it to cache MinPoint and MaxPoint here..
        };
        
        std::ostream& operator<< (std::ostream& strm, const BoundedRay& ray);
    }
}



#endif	/* BOUNDEDRAY_H */


/* 
 * File:   TMatrix.cpp
 * Author: Kevin
 * 
 * Created on 19 April 2014, 13:30
 */

#include "Mathematics/TMatrix.h"
#include "Mathematics/Funtions.h"
#include <stdexcept>

using namespace Candela::Mathematics;

template<class T, unsigned R, unsigned C>
TMatrix<T, R, C>::TMatrix()
        : data_ () //will zero initialise it
{
}

template<class T, unsigned R, unsigned C>
unsigned char TMatrix<T, R, C>::getRows() const {
    return R;
}

template<class T, unsigned R, unsigned C>
unsigned char TMatrix<T, R, C>::getColumns() const {
    return C;
}

template<class T, unsigned R, unsigned C>
bool TMatrix<T, R, C>::isSquare() const {
    return R == C;
}

template<class T, unsigned R, unsigned C>
TMatrix<T, R, C> TMatrix<T, R, C>::getIdentity() const {
    if(!isSquare()) throw std::runtime_error("This is not a Square Matrix");
    TMatrix tempMatrix;
    for(unsigned i = 0; i < R; ++i)
    {
        tempMatrix(i, i) = (T)1;
    }
    return tempMatrix;
}

template<class T, unsigned R, unsigned C>
TMatrix<T, 4, 4> TMatrix<T, R, C>::getRotationX(T theta) {
    TMatrix<T, 4, 4> temp;
    T compCos = Functions::cos(theta);
    T compSin = Functions::sin(theta);
    temp (0,0) = 1;
    temp (1,1) = compCos;
    temp (1,2) = -compSin;
    temp (2,1) = compSin;
    temp (2,2) = compCos;
    temp (3,3) = 1; //if 4x4
    return temp;
}

template<class T, unsigned R, unsigned C>
TMatrix<T, 4, 4> TMatrix<T, R, C>::getRotationY(float theta) {
    TMatrix<T, 4, 4> temp;
    T compCos = Functions::cos(theta);
    T compSin = Functions::sin(theta);
    temp (0,0) = compCos;
    temp (0,2) = compSin;
    temp (1,1) = 1;
    temp (2,0) = -compSin;
    temp (2,2) = compCos;
    temp (3,3) = 1; //if 4x4
    return temp;
}

template<class T, unsigned R, unsigned C>
TMatrix<T, 4, 4> TMatrix<T, R, C>::getRotationZ(float theta) {
    TMatrix<T, 4, 4> temp;
    T compCos = Functions::cos(theta);
    T compSin = Functions::sin(theta);
    temp (0,0) = compCos;
    temp (0,1) = -compSin;
    temp (1,0) = compSin;
    temp (1,1) = compCos;
    temp (2,2) = 1;
    temp (3,3) = 1; //if 4x4
    return temp;
}

template<class T, unsigned R, unsigned C>
TMatrix<T, 4, 4> TMatrix<T, R, C>::getScaling(const Vector& scale) {
    TMatrix<T, 4, 4> temp;
    temp(0,0) = scale.xyz[0];
    temp(1,1) = scale.xyz[1];
    temp(2,2) = scale.xyz[2];
    temp(3,3) = 1;
    return temp;
}

template<class T, unsigned R, unsigned C>
TMatrix<T, 4, 4> TMatrix<T, R, C>::getTranslation(const Vector& translation) {
    TMatrix<T, 4, 4> temp;
    temp(0,0) = 1;
    temp(0,3) = translation.xyz[0];
    temp(1,1) = 1;
    temp(1,3) = translation.xyz[1];
    temp(2,2) = 1;
    temp(2,3) = translation.xyz[2];
    temp(3,3) = 1;
    return temp;
}

template<class T, unsigned R, unsigned C>
template<unsigned R2, unsigned C2, class T2>
TMatrix<typename conditional<sizeof(T) >= sizeof(T2), T, T2>::type, R, C2> TMatrix<T, R, C>::operator *(const TMatrix<T2, R2, C2>& matrix) const {
    if(C == R2)
    {   
        TMatrix<typename conditional<sizeof(T) >= sizeof(T2), T, T2>::type, R, C2> tempMatrix;
        for(unsigned i = 0; i < R; i++)
        {
            for(unsigned j = 0; j < C2; j++)
            {
                for(unsigned k = 0; k < C; k++)
                {
                    tempMatrix(i, j) += data_[i][k] * matrix(k,j);
                }
            }
        }
        return tempMatrix;
    }else{
        throw std::runtime_error("Invalid Matrix Multiplication");
    }
}

template<class T, unsigned R, unsigned C>
Vector TMatrix<T, R, C>::operator *(const Vector& vector) const {
    if(C == 3 || C == 4)
    {   //multiplication is defined and valid
        TMatrix<T, R, 1> tempMatrix;
        TMatrix<T, C, 1> matrix;
        
        matrix(0, 0) = vector.xyz[0];
        matrix(1, 0) = vector.xyz[1];
        matrix(2, 0) = vector.xyz[2];
        if(C == 4) { matrix(3, 0) = 1; }
        //float (*cell)[tempMatrix.columns_] = (float (*) [tempMatrix.columns_]) tempMatrix.data_;
        for(unsigned i = 0; i < R; i++)
        {
            for(unsigned k = 0; k < C; k++)
            {
                tempMatrix(i, 0) += data_[i][k] * matrix(k, 0);
            }
        }
        return Vector((tempMatrix(0, 0), tempMatrix(1, 0), tempMatrix(2, 0)));
    }else
    {
        return Vector();
    }
}

template<class T, unsigned R, unsigned C>
TMatrix<T, R, C> TMatrix<T, R, C>::operator *(float scalar) const {
    TMatrix<T, R, C> tempMatrix = *this;
    for(unsigned i = 0; i < R; i++)
    {
        for(unsigned j = 0; j < C; j++)
        {
            tempMatrix(i, j) *= scalar;
        }
    }
    return tempMatrix;
}

template<class T, unsigned R, unsigned C>
TMatrix<T, R, C> TMatrix<T, R, C>::operator +(const TMatrix& matrix) const {
    TMatrix tempMatrix;
    for(unsigned i = 0; i < R; i++)
    {
        for(unsigned j = 0; j < C; j++)
        {
            tempMatrix(i, j) = data_[i][j] + matrix(i, j);
        }
    }
    return tempMatrix;
}


template<class T, unsigned R, unsigned C>
TMatrix<T, R, C> TMatrix<T, R, C>::operator -(const TMatrix& matrix) const {
    TMatrix tempMatrix;
    for(unsigned i = 0; i < R; i++)
    {
        for(unsigned j = 0; j < C; j++)
        {
            tempMatrix(i, j) = data_[i][j] - matrix(i, j);
        }
    }
    return tempMatrix;
}

template<class T, unsigned R, unsigned C>
template<unsigned R2, unsigned C2>
const TMatrix<T, R, C>& TMatrix<T, R, C>::operator *=(const TMatrix<T, R2, C2>& matrix) {
    //(rows_, matrix.columns_);
    if(C == R2 && C == C2)
    {   //multiplication is defined and valid
        TMatrix tempMatrix = *this;
        //float (*cell)[tempMatrix.columns_] = (float (*) [tempMatrix.columns_]) tempMatrix.data_;
        for(unsigned i = 0; i < R; i++)
        {
            for(unsigned j = 0; j < C; j++)
            {
                data_[i][j] = 0;
                for(unsigned k = 0; k < C; k++)
                {
                    data_[i][j]  += tempMatrix(i, k) * matrix(k, j);
                }
            }
        }
        
        return (*this);
    }else
    {
        return (*this);
    }
}

template<class T, unsigned R, unsigned C>
const TMatrix<T, R, C>& TMatrix<T, R, C>::operator +=(const TMatrix& matrix) {
    for(unsigned i = 0; i < R; i++)
    {
        for(unsigned j = 0; j < C; j++)
        {
            data_[i][j] += matrix(i, j);
        }
    }
    return (*this);
}

template<class T, unsigned R, unsigned C>
const TMatrix<T, R, C>& TMatrix<T, R, C>::operator -=(const TMatrix& matrix) {
    for(unsigned i = 0; i < R; i++)
    {
        for(unsigned j = 0; j < C; j++)
        {
            data_[i][j] -= matrix(i, j);
        }
    }
    return (*this);
}

template<class T, unsigned R, unsigned C>
T& TMatrix<T, R, C>::operator ()(unsigned row, unsigned column) {
    return data_[row][column];
}

template<class T, unsigned R, unsigned C>
T TMatrix<T, R, C>::operator ()(unsigned row, unsigned column) const {
    return data_[row][column];
}

//globals in Candela::Mathematics - friends of Matrix
template<class T, unsigned R, unsigned C>
TMatrix<T, R, C> Candela::Mathematics::operator* (T scalar, const TMatrix<T, R, C>& matrix)
{
    TMatrix<T, R, C> tempMatrix = matrix;
    for(unsigned i = 0; i < R; i++)
    {
        for(unsigned j = 0; j < C; j++)
        {
            tempMatrix(i,j) *= scalar;   
        }
    }
    return tempMatrix;
}

/*template class TMatrix<float, 3, 2>;
template class TMatrix<float, 2, 3>;
template class TMatrix<float, 2, 2>;
template TMatrix<float, 2 ,2> TMatrix<float, 2, 3>::operator *<3, 2, float> (const TMatrix<float, 3, 2>&) const;*/

/* 
 * File:   Rectangle.h
 * Author: Kevin
 *
 * Created on 26 March 2014, 14:26
 */

#ifndef RECTANGLE_H
#define	RECTANGLE_H

#include <string>

namespace Candela
{
    namespace Mathematics
    {
        template<class T>
        class Rectangle {
        public:
            Rectangle();
            Rectangle(T width, T height);
            Rectangle(T pLeft, T pRight, T pBottom, T pTop);
            
            T left, right, bottom, top;
            
            void setRect(T width, T height);
            void setRect(T pLeft, T pRight, T pBottom, T pTop);
            
            std::string toString() const;
            bool contains (T x, T y) const;
            T getWidth() const;
            T getHeight() const;
            T getArea() const;
        };
        
        template<class T>
        std::ostream& operator<< (std::ostream& strm, const Rectangle<T>& rect);
        
        typedef Rectangle<float> Rectangle_F;
        typedef Rectangle<size_t> Rectangle_I;
    }
}



#endif	/* RECTANGLE_H */


/* 
 * File:   Rectangle.cpp
 * Author: Kevin
 * 
 * Created on 26 March 2014, 14:26
 */

#include "Mathematics/Rectangle.h"

#include <sstream>

using namespace Candela::Mathematics;

template<class T>
Rectangle<T>::Rectangle()
        : left ( ), right ( ), bottom ( ), top ( ) 
{

}

template<class T>
Rectangle<T>::Rectangle(T width, T height) 
        : left ( -width / 2 ), right ( -left ), bottom ( -height /  2 ), top ( -bottom )
{
}

template<class T>
Rectangle<T>::Rectangle(T pLeft, T pRight, T pBottom, T pTop) 
        : left ( pLeft ), right ( pRight ), bottom ( pBottom ), top ( pTop ) 
{

}

template<class T>
void Rectangle<T>::setRect(T width, T height) {
    left = - width / 2;
    right = - left;
    bottom = - height / 2;
    top = - bottom;
}

template<class T>
void Rectangle<T>::setRect(T pLeft, T pRight, T pBottom, T pTop) {
    left = pLeft;
    right = pRight;
    bottom = pBottom;
    top = pTop;
}

template<class T>
T Rectangle<T>::getWidth() const {
    return right > left ? right - left : left - right;
}

template<class T>
T Rectangle<T>::getHeight() const {
    return top > bottom ?  top - bottom : bottom - top;
}

template<class T>
T Rectangle<T>::getArea() const {
    return getWidth() * getHeight();
}

/*template<class T>
void Rectangle<T>::setWidth(T width) {
    right = width / 2;
    left = -right;
}

template<class T>
void Rectangle<T>::setHeight(T height) {
    top = height / 2;
    bottom = -top;
}

template<class T>
void Rectangle<T>::setCenter(T x, T y) {
    left += x;
    right += x;
    top += y;
    bottom += y;
}*/


template<class T>
bool Rectangle<T>::contains(T x, T y) const {
    return x >= left && x <= right && y >= bottom && y <= top;
}


//Init

template<class T>
std::string Rectangle<T>::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "Rectangle - Address = " << this
         << "\n\tBottom-Left = (" << left << ", " << bottom << ")"
         << "\n\tTop-Right = (" << right << ", " << top << ")";
    return sstr.str();
}

template<class T>
std::ostream& Candela::Mathematics::operator<<(std::ostream& strm, const Rectangle<T>& v) {
    return strm << v.toString();
}

template class Rectangle<float>;
template class Rectangle<size_t>;

template std::ostream& Candela::Mathematics::operator<< <float> (std::ostream&, const Rectangle<float>&);
template std::ostream& Candela::Mathematics::operator<< <size_t> (std::ostream&, const Rectangle<size_t>&);
/* 
 * File:   Ray.cpp
 * Author: Kevin
 * 
 * Created on 22 December 2013, 14:13
 */

#include "Mathematics/Ray.h"
#include <sstream>

using namespace Candela::Mathematics;

Ray::Ray(const Vector& position, const Vector& direction)
        : position_ ( position ), direction_ ( direction )
{
}


Vector Ray::getPoint(float t) const
{
    return position_ + direction_ * t;
}

const Vector& Ray::getDirection() const
{
    return direction_;
}

const Vector& Ray::getPosition() const
{
    return position_;
}

Ray Ray::operator -() const {
    return Ray(position_, -direction_);
}


Ray Ray::getRayStartingFrom(float t) const
{
    return Ray(getPoint(t), direction_);
}

void Ray::setDirection(const Vector& direction)
{
    direction_ = direction;
}

void Ray::setPosition(const Vector& position)
{
    position_ = position;
}

void Ray::setRay(const Vector& position, const Vector& direction)
{
    direction_ = direction;
    position_ = position;
}

//Update this to use MOVE Semantics when available!!
std::string Ray::toString() const
{
    using namespace std;
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << "Ray - Address = " << this << "\n\tOrigin: " << position_
                << "\n\tDirection: " << direction_;
    return sstr.str();
}

std::ostream& Candela::Mathematics::operator <<(std::ostream& strm, const Ray& ray)
{
    return strm << ray.toString();
}
/* 
 * File:   BoundedRay.cpp
 * Author: Kevin
 * 
 * Created on 28 December 2013, 15:34
 */

#include "Mathematics/BoundedRay.h"
#include "Mathematics/Float.h"
#include <sstream>

using namespace Candela::Mathematics;

BoundedRay::BoundedRay()
    : Ray(0.f, 0.f), tMin_ ( 0.f ), tMax_ ( Float::Infinity )
{
}

BoundedRay::BoundedRay(const Vector& position, const Vector& direction)
        : Ray(position, direction), tMin_ ( 0.f ), tMax_ ( Float::Infinity )
{
}

BoundedRay::BoundedRay(const Vector& position, const Vector& direction, float tMax)
        : Ray(position, direction), tMin_ ( 0.f )
{
    setMax(tMax);
}


BoundedRay::BoundedRay(const Vector& position, const Vector& direction, float tMin, float tMax) 
        : Ray(position, direction), tMin_ ( tMin )
{
    setMax(tMax);
}


float BoundedRay::getMin() const {
    return tMin_;
}

float BoundedRay::getMax() const {
    return tMax_;
}

Vector BoundedRay::getMinPoint() const {
    return tMin_ == 0.f ? getPosition() : getPoint(tMin_);
}

Vector BoundedRay::getMaxPoint() const {
    return getPoint(tMax_);
}

void BoundedRay::setMin(float tMin) {
    tMin_ = tMin;
    if(tMax_ < tMin_)
    {
        tMax_ = tMin_;
    }
}

void BoundedRay::setMax(float tMax) {
    tMax_ = tMax;
    if(tMin_ > tMax_)
    {
        tMin_ = tMax_;
    }
}

void BoundedRay::setMinSafe(float tMin) {
    if(tMax_ >= tMin)
    {
        tMin_ = tMin;
    }
}

void BoundedRay::setMaxSafe(float tMax) {
    if(tMin_ <= tMax)
    {
        tMax_ = tMax;
    }
}

std::string BoundedRay::toString() const {
    using namespace std;
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << Ray::toString() << "\nBoundedRay - Address = " << this << "\n\t"
            << "Min: " << tMin_ << " Max: " << tMax_;
    return sstr.str();
}

void BoundedRay::advanceMinByEpsilon(float factor) {
    setMin(tMin_ + factor * Float::MachineEpsilon);
}

void BoundedRay::setMinAndAdvanceByEpsilon(float tMin, float factor) {
    setMin(tMin + factor * Float::MachineEpsilon);
}

std::ostream& Candela::Mathematics::operator <<(std::ostream& strm, const BoundedRay& ray)
{
    return strm << ray.toString();
}

/* 
 * File:   Funtions.h
 * Author: Kevin
 *
 * Created on 19 April 2014, 18:57
 */

#ifndef FUNTIONS_H
#define	FUNTIONS_H

#include <algorithm>

namespace Candela
{
    namespace Mathematics
    {
        //instead of static class
        //define functions in header later and compare speedup
        namespace Functions
        {
            float sin(float theta);
            double sin(double theta);
            float cos(float theta);
            double cos(double theta);
            float pow(float num, float exp);
            double pow(double num, double exp);
            float ln(float num);
            double ln(double ln);
            float sqrt(float num);
            double sqrt(double num);
            float mod (float num, float denom);
            double mod (double num, double denom);
            float abs(float num);
            double abs(double num);

            template <class T>
            T max (T a, T b) {
                return std::max(a, b);
            }
            template<>
            float max(float a, float b);
            template<>
            double max(double a, double b);

            template <class T>
            T min (T a, T b) {
                return std::min(a, b);
            }
            template<>
            float min(float a, float b);
            template<>
            double min(double a, double b);



            float add(float, float);
            float subtract(float, float);
            float multiply(float, float);
            float divide(float, float);
            double add(double, double);
            double subtract(double, double);
            double multiply(double, double);
            double divide(double, double);

            template <class T>
            T clamp(T val, T a, T b) {
                if (val < a) {
                    return a;
                } else if (val > b) {
                    return b;
                } else {
                    return val;
                }
            }
            
        }
    }
            
}

#endif	/* FUNTIONS_H */


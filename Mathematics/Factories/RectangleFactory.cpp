//
// Created by Kevin on 06/12/2017.
//

#include "RectangleFactory.h"

using namespace std;
using namespace Candela::Mathematics;
using namespace Candela::Configuration;

unique_ptr<Rectangle_F> Rectangle_F_Factory::create() const {
    return make_unique<Rectangle_F>();
}

unique_ptr<Rectangle_F> Rectangle_F_Factory::create(const ConfigurationNode &config) const {
    return make_unique<Rectangle_F>(static_cast<float>(config["Width"]->read<double>()),
                                    static_cast<float>(config["Height"]->read<double>()));
}

unique_ptr<Rectangle_I> Rectangle_I_Factory::create() const {
    return make_unique<Rectangle_I>();
}

unique_ptr<Rectangle_I> Rectangle_I_Factory::create(const ConfigurationNode &config) const {
    return make_unique<Rectangle_I>(config["Left"]->read<uint64_t>(),
                                    config["Right"]->read<uint64_t>(),
                                    config["Bottom"]->read<uint64_t>(),
                                    config["Top"]->read<uint64_t>());
}

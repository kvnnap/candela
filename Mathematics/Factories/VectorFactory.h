//
// Created by Kevin on 06/12/2017.
//

#ifndef CANDELA_VECTORFACTORY_H
#define CANDELA_VECTORFACTORY_H

#include "Factory/Factory.h"
#include "Mathematics/Vector3.h"

namespace Candela
{
    namespace Mathematics
    {
        class VectorFactory
                : public Factory::Factory<Vector>
        {
        public:
            std::unique_ptr<Vector> create() const override;
            std::unique_ptr<Vector> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}


#endif //CANDELA_VECTORFACTORY_H

//
// Created by Kevin on 06/12/2017.
//

#ifndef CANDELA_RECTANGLEFACTORY_H
#define CANDELA_RECTANGLEFACTORY_H

#include "Factory/Factory.h"
#include "Mathematics/Rectangle.h"

namespace Candela
{
    namespace Mathematics
    {
        class Rectangle_F_Factory
                : public Factory::Factory<Rectangle_F>
        {
        public:
            std::unique_ptr<Rectangle_F> create() const override;
            std::unique_ptr<Rectangle_F> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}

namespace Candela
{
    namespace Mathematics
    {
        class Rectangle_I_Factory
                : public Factory::Factory<Rectangle_I>
        {
        public:
            std::unique_ptr<Rectangle_I> create() const override;
            std::unique_ptr<Rectangle_I> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}


#endif //CANDELA_RECTANGLEFACTORY_H

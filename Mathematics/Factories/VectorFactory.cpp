//
// Created by Kevin on 06/12/2017.
//

#include "VectorFactory.h"

using namespace std;
using namespace Candela::Mathematics;
using namespace Candela::Configuration;

unique_ptr<Vector> VectorFactory::create() const {
    return make_unique<Vector>();
}

unique_ptr<Vector> VectorFactory::create(const ConfigurationNode &vectorNode) const {
    unique_ptr<Vector> v = create();
    v->x = static_cast<float>(vectorNode["x"]->read<double>());
    v->y = static_cast<float>(vectorNode["y"]->read<double>());
    v->z = static_cast<float>(vectorNode["z"]->read<double>());
    return v;
}

/* 
 * File:   Funtions.cpp
 * Author: Kevin
 * 
 * Created on 19 April 2014, 18:57
 */

#include "Mathematics/Funtions.h"

#include <cmath>

float Candela::Mathematics::Functions::sin(float theta)
{
    return std::sin(theta);
}

float Candela::Mathematics::Functions::cos(float theta)
{
    return std::cos(theta);
}

float Candela::Mathematics::Functions::pow(float num, float exp)
{
    return std::pow(num, exp);
}

double Candela::Mathematics::Functions::sin(double theta)
{
    return std::sin(theta);
}

double Candela::Mathematics::Functions::cos(double theta)
{
    return std::cos(theta);
}

double Candela::Mathematics::Functions::pow(double num, double exp)
{
    return std::pow(num, exp);
}

float Candela::Mathematics::Functions::ln(float num)
{
    return std::log(num);
}

double Candela::Mathematics::Functions::ln(double num)
{
    return std::log(num);
}

float Candela::Mathematics::Functions::sqrt(float num) {
    return std::sqrt(num);
}

double Candela::Mathematics::Functions::sqrt(double num) {
    return std::sqrt(num);
}

float Candela::Mathematics::Functions::mod(float num, float denom) {
    return std::fmod(num, denom);
}

double Candela::Mathematics::Functions::mod(double num, double denom) {
    return std::fmod(num, denom);
}

float Candela::Mathematics::Functions::abs(float num) {
    return std::abs(num);
}

double Candela::Mathematics::Functions::abs(double num) {
    return std::abs(num);
}

template<>
float Candela::Mathematics::Functions::min(float a, float b) {
    return std::fminf(a, b);
}

template<>
double Candela::Mathematics::Functions::min(double a, double b) {
    return std::fmin(a, b);
}

template<>
float Candela::Mathematics::Functions::max(float a, float b) {
    return std::fmaxf(a, b);
}

template<>
double Candela::Mathematics::Functions::max(double a, double b) {
    return std::fmax(a, b);
}

float Candela::Mathematics::Functions::add(float a, float b)
{
    return a + b;
}
float Candela::Mathematics::Functions::subtract(float a, float b)
{
    return a - b;
}
float Candela::Mathematics::Functions::multiply(float a, float b)
{
    return a * b;
}
float Candela::Mathematics::Functions::divide(float a, float b)
{
    return a / b;
}

double Candela::Mathematics::Functions::add(double a, double b)
{
    return a + b;
}
double Candela::Mathematics::Functions::subtract(double a, double b)
{
    return a - b;
}
double Candela::Mathematics::Functions::multiply(double a, double b)
{
    return a * b;
}
double Candela::Mathematics::Functions::divide(double a, double b)
{
    return a / b;
}

/* 
 * File:   Ray.h
 * Author: Kevin
 *
 * Created on 22 December 2013, 14:13
 */

#ifndef RAY_H
#define	RAY_H

#include "Vector3.h"

namespace Candela 
{
    namespace Mathematics
    {
        class Ray {
        public:
            Ray(const Vector& position, const Vector& direction);
            
            Vector getPoint(float t) const;
            
            const Vector& getDirection() const;
            const Vector& getPosition() const;
            
            Ray operator - () const;
            Ray getRayStartingFrom(float t) const;
            
            void setPosition(const Vector& position);
            void setDirection(const Vector& direction);
            void setRay(const Vector& position, const Vector& direction);
            
            /*virtual */std::string toString() const;
            
        private:
            //A ray is defined by r = position_ + t * direction_
            Vector position_, direction_;
        };
        
        std::ostream& operator<< (std::ostream& strm, const Ray& ray);
    }
}

#endif	/* RAY_H */


/* 
 * File:   Vector2.h
 * Author: Kevin
 *
 * Created on 25 April 2014, 21:48
 */

#ifndef VECTOR2_H
#define	VECTOR2_H

#include <ostream>

namespace Candela
{
    namespace Mathematics
    {
        template<class T>
        class Vector2 {
        public:
            
            //unnamed unions is part of C++ standard
            union {
                //warning - unnamed structures are not C++ standards (as as side note C standard does not support both)
                struct {
                    T x, y;
                };
                T xy[2];
            };
            
            static const Vector2 UnitX;
            static const Vector2 UnitY;
            
            Vector2();
            Vector2(T xy); //will implicitly convert
            Vector2(T x, T y);
            
            //unary operators
            Vector2 operator - () const;
            T operator ! () const; //magnitude
            Vector2 operator ~ () const; //normalise
            bool isNotSet() const;
            T angle(const Vector2 &v) const;
            T cosAngle(const Vector2 &) const;
            
            //binary operators
            Vector2 operator + (const Vector2 &) const;
            Vector2 operator - (const Vector2 &) const;
            T operator ^ (const Vector2 &) const; //cross-product - returns i part so returns T (or make it return Vector3 with i set)
            Vector2 hadamard(const Vector2 &) const;
            Vector2 operator * (T) const; //scalar product
            Vector2 operator / (T) const; //scalar product
            T operator * (const Vector2 &) const; //dot product
            bool operator == (const Vector2 &) const; //uses ULP
            
            //self binary operators
            Vector2& operator += (const Vector2 &);
            Vector2& operator -= (const Vector2 &);
            Vector2& setHadamard(const Vector2 &);
            Vector2& operator *= (T);
            Vector2& operator /= (T);
            
        private:
            
            //typedef Vector3<float> Vector;
        };
        
        //Vector Multiplication Operator for 
        template <class T> 
        Vector2<T> operator* (T, const Vector2<T> &);

        template <class T>
        std::ostream& operator<< (std::ostream&, const Vector2<T>&);
    }
}



#endif	/* VECTOR2_H */


//
// Created by kvnna on 04/02/2018.
//

#include "WhittedIntegratorFactory.h"
#include "Integrator/WhittedIntegrator.h"

using namespace std;
using namespace Candela;
using namespace Candela::Configuration;
using namespace Candela::Integrator;

unique_ptr<IIntegrator> WhittedIntegratorFactory::create() const {
    return make_unique<WhittedIntegrator>();
}

unique_ptr<IIntegrator> WhittedIntegratorFactory::create(const ConfigurationNode &config) const {
    unique_ptr<WhittedIntegrator> integrator = make_unique<WhittedIntegrator>();
    integrator->setName(**config["Name"]);
    integrator->setRayMaxDepth(static_cast<size_t>(config["RayMaxDepth"]->read<uint64_t>()));
    return integrator;
}

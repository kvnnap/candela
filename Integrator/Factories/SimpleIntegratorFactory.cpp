//
// Created by kvnna on 18/12/2017.
//

#include "SimpleIntegratorFactory.h"
#include "Integrator/SimpleIntegrator.h"

using namespace std;
using namespace Candela;
using namespace Candela::Configuration;
using namespace Candela::Integrator;

unique_ptr<IIntegrator> SimpleIntegratorFactory::create() const {
    return make_unique<SimpleIntegrator>();
}

unique_ptr<IIntegrator> SimpleIntegratorFactory::create(const Configuration::ConfigurationNode &config) const {
    unique_ptr<IIntegrator> integrator = make_unique<SimpleIntegrator>();
    integrator->setName(**config["Name"]);
    return integrator;
}

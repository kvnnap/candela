//
// Created by kvnna on 04/02/2018.
//

#ifndef CANDELA_WHITTEDINTEGRATORFACTORY_H
#define CANDELA_WHITTEDINTEGRATORFACTORY_H

#include "Factory/Factory.h"
#include "Integrator/IIntegrator.h"

namespace Candela
{
    // Integrators give back the radiance value from an intersection point
    namespace Integrator
    {
        class WhittedIntegratorFactory
                : public Factory::Factory<IIntegrator>
        {
        public:
            std::unique_ptr<IIntegrator> create() const override;
            std::unique_ptr<IIntegrator> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}


#endif //CANDELA_WHITTEDINTEGRATORFACTORY_H

//
// Created by kvnna on 07/01/2018.
//

#ifndef CANDELA_PATHINTEGRATORFACTORY_H
#define CANDELA_PATHINTEGRATORFACTORY_H

#include "Factory/Factory.h"
#include "Integrator/IIntegrator.h"

namespace Candela
{
    // Integrators give back the radiance value from an intersection point
    namespace Integrator
    {
        class PathIntegratorFactory
                : public Factory::Factory<IIntegrator>
        {
        public:
            std::unique_ptr<IIntegrator> create() const override;
            std::unique_ptr<IIntegrator> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}

#endif //CANDELA_PATHINTEGRATORFACTORY_H

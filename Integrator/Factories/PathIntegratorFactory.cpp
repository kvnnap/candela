//
// Created by kvnna on 07/01/2018.
//

#include "PathIntegratorFactory.h"
#include "Integrator/PathIntegrator.h"

using namespace std;
using namespace Candela;
using namespace Candela::Configuration;
using namespace Candela::Integrator;

unique_ptr<IIntegrator> PathIntegratorFactory::create() const {
    return make_unique<PathIntegrator>();
}

unique_ptr<IIntegrator> PathIntegratorFactory::create(const Configuration::ConfigurationNode &config) const {
    unique_ptr<PathIntegrator> integrator = make_unique<PathIntegrator>();
    integrator->setName(**config["Name"]);

    if (config.keyExists("ExplicitDirectLighting")) {
        integrator->setExplicitDirectLighting(config["ExplicitDirectLighting"]->read<bool>());
    }

    if (config.keyExists("CosineWeightedDiffuseRays")) {
        integrator->setCosineWeightedDiffuseRays(config["CosineWeightedDiffuseRays"]->read<bool>());
    }

    return integrator;
}


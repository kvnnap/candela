//
// Created by kvnna on 18/12/2017.
//

#ifndef CANDELA_SIMPLEINTEGRATORFACTORY_H
#define CANDELA_SIMPLEINTEGRATORFACTORY_H

#include "Factory/Factory.h"
#include "Integrator/IIntegrator.h"

namespace Candela
{
    // Integrators give back the radiance value from an intersection point
    namespace Integrator
    {
        class SimpleIntegratorFactory
            : public Factory::Factory<IIntegrator>
        {
        public:
            std::unique_ptr<IIntegrator> create() const override;
            std::unique_ptr<IIntegrator> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}


#endif //CANDELA_SIMPLEINTEGRATORFACTORY_H

//
// Created by kvnna on 04/02/2018.
//

#ifndef CANDELA_WHITTEDINTEGRATOR_H
#define CANDELA_WHITTEDINTEGRATOR_H

#include "Integrator.h"

namespace Candela
{
    // Integrators give back the radiance value from an intersection point
    namespace Integrator
    {

        class WhittedContext {
        public:
            size_t maxDepth;
            float refractiveIndex;
        };

        class WhittedIntegrator
                : public Integrator
        {
        public:
            WhittedIntegrator();

            void radiance(Renderer::RadianceContent& radianceContent,
                          const Scene::IScene& scene,
                          const Mathematics::BoundedRay& ray) override;

            void radiance(Renderer::RadianceContent& radianceContent,
                          const Scene::IScene& scene,
                          const Mathematics::BoundedRay& ray, WhittedContext context) const;

            std::unique_ptr<IIntegrator> clone() const override;
            std::string toString() const override;

            void setRayMaxDepth(size_t maxDepth);
            size_t getRayMaxDepth() const;

        private:
            size_t maxDepth;
        };
    }
}

#endif //CANDELA_WHITTEDINTEGRATOR_H

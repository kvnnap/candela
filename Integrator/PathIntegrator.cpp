//
// Created by kvnna on 28/06/2016.
//

#include <memory>
#include <iostream>

#include <Material/BaseMaterial.h>
#include <Mathematics/Float.h>
#include <Mathematics/Funtions.h>
#include <Mathematics/Constants.h>
#include "SimpleIntegrator.h"
#include "Spectrum/RgbSpectrum.h"
#include "PathIntegrator.h"

using namespace std;
using namespace Candela::Integrator;
using namespace Candela::Sampler;
using namespace Candela::Spectrum;
using namespace Candela::Integrator;
using namespace Candela::Mathematics;
using namespace Candela::Material;
using namespace Candela::Scene;
using namespace Candela::Light;
using namespace Candela::Renderer;

PathIntegrator::PathIntegrator()
        : explicitDirectLighting (true), cosineWeightedDiffuseRays (true)
{}

void PathIntegrator::radiance(RadianceContent &radianceContent, const IScene &scene,
                              const BoundedRay &ray)
{
    recursiveRadiance(radianceContent, scene, ray, 0);
}

void PathIntegrator::recursiveRadiance(RadianceContent &radianceContent, const IScene &scene,
                                       const BoundedRay &ray, size_t depth) {
    // Clear radiance
    radianceContent.radiance = 0.f;

    // Intersect the given ray into the scene's mesh
    Intersection intersection, shadowIntersection;
    scene.intersect(ray, intersection);

    // If we intersect nothing, return
    if (intersection.shapeIntersection.Type != Shape::Intersection::External)
        return;

    // If this is the spawned ray from the camera, light sources must contribute
    if (intersection.primitive->isEmissive()) {
        if (((depth == 0) || !explicitDirectLighting) && (intersection.shapeIntersection.Type == Shape::Intersection::External)) {
            intersection.primitive->getLight()->radiance(ray, radianceContent.radiance);
        }
        return;
    }

    const Shape::IShape* intersectant = intersection.shapeIntersection.Intersectant;
    const Material::IMaterial* material = intersection.primitive->getMaterial();

    // Calculate intersection point
    const Vector& intersectionPoint =
            intersection.shapeIntersection.point = ray.getPoint(intersection.shapeIntersection.Distance);
    const Vector& unitDirection = -~ray.getDirection();
    const Vector& unitNormal = intersectant->getUnitNormal(intersection.shapeIntersection);
    const Vector2<float>& textureUV = intersectant->getTextureUv(intersection.shapeIntersection);

    // Calculate direct lighting - Explicit direct lighting

    // Using naive approach
    if (explicitDirectLighting && !scene.getLights().empty()) {
        const float lightPdf = 1.f / scene.getLights().size();
        ILight* light =  scene.getLights()[sampler.chooseInRange(0, scene.getLights().size() - 1)];

        // Choose random point on luminaire
        Vector unitLightSurfaceNormal;
        Vector pointOnLightSource = light->samplePoint(sampler.nextSample(), sampler.nextSample(), unitLightSurfaceNormal);
        BoundedRay shadowRay (intersectionPoint, pointOnLightSource - intersectionPoint);

        // Advance the ray by a little
        shadowRay.setMin(1000.f * Float::MachineEpsilon);
        shadowRay.setMax(1.f);

        const Vector& unitShadowRayDirection = ~shadowRay.getDirection();
        const float dot = unitNormal * unitShadowRayDirection;

        // If the light is behind this primitive, then skip
        if (dot >= 0.f) {
            const float lightShadowDot = unitLightSurfaceNormal * -unitShadowRayDirection;

            // Check that we don't hit the opposite face of the light source and that we're not occluded
            if (lightShadowDot >= 0.f && !scene.intersect(shadowRay, shadowIntersection, light)) {

                // Get radiance
                RgbSpectrum rgbSpectrum, lightRgb;
                light->radiance(shadowRay, lightRgb);

                // Get projected area
                const float d = !shadowRay.getDirection();
                const float projectedArea = light->getArea() * lightShadowDot / (d * d);

                // Multiply diffuse albedo by 1/Pi to respect energy conservation
                // Brdf diffuse = albedo/pi
                if (material->getDiffuse() != 0.f) {
                    rgbSpectrum = lightRgb;
                    rgbSpectrum *= material->getDiffuseTexture().GetSpectrum(textureUV)->toTristimulusSRGB();
                    rgbSpectrum *= material->getDiffuse() * Constants::OneOverPi * dot * (projectedArea / lightPdf);
                    radianceContent.radiance += rgbSpectrum;
                }

                // Add glossy

                // Get pure specular ray from L (shadow) and N
                if (material->getSpecular() != 0.f) {
                    const Vector pureSpecularDirection =
                            (2.f * (unitNormal * unitShadowRayDirection)) * unitNormal - unitShadowRayDirection;
                    float specCoeff = (material->getSpecularExponent() + 2.f) / (2.f * Constants::Pi)
                                      * Functions::pow(unitDirection * pureSpecularDirection,
                                                       material->getSpecularExponent());

                    specCoeff *= material->getSpecular() * dot * (projectedArea / lightPdf);

                    rgbSpectrum = lightRgb;
                    rgbSpectrum *= specCoeff;
                    radianceContent.radiance += rgbSpectrum;
                }
            }
        }
    }

    // Choose whether to trace diffuse indirect or specular glossy
    const float probabilityOfDiffuse = material->getDiffuse() / (material->getDiffuse() + material->getSpecular());
    if (probabilityOfDiffuse == 1.f || (sampler.nextSample() <= probabilityOfDiffuse)) {
        if (cosineWeightedDiffuseRays) {
            float p;
            BoundedRay indirectRay (intersectionPoint, randomRayLobe(unitNormal, sampler, p));
            indirectRay.setMin(1000.f * Float::MachineEpsilon);
            const float probabilityOfContinuing = unitNormal * indirectRay.getDirection();
            if (sampler.nextSample() <= probabilityOfContinuing) {
                RadianceContent indirectRadiance;
                // We continue
                recursiveRadiance(indirectRadiance, scene, indirectRay, depth + 1);

                // Color (albedo)
                indirectRadiance.radiance *= material->getDiffuseTexture().GetSpectrum(textureUV)->toTristimulusSRGB();
                indirectRadiance.radiance *= material->getDiffuse()
                                            / (probabilityOfDiffuse * // <-- weight for diffuse choice
                                               probabilityOfContinuing); // <-- cosine weighted sampling where pdf = cos/pi. 1 / pdf = pi/cos
                radianceContent.radiance += indirectRadiance.radiance;

                // weight for diffuse choice - Integrated in above line for performance
//                indirectRadiance.radiance *= 1.f / probabilityOfDiffuse;

                // Russian roulette weighting
//        indirectRadiance.radiance *= 1.f / probabilityOfContinuing;

                // Dot product
//        indirectRadiance.radiance *=  probabilityOfContinuing;

                // BRDF  = albedo / pi
//        indirectRadiance.radiance *= Constants::OneOverPi;

                // cosine weighted sampling where pdf = cos/pi. 1 / pdf = pi/cos - Integrated in above line for performance
//                indirectRadiance.radiance *= 1.f /** Constants::Pi*/ / probabilityOfContinuing;
            }

        } else {
            // Calculate indirect lighting
            BoundedRay indirectRay(intersectionPoint, randomRayHemisphere(unitNormal, sampler));
            indirectRay.setMin(1000.f * Float::MachineEpsilon);
            const float probabilityOfContinuing = unitNormal * indirectRay.getDirection();
            if (sampler.nextSample() <= probabilityOfContinuing) {
                RadianceContent indirectRadiance;
                // We continue
                recursiveRadiance(indirectRadiance, scene, indirectRay, depth + 1);

                // Color (albedo)
                indirectRadiance.radiance *= material->getDiffuseTexture().GetSpectrum(textureUV)->toTristimulusSRGB();
                indirectRadiance.radiance *= (material->getDiffuse() *
                                             2.f)// <--  1 / pdf for uniform sampling on the hemisphere , where pdf = 1/ 2PI
                                             / probabilityOfDiffuse; // <-- weight for diffuse choice
                radianceContent.radiance += indirectRadiance.radiance;

                // Russian roulette weighting
//        indirectRadiance.radiance *= 1.f / probabilityOfContinuing;

                // Dot product
//        indirectRadiance.radiance *=  probabilityOfContinuing;

                // weight for diffuse choice - Integrated in above line for performance
//                indirectRadiance.radiance *= 1.f / probabilityOfDiffuse;

                // BRDF  = albedo / pi
//        indirectRadiance.radiance *= Constants::OneOverPi;

                // 1 / pdf for uniform sampling on the hemisphere , where pdf = 1/ 2PI  - Integrated in above line for performance
//                indirectRadiance.radiance *= 2.f; //* Constants::Pi;
            }

        }

    } else // Calculate glossy (Phong - I think this is the modified one)
    {
        // Generate Ray according to Phong PDF - basically the view ray is used to generate the purely specular
        // reflected ray (R) using the pdf. Then, the outgoing ray (into the scene) can be calculated from R.
        float probabilityOfSpecular; // This equals (n + 1) / (2pi) * cos^n(phi)
        BoundedRay pureSpecularRay (intersectionPoint, randomRayLobe(unitDirection, sampler, probabilityOfSpecular, material->getSpecularExponent()));
        // For sharp angles, it is likely that the selected specular is larger than 90 degrees from the unitNormal.
        // Currently, this sample is rejected in order to make it easier..
        const float probabilityOfContinuing = unitNormal * pureSpecularRay.getDirection();
        if (probabilityOfContinuing >= 0.f) {

            if (sampler.nextSample() <= probabilityOfContinuing) {
                RadianceContent glossyRadiance;

                // Reflect pure specular ray to get outgoing one
                BoundedRay outgoingRay (intersectionPoint, (2.f * (unitNormal * pureSpecularRay.getDirection())) * unitNormal - pureSpecularRay.getDirection());
                outgoingRay.setMin(1000.f * Float::MachineEpsilon);

                recursiveRadiance(glossyRadiance, scene, outgoingRay, depth + 1);

                // Cosine term from rendering equation
//                glossyRadiance.radiance *= probabilityOfContinuing;

                // Russian roulette weighting
//                glossyRadiance.radiance *= 1.f / probabilityOfContinuing;

                // Specular Ks - TODO: support map later
                //glossyRadiance.radiance *= material->getSpecularTexture().GetSpectrum(textureUV)->toTristimulusSRGB();
                glossyRadiance.radiance *= (material->getSpecular()
                                            * ((material->getSpecularExponent() + 2.f) / (material->getSpecularExponent() + 1.f))) // <-- normalisation / pdf of specular
                                           / (1.f - probabilityOfDiffuse); // <-- Weight for choosing specular
                radianceContent.radiance += glossyRadiance.radiance;

                // Normalised Phong - (n+2) * cos^n /(2pi) - BRDF
//                glossyRadiance.radiance *= (material->getSpecularExponent() + 2.f) / (2.f * Mathematics::Constants::Pi)
//                                           * Functions::pow(unitDirection * pureSpecularRay.getDirection(), material->getSpecularExponent());

                // Weight according to pdf of specular - this means 2pi / ((n+1)*cos^n(phi))
//                glossyRadiance.radiance *= 1.f / probabilityOfSpecular;

                // Result of the above commented two calculations i.e. normalisation / pdf - Integrated in above line for performance
//                glossyRadiance.radiance *= (material->getSpecularExponent() + 2.f) / (material->getSpecularExponent() + 1.f);

                // Weight for choosing specular - Integrated in above line for performance
//                glossyRadiance.radiance *= 1.f / (1.f - probabilityOfDiffuse);

            }

        }
    }
}

Vector PathIntegrator::transformPointToBasis(const Vector &unitNormal, const Vector &point) {
    // Create first vector perpendicular to the normal
    const Vector u = unitNormal.y != 0.f || unitNormal.x != 0.f ?
                    ~Vector(unitNormal.y, -unitNormal.x, 0.f) :
                     Vector(unitNormal.z, 0.f, 0.f);

    // Create second vector perpendicular to the normal
    const Vector w = ~(u ^ unitNormal);
    const Vector& v = unitNormal;

    return point.x * u + point.y * v + point.z * w;
}

Vector PathIntegrator::randomRayHemisphere(const Vector &unitNormal, UniformSampler &sampler) {
    // The pdf is 1 / (2*pi)
    // Generate two random numbers and sample unit hemisphere with y-axis orientation
    const float cosTheta = sampler.nextSample();
    const float sinTheta = Functions::sqrt(1.f - cosTheta * cosTheta);
    const float phi = 2.f * Mathematics::Constants::Pi * sampler.nextSample();

    // Map onto world hemisphere
    return transformPointToBasis(unitNormal, Vector(sinTheta * Functions::cos(phi), cosTheta, sinTheta * Functions::sin(phi)));
}

Vector PathIntegrator::randomRayLobe(const Vector &unitNormal, UniformSampler &sampler, float& probability, float n) {
    // The pdf is (n + 1) cos^n(phi) / (2*pi)
    const float nPlusOne = n + 1.f;
    const float cosPhiToTheNPlusOne = sampler.nextSample();
    // TODO: uncomment if used
    //probability = nPlusOne / (2.f * Mathematics::Constants::Pi) * Functions::pow(cosPhiToTheNPlusOne, n / nPlusOne);
    const float cosPhi = Functions::pow(cosPhiToTheNPlusOne, 1.f / nPlusOne);
    const float sinPhi = Functions::sqrt(1.f - cosPhi * cosPhi);
    const float theta = 2.f * Mathematics::Constants::Pi * sampler.nextSample();
    return transformPointToBasis(unitNormal, Vector(sinPhi * Functions::cos(theta), cosPhi, sinPhi * Functions::sin(theta)));
}

std::unique_ptr<IIntegrator> PathIntegrator::clone() const {
    unique_ptr<PathIntegrator> integrator = make_unique<PathIntegrator>();
    integrator->setName(getName());
    integrator->setExplicitDirectLighting(explicitDirectLighting);
    integrator->setCosineWeightedDiffuseRays(cosineWeightedDiffuseRays);
    return integrator;
}

bool PathIntegrator::getExplicitDirectLighting() const {
    return explicitDirectLighting;
}

void PathIntegrator::setExplicitDirectLighting(bool explicitDirectLighting) {
    this->explicitDirectLighting = explicitDirectLighting;
}

bool PathIntegrator::getCosineWeightedDiffuseRays() const {
    return cosineWeightedDiffuseRays;
}

void PathIntegrator::setCosineWeightedDiffuseRays(bool cosineWeightDiffuseRays) {
    this->cosineWeightedDiffuseRays = cosineWeightDiffuseRays;
}



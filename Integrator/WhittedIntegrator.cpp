//
// Created by kvnna on 04/02/2018.
//

#include <sstream>

#include "WhittedIntegrator.h"
#include "Light/ILight.h"
#include "Mathematics/Float.h"
#include "Mathematics/Funtions.h"
#include "Spectrum/RgbSpectrum.h"
#include "Texture/MatteTexture.h"

using namespace std;
using namespace Candela::Integrator;
using namespace Candela::Renderer;
using namespace Candela::Mathematics;
using namespace Candela::Scene;
using namespace Candela::Light;
using namespace Candela::Spectrum;
using namespace Candela::Texture;

WhittedIntegrator::WhittedIntegrator()
    : maxDepth ()
{}

std::unique_ptr<IIntegrator> WhittedIntegrator::clone() const {
    unique_ptr<WhittedIntegrator> integrator = make_unique<WhittedIntegrator>();
    integrator->setName(getName());
    integrator->setRayMaxDepth(getRayMaxDepth());
    return integrator;
}

std::string WhittedIntegrator::toString() const {
    ostringstream sstr;
    sstr << IIntegrator::toString()
         << " - Whitted Integrator: Max Depth = "
         << maxDepth << endl;
    return sstr.str();
}

void WhittedIntegrator::setRayMaxDepth(size_t maxDepth) {
    this->maxDepth = maxDepth;
}

size_t WhittedIntegrator::getRayMaxDepth() const {
    return maxDepth;
}

void WhittedIntegrator::radiance(RadianceContent &radianceContent, const IScene &scene, const BoundedRay &ray) {
    return radiance(radianceContent, scene, ray, WhittedContext {maxDepth, 1.f});
}

void WhittedIntegrator::radiance(RadianceContent &radianceContent, const IScene &scene, const BoundedRay &ray,
                                 WhittedContext context) const {

    // Clear radiance
    radianceContent.radiance = 0.f;

    // Intersect the given ray into the scene's mesh
    Intersection intersection, shadowIntersection;
    scene.intersect(ray, intersection);

    // If we intersect nothing, return
    if (intersection.shapeIntersection.Type != Shape::Intersection::External)
        return;

    if (intersection.primitive->isEmissive()) {
        if (context.maxDepth == maxDepth && intersection.shapeIntersection.Type == Shape::Intersection::External)
            intersection.primitive->getLight()->radiance(ray, radianceContent.radiance);
        return;
    }

    // Hacky, need to solve this.
    const Shape::IShape* intersectant = intersection.shapeIntersection.Intersectant;
    const Material::IMaterial* material = intersection.primitive->getMaterial();

    // Calculate intersection point
    const Vector& intersectionPoint =
            intersection.shapeIntersection.point = ray.getPoint(intersection.shapeIntersection.Distance);
    const Vector& unitNormal = intersectant->getUnitNormal(intersection.shapeIntersection);
    const Vector& unitDirection = ~ray.getDirection();
    const Vector2<float>& textureUV = intersectant->getTextureUv(intersection.shapeIntersection);

    // Calculate direct lighting
    for (const ILight* light : scene.getLights()) {

        // Launch ray from intersection point to the light, and check for any occluders
        // Cast ray towards light source to detect occluder, if any
        BoundedRay shadowRay (intersectionPoint,
                              light->getCentroid() - intersectionPoint);

        // Advance the ray by a little
        shadowRay.setMin(1000.f * Float::MachineEpsilon);
        shadowRay.setMax(1.f);

        // Compute dot product to get intensity coefficient
        const Vector& unitShadowRayDirection = ~shadowRay.getDirection();
        float dot = unitNormal * unitShadowRayDirection;

        // We can't see this
        if (dot < 0.f)
            continue;

        // Check if we would hit the opposite face of the light source
        Vector unitLightSurfaceNormal;
        light->samplePoint(0.f, 0.f, unitLightSurfaceNormal);
        const float lightShadowDot = unitLightSurfaceNormal * -unitShadowRayDirection;
        if (lightShadowDot < 0.f) {
            continue;
        }

        // If Occlusion, try next light source
        if (scene.intersect(shadowRay, shadowIntersection, light) &&
                (shadowIntersection.primitive->isEmissive() ||
            &shadowIntersection.primitive->getMaterial()->getDiffuseTexture() != &MatteTexture::black))
            continue;

        // Get radiance
        RgbSpectrum lightRgb;
        light->radiance(shadowRay, lightRgb);

        // Multiply and add
        lightRgb *= material->getDiffuseTexture().GetSpectrum(textureUV)->toTristimulusSRGB();

        // Also multiplying by lightShadowDot to simulate area light falloff. for point lights it would be one
        const float shadowRayLength = !shadowRay.getDirection();
        lightRgb *= dot * lightShadowDot / (shadowRayLength * shadowRayLength);
        radianceContent.radiance += lightRgb;

        // Calculate specular reflection, if any - TODO: Improve comparison
        if (&material->getSpecularTexture() == &MatteTexture::black)
            continue;

        // Reflect  the ray coming from light source to primitive (negative ref)
        Vector specularReflection ( unitShadowRayDirection - (2 * (unitNormal * unitShadowRayDirection)) * unitNormal );
        dot = unitDirection * ~specularReflection;
        if (dot < 0)
            continue;

        // Add to radiance
        RgbSpectrum specularSpectrum;
        material->getSpecularTexture().GetSpectrum(textureUV)->multiply(lightRgb, specularSpectrum);
        specularSpectrum *= Functions::pow(dot, material->getSpecularExponent());

        radianceContent.radiance += specularSpectrum;
    }

    // Can't go in more depth
    if (context.maxDepth == 0)
        return;

    // Reflection
    if (&material->getReflectionTexture() != &MatteTexture::black) {
        Vector reflection(unitDirection - (2 * (unitNormal * unitDirection)) * unitNormal);
        RadianceContent tempRadiance;
        RgbSpectrum reflectionSpectrum = material->getDiffuseTexture().GetSpectrum(textureUV)->toTristimulusSRGB();
        reflectionSpectrum *= material->getReflectionTexture().GetSpectrum(textureUV)->toTristimulusSRGB();
        BoundedRay reflectionRay(intersectionPoint, reflection);
        reflectionRay.setMin(1000.f * Float::MachineEpsilon);
        radiance(tempRadiance, scene, reflectionRay, WhittedContext { context.maxDepth - 1, context.refractiveIndex});
        reflectionSpectrum *= tempRadiance.radiance;
        radianceContent.radiance += reflectionSpectrum;
    }

    // Transmission
    if (&material->getRefractionTexture() != &MatteTexture::black) {

        Vector transUnitNormal = unitNormal;
        float refractiveIndex = material->getRefractiveIndex();

        // If going back to vacuum
        if (intersection.shapeIntersection.Type == Shape::Intersection::Internal) {
            transUnitNormal *= -1.f;
            refractiveIndex = 1.f;
        }

        float dot = unitDirection * transUnitNormal;
        float indexRatio = context.refractiveIndex / refractiveIndex;
        float sq = ( 1 - indexRatio * indexRatio * (1 - dot * dot) );

        BoundedRay refractionRay;
        refractionRay.setPosition(intersectionPoint);
        refractionRay.setMin(1000.f * Float::MachineEpsilon);

        Vector transmissionDirection;

        // Calculate refraction ray direction
        if (sq > 0.f) {
            transmissionDirection = indexRatio * unitDirection
                                           - (indexRatio * dot + Functions::sqrt(sq)) * transUnitNormal;
        } else {
            // Total internal reflection
            transmissionDirection = unitDirection - (2.f * dot) * transUnitNormal;
            refractiveIndex = material->getRefractiveIndex();
        }
        refractionRay.setDirection(transmissionDirection);
        RgbSpectrum refractionSpectrum = material->getRefractionTexture().GetSpectrum(textureUV)->toTristimulusSRGB();
        RadianceContent tempRadiance;
        //RgbSpectrum(1.f).subtract(refractionSpectrum, refractionSpectrum);
        radiance(tempRadiance, scene, refractionRay, WhittedContext { context.maxDepth - 1, refractiveIndex});
        refractionSpectrum *= tempRadiance.radiance;
        radianceContent.radiance += refractionSpectrum;
    }

}


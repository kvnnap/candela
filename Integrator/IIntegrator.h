//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_IINTEGRATOR_H
#define MSC_CANDELA_IINTEGRATOR_H

#include <memory>
#include "Renderer/RadianceBuffer.h"
#include "Scene/IScene.h"

namespace Candela
{
    // Integrators give back the radiance value from an intersection point
    namespace Integrator
    {
        class IIntegrator {
        public:
            virtual ~IIntegrator();

            virtual std::string toString() const;

            // The name of this integrator
            virtual std::string getName() const = 0;
            virtual void setName(const std::string& name) = 0;

            virtual void radiance(Renderer::RadianceContent& radianceContent,
                                  const Scene::IScene& scene,
                                  const Mathematics::BoundedRay& ray) = 0;
            virtual std::unique_ptr<IIntegrator> clone() const = 0;
        };

        std::ostream& operator<< (std::ostream& strm, const IIntegrator& integrator);
    }
}

#endif //MSC_CANDELA_IINTEGRATOR_H

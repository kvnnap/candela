//
// Created by kvnna on 28/06/2016.
//

#ifndef MSC_CANDELA_PATHINTEGRATOR_H
#define MSC_CANDELA_PATHINTEGRATOR_H

#include <stack>
#include "Integrator.h"
#include "Sampler/UniformSampler.h"

namespace Candela
{
    // Integrators give back the radiance value from an intersection point
    namespace Integrator
    {

        struct PathContext {
            Scene::Intersection intersection;
            Mathematics::BoundedRay ray;
            Spectrum::RgbSpectrum coefficient;
        };

        class PathIntegrator
            : public Integrator
        {
        public:
            PathIntegrator();
            void radiance(Renderer::RadianceContent& radianceContent,
                          const Scene::IScene& scene,
                          const Mathematics::BoundedRay& primaryRay) override;

            bool getExplicitDirectLighting() const;
            void setExplicitDirectLighting(bool explicitDirectLighting);
            bool getCosineWeightedDiffuseRays() const;
            void setCosineWeightedDiffuseRays(bool cosineWeightDiffuseRays);

            std::unique_ptr<IIntegrator> clone() const override;
        private:

            void recursiveRadiance(Renderer::RadianceContent& radianceContent,
                          const Scene::IScene& scene,
                          const Mathematics::BoundedRay& primaryRay, size_t depth);

            static Mathematics::Vector transformPointToBasis(const Mathematics::Vector& unitNormal, const Mathematics::Vector& point);
            static Mathematics::Vector randomRayHemisphere(const Mathematics::Vector& unitNormal, Sampler::UniformSampler& sampler);
            static Mathematics::Vector randomRayLobe(const Mathematics::Vector& unitNormal, Sampler::UniformSampler& sampler, float& probability, float n = 1.f);

            Sampler::UniformSampler sampler;
            std::stack<PathContext> intersectionStack;

            bool explicitDirectLighting;
            bool cosineWeightedDiffuseRays;
        public:

        };

    }
}


#endif //MSC_CANDELA_PATHINTEGRATOR_H

//
// Created by kvnna on 18/12/2017.
//

#ifndef CANDELA_INTEGRATOR_H
#define CANDELA_INTEGRATOR_H

#include "IIntegrator.h"

namespace Candela
{
    // Integrators give back the radiance value from an intersection point
    namespace Integrator
    {
        class Integrator
            : public IIntegrator
        {
        public:
            std::string getName() const override ;
            void setName(const std::string& name) override;
        private:
            std::string name;
        };
    }
}


#endif //CANDELA_INTEGRATOR_H

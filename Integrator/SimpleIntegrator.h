//
// Created by kvnna on 26/06/2016.
//

#ifndef MSC_CANDELA_SIMPLEINTEGRATOR_H
#define MSC_CANDELA_SIMPLEINTEGRATOR_H

#include "Integrator.h"

namespace Candela
{
    // Integrators give back the radiance value from an intersection point
    namespace Integrator
    {
        class SimpleIntegrator
            : public Integrator
        {
        public:
            void radiance(Renderer::RadianceContent& radianceContent,
                          const Scene::IScene& scene,
                          const Mathematics::BoundedRay& ray) override;

            std::unique_ptr<IIntegrator> clone() const override;
            std::string toString() const override;
        };
    }
}


#endif //MSC_CANDELA_SIMPLEINTEGRATOR_H

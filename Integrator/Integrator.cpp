//
// Created by kvnna on 18/12/2017.
//

#include "Integrator.h"

using namespace std;
using namespace Candela::Integrator;

string Integrator::getName() const {
    return name;
}

void Integrator::setName(const std::string &name) {
    this->name = name;
}

//
// Created by kvnna on 26/06/2016.
//
#include <memory>
#include <sstream>
#include <Material/BaseMaterial.h>
#include <Mathematics/Float.h>
#include "SimpleIntegrator.h"
#include "Spectrum/RgbSpectrum.h"

using namespace std;
using namespace Candela::Integrator;
using namespace Candela::Light;
using namespace Candela::Scene;
using namespace Candela::Spectrum;
using namespace Candela::Mathematics;
using namespace Candela::Material;
using namespace Candela::Renderer;

void SimpleIntegrator::radiance(RadianceContent &radianceContent,
                                const IScene &scene,
                                const BoundedRay &primaryRay) {

    Intersection intersection, shadowIntersection;
    BoundedRay pRay = primaryRay;

    // Initialise radiance - radianceContent.direct = radianceContent.indirect =
    radianceContent.radiance = 0.f;

    scene.intersect(pRay, intersection);

    //check if any intersections
    if(intersection.shapeIntersection.Type == Shape::Intersection::External)
    {
        if (intersection.primitive->isEmissive()) {
            if (intersection.shapeIntersection.Type == Shape::Intersection::External)
                intersection.primitive->getLight()->radiance(primaryRay, radianceContent.radiance);
            return;
        }

        intersection.shapeIntersection.point = pRay.getPoint(intersection.shapeIntersection.Distance);
        const Vector& intersectionPoint = intersection.shapeIntersection.point;
        for (const ILight* light : scene.getLights()){

            // Cast ray towards light source to detect occluder, if any
            BoundedRay shadowRay (intersectionPoint,
                                  light->getCentroid() - intersectionPoint);
            shadowRay.setMin(1000.f * Float::MachineEpsilon);
            shadowRay.setMax(1);

            // dot product
            float dot = intersection.shapeIntersection.Intersectant->getUnitNormal(intersection.shapeIntersection)
                        * (~shadowRay.getDirection());
            if (dot < 0) {
                continue;
            }

            // Check if we would hit the opposite face of the light source
            Vector unitLightSurfaceNormal;
            light->samplePoint(0.f, 0.f, unitLightSurfaceNormal);
            const float lightShadowDot = unitLightSurfaceNormal * -~shadowRay.getDirection();
            if (lightShadowDot < 0.f) {
                continue;
            }

            // Cast it
            if (!scene.intersect(shadowRay, shadowIntersection, light)){

                RgbSpectrum lightRgb;
                light->radiance(shadowRay, lightRgb);
                lightRgb *= intersection.primitive->getMaterial()->getDiffuseTexture().GetSpectrum(
                        intersection.shapeIntersection.Intersectant->getTextureUv(intersection.shapeIntersection)
                )->toTristimulusSRGB();
                const float shadowRayLength = !shadowRay.getDirection();
                // Also multiplying by lightShadowDot to simulate area light falloff. for point lights it would be one
                lightRgb *= dot * lightShadowDot / (shadowRayLength * shadowRayLength);

                radianceContent.radiance += lightRgb;
            }
            //radianceContent.radiance += intersection.primitive->getMaterial()->getDiffuseTexture().GetSpectrum({})->toTristimulusSRGB();
        }

    }
}

string SimpleIntegrator::toString() const {
    ostringstream sstr;
    sstr << IIntegrator::toString()
         << " - Simple Integrator";
    return sstr.str();
}

std::unique_ptr<IIntegrator> SimpleIntegrator::clone() const {
    unique_ptr<IIntegrator> integrator = make_unique<SimpleIntegrator>();
    integrator->setName(getName());
    return integrator;
}


//
// Created by kvnna on 25/06/2016.
//

#include "IIntegrator.h"
#include "sstream"

using namespace std;
using namespace Candela::Integrator;

IIntegrator::~IIntegrator() { }

string IIntegrator::toString() const {
    ostringstream sstr;
    sstr << "IIntegrator - Address = " << this
         << "\n\tName: " << getName();
    return sstr.str();
}

std::ostream& Candela::Integrator::operator <<(std::ostream& strm, const IIntegrator& integrator)
{
    return strm << integrator.toString();
}
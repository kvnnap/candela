//
// Created by kvnna on 05/11/2017.
//

#include "ConfigurationIterator.h"

using namespace Candela::Configuration;

ConfigurationItem::ConfigurationItem(const std::string &key, const ConfigurationNode &value)
        : key (key), value (value)
{}

ConfigurationIterator::ConfigurationIterator(PtCustomIterator customIterator)
        : customIterator (move(customIterator))
{}

ConfigurationIterator::ConfigurationIterator(ConfigurationIterator && other)
        : customIterator (move(other.customIterator))
{}
ConfigurationIterator::~ConfigurationIterator() {}


bool ConfigurationIterator::operator==(const ConfigurationIterator &rhs) const {
    // Using visitor pattern variant to determine equality
    return *customIterator == *rhs.customIterator;
}

bool ConfigurationIterator::operator!=(const ConfigurationIterator &rhs) const {
    return !(*this == rhs);
}

ConfigurationIterator &ConfigurationIterator::operator++() {
    ++*customIterator;
    return *this;
}

const ConfigurationItem ConfigurationIterator::operator*() {
    return **customIterator;
}

// Custom Iterator methods
ICustomIterator::~ICustomIterator() {}
bool ICustomIterator::operator==(const ObjectNodeIterator &rhs) const { return false; }
bool ICustomIterator::operator==(const ListNodeIterator &rhs) const { return false; }
bool ICustomIterator::operator==(const LiteralNodeIterator &rhs) const { return false; }

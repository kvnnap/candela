//
// Created by kvnna on 28/10/2017.
//

#include "LiteralNode.h"

using namespace std;
using namespace Candela::Configuration;

ConfigurationNodePt LiteralNode::nullNode;

const ConfigurationNodePt& LiteralNode::operator[](const string &key) const {
    return nullNode;
}

ConfigurationNodePt& LiteralNode::operator[](const std::string &key) {
    return nullNode;
}

bool LiteralNode::keyExists(const std::string &key) const {
    return false;
}

size_t LiteralNode::size() const {
    return 0;
}

bool LiteralNode::isLeaf() const {
    return true;
}

bool LiteralNode::isDictionaryBased() const {
    return false;
}

ConfigurationIterator LiteralNode::begin() const {
    return ConfigurationIterator(PtCustomIterator(new LiteralNodeIterator(*this)));
}

ConfigurationIterator LiteralNode::end() const {
    return begin();
}

/*template <>
std::string Candela::Configuration::LiteralNodeValue<bool>::operator*() const {
    return value ? "true" : "false";
}*/

template <>
std::string Candela::Configuration::LiteralNodeValue<std::string>::operator*() const {
    return value;
}

template <>
std::string Candela::Configuration::LiteralNodeValue<std::string>::toString() const {
    string ret;
    ret += '"';
    ret += **this;
    ret += '"';
    return ret;
}

// Iterator
LiteralNodeIterator::LiteralNodeIterator(const LiteralNode& node)
    : node (node)
{}

ConfigurationItem LiteralNodeIterator::operator*() {
    return ConfigurationItem(string(), node);
}

ICustomIterator &LiteralNodeIterator::operator++() {
    return *this;
}

bool LiteralNodeIterator::operator==(const ICustomIterator &rhs) const {
    return rhs == *this;
}

bool LiteralNodeIterator::operator==(const LiteralNodeIterator &rhs) const {
    return &node == &rhs.node;
}

ConfigurationNodePt Candela::Configuration::operator ""_s(const char *value, size_t size) {
    return LiteralNode::fromValue(string(value, size));
}

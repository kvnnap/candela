//
// Created by kvnna on 28/10/2017.
//

#include "ObjectNode.h"

using namespace std;
using namespace Candela::Configuration;

ObjectNodeIterator::ObjectNodeIterator(ObjectNodeMapIteratorType iterator)
    : iterator (iterator)
{}

ICustomIterator &ObjectNodeIterator::operator++() {
    ++iterator;
    return *this;
}

ConfigurationItem ObjectNodeIterator::operator*() {
    return ConfigurationItem(iterator->first, *iterator->second);
}

bool ObjectNodeIterator::operator==(const ICustomIterator &rhs) const {
    return rhs == *this;
}

bool ObjectNodeIterator::operator==(const ObjectNodeIterator &rhs) const {
    return iterator == rhs.iterator;
}

bool ObjectNode::isLeaf() const {
    return false;
}

bool ObjectNode::isDictionaryBased() const {
    return true;
}

ConfigurationIterator ObjectNode::begin() const {
    return ConfigurationIterator(PtCustomIterator(new ObjectNodeIterator(objectMap.begin())));
}

ConfigurationIterator ObjectNode::end() const {
    return ConfigurationIterator(PtCustomIterator(new ObjectNodeIterator(objectMap.end())));
}

const ConfigurationNodePt &ObjectNode::operator[](const std::string &key) const {
    return objectMap.at(key);
}

ConfigurationNodePt &ObjectNode::operator[](const std::string &key) {
    return objectMap[key];
}

bool ObjectNode::keyExists(const std::string &key) const {
    return objectMap.find(key) != objectMap.end();
}

std::string ObjectNode::toString() const {
    string returnString;
    returnString += '{';
    size_t i = 0;
    for (const auto& child : *this) {
        returnString += '"';
        returnString += child.key;
        returnString += "\": ";
        returnString += child.value.toString();
        if (++i != size()) {
            returnString += ", ";
        }
    }
    returnString += '}';
    return returnString;
}

size_t ObjectNode::size() const {
    return objectMap.size();
}

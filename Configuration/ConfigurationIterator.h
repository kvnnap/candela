//
// Created by kvnna on 05/11/2017.
//

#ifndef CANDELA_CONFIGURATIONITERATOR_H
#define CANDELA_CONFIGURATIONITERATOR_H

#include <string>
#include <memory>

namespace Candela
{
    namespace Configuration
    {
        class ConfigurationNode;

        class ConfigurationItem
        {
        public:
            ConfigurationItem(const std::string& key, const ConfigurationNode& value);
            const std::string key;
            const ConfigurationNode& value;
        };

        // List the iterators
        class ObjectNodeIterator;
        class ListNodeIterator;
        class LiteralNodeIterator;
        class ICustomIterator
        {
        public:
            virtual ~ICustomIterator();

            virtual ConfigurationItem operator*() = 0;
            virtual ICustomIterator& operator++() = 0;
            virtual bool operator==(const ICustomIterator& rhs) const = 0;
            virtual bool operator==(const ObjectNodeIterator& rhs) const;
            virtual bool operator==(const ListNodeIterator& rhs) const;
            virtual bool operator==(const LiteralNodeIterator& rhs) const;
        };

        // Iterator
        using PtCustomIterator = std::unique_ptr<ICustomIterator>;
        class ConfigurationIterator
                : public std::iterator<std::input_iterator_tag, ConfigurationItem>
        {
        private:
            PtCustomIterator customIterator;
        public:
            virtual ~ConfigurationIterator();
            ConfigurationIterator(ConfigurationIterator&& other);
            ConfigurationIterator(PtCustomIterator customIterator);

            const ConfigurationItem operator*();
            ConfigurationIterator& operator++();

            bool operator==(const ConfigurationIterator& rhs) const;
            bool operator!=(const ConfigurationIterator& rhs) const;
        };
    }
}


#endif //CANDELA_CONFIGURATIONITERATOR_H

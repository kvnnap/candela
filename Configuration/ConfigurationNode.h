//
// Created by kvnna on 21/10/2017.
//

#ifndef CANDELA_CONFIGURATIONNODE_H
#define CANDELA_CONFIGURATIONNODE_H

#include <string>
#include <iterator>
#include <vector>
#include <map>
#include <memory>
#include "ConfigurationIterator.h"

namespace Candela
{
    namespace Configuration
    {
        class ConfigurationNode;
        using ConfigurationNodePt = std::unique_ptr<ConfigurationNode>;

        class ConfigurationNode {
        public:
            virtual ~ConfigurationNode();

            // Represent lists and objects
            virtual const ConfigurationNodePt& operator[](const std::string& key) const = 0;
            virtual ConfigurationNodePt& operator[](const std::string& key) = 0;
            virtual bool keyExists(const std::string& key) const = 0;
            virtual std::string operator*() const;
            virtual std::string toString() const;

            template <class T>
            T read() const {
                T val;
                if(!read(val)) {
                    throw std::runtime_error("Configuration Node: Cannot read val using this type");
                };
                return val;
            }

            virtual bool read(std::string& dest) const;
            virtual bool read(bool& dest) const;
            virtual bool read(std::uint64_t & dest) const;
            virtual bool read(std::int64_t& dest) const;
            virtual bool read(double& dest) const;

            // size
            virtual size_t size() const = 0;

            // Is Leaf
            virtual bool isLeaf() const = 0;
            virtual bool isDictionaryBased() const = 0;

            // Return iterator
            virtual ConfigurationIterator begin() const = 0;
            virtual ConfigurationIterator end() const = 0;
        };
    }
}

#endif //CANDELA_CONFIGURATIONNODE_H

//
// Created by kvnna on 21/10/2017.
//

#include "ConfigurationNode.h"

using namespace std;
using namespace Candela::Configuration;

ConfigurationNode::~ConfigurationNode() {}

string ConfigurationNode::operator*() const {
    return string();
}

string ConfigurationNode::toString() const {
    return **this;
}

bool ConfigurationNode::read(bool &dest) const {
    return false;
}

bool ConfigurationNode::read(uint64_t &dest) const {
    return false;
}

bool ConfigurationNode::read(int64_t &dest) const {
    // Fall back to unsigned
    uint64_t temp;
    if (read(temp)) {
        const int64_t t = static_cast<int64_t>(temp);
        if (t >= 0) {
            dest = t;
            return true;
        }
    }
    return false;
}

bool ConfigurationNode::read(double &dest) const {
    // Fallback to unsigned/signed int
    union {
        int64_t signedVal;
        uint64_t unsignedVal;
    };
    if (read(unsignedVal)) {
        dest = unsignedVal;
        return true;
    } else if (read(signedVal)) {
        dest = signedVal;
        return true;
    }
    return false;
}

bool ConfigurationNode::read(std::string &dest) const {
    return false;
}

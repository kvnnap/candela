//
// Created by kvnna on 12/11/2017.
//

#ifndef CANDELA_PARSER_H
#define CANDELA_PARSER_H

#include <string>
#include "Configuration/ConfigurationNode.h"

namespace Candela
{
    namespace Configuration
    {
        namespace Parser
        {
            class Parser {
            public:
                virtual ~Parser();

                void setFileName(const std::string& fileName);
                virtual ConfigurationNodePt loadConfiguration() const = 0;

            protected:
                std::string fileName;
            };

            using ParserPt = std::unique_ptr<Parser>;
        }
    }
}


#endif //CANDELA_PARSER_H

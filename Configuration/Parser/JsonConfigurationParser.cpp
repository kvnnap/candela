//
// Created by kvnna on 12/11/2017.
//

#include <vector>
#include "JsonConfigurationParser.h"
#include "Configuration/ObjectNode.h"
#include "Configuration/ListNode.h"
#include "Configuration/LiteralNode.h"
#include "External/rapidjson/filereadstream.h"

using namespace std;
using namespace Candela::Configuration;
using namespace Candela::Configuration::Parser;
using namespace rapidjson;


ConfigurationNodePt JsonConfigurationParser::loadConfiguration() const {
    // Validate file name
    if (fileName.empty()) {
        throw invalid_argument("JsonConfigurationParser: fileName not set");
    }

    // Open config file
    FILE* fp = fopen(fileName.c_str(), "r");
    if (fp == nullptr) {
        throw runtime_error("JsonConfigurationParser: supplied file name does not exist");
    }

    // Initialise buffer on heap
    vector<char> buffer (65536);
    FileReadStream fileReadStream (fp, buffer.data(), buffer.size());
    Document document;
    document.ParseStream(fileReadStream);

    // close file and clear buffer
    buffer.clear();
    fclose(fp);

    // validate root node
    if (!document.IsObject() && !document.IsArray()) {
        throw runtime_error("JsonConfigurationParser: Invalid root - not object nor array");
    }

    // Valid root, start parsing
    return fromValue(document);
}

ConfigurationNodePt JsonConfigurationParser::fromValue(rapidjson::Value &value) const {
    ConfigurationNodePt configNode;
    if (value.IsObject()) {
        configNode = make_unique<ObjectNode>();
        for(auto& v : value.GetObject()) {
            (*configNode)[v.name.GetString()] = fromValue(v.value);
        }
    } else if (value.IsArray()) {
        ListNodePt listNode = make_unique<ListNode>();
        for(auto& v : value.GetArray()) {
            listNode->add(fromValue(v));
        }
        configNode = move(listNode);
    } else {
        switch (value.GetType()) {
            case kFalseType:
                configNode = LiteralNode::fromValue(false);
                break;
            case kTrueType:
                configNode = LiteralNode::fromValue(true);
                break;
            case kStringType:
                configNode = LiteralNode::fromValue(string(value.GetString()));
                break;
            case kNumberType:
                if (value.IsUint64()) {
                    configNode = LiteralNode::fromValue(value.GetUint64());
                } else if (value.IsInt64()){
                    configNode = LiteralNode::fromValue(value.GetInt64());
                } else {
                    configNode = LiteralNode::fromValue(value.GetDouble());
                }
                break;
            case kNullType:
            default:
                configNode = "null"_s;
                break;
        }
    }

    return configNode;
}

// Factory
ParserPt JsonConfigurationParserFactory::create() const {
    return make_unique<JsonConfigurationParser>();
}

ParserPt JsonConfigurationParserFactory::create(const ConfigurationNode &config) const {
    unique_ptr<JsonConfigurationParser> parser = make_unique<JsonConfigurationParser>();
    parser->setFileName(**config["file"]);
    return parser;
}

//
// Created by kvnna on 12/11/2017.
//

#ifndef CANDELA_JSONCONFIGURATIONPARSER_H
#define CANDELA_JSONCONFIGURATIONPARSER_H

#include <memory>
#include "Factory/Factory.h"
#include "Configuration/Parser/Parser.h"
#include "Configuration/ConfigurationNode.h"
#include "External/rapidjson/document.h"

namespace Candela
{
    namespace Configuration
    {
        namespace Parser
        {
            class JsonConfigurationParser
                : public Parser
            {
            public:
                ConfigurationNodePt loadConfiguration() const override;
            private:
                ConfigurationNodePt fromValue(rapidjson::Value& value) const;
            };

            class JsonConfigurationParserFactory
                : public Factory::Factory<Parser>
            {
            public:
                ParserPt create() const override;
                ParserPt create(const Configuration::ConfigurationNode& config) const override;
            };
        }
    }
}



#endif //CANDELA_JSONCONFIGURATIONPARSER_H

//
// Created by kvnna on 28/10/2017.
//

#ifndef CANDELA_LITERALNODE_H
#define CANDELA_LITERALNODE_H

#include "ConfigurationNode.h"
#include <string>

namespace Candela
{
    namespace Configuration
    {

        template <class T>
        class LiteralNodeValue;

        class LiteralNode
                : public ConfigurationNode
        {
        public:
            // Make literal node - like a factory method
            template <class T>
            static ConfigurationNodePt fromValue(const T& value) {
                return ConfigurationNodePt(new LiteralNodeValue<T>(value));
            }

            // Represent lists and objects
            const ConfigurationNodePt& operator[](const std::string& key) const override;
            ConfigurationNodePt& operator[](const std::string& key) override;
            bool keyExists(const std::string& key) const override;

            // size
            size_t size() const override;

            // Is Leaf
            bool isLeaf() const override ;
            bool isDictionaryBased() const override;

            // Return iterator
            ConfigurationIterator begin() const override;
            ConfigurationIterator end() const override;

            static ConfigurationNodePt nullNode;
        };

        template <class T>
        class LiteralNodeValue
                : public LiteralNode
        {
        public:
            LiteralNodeValue(const T& value) : value (value) {}
            std::string operator*() const override { return std::to_string(value); }
            std::string toString() const override { return LiteralNode::toString(); }
            bool read(T& dest) const override { dest = value; return true; }
        private:
            T value;
        };

        // Specialise member functions
        template <>
        inline std::string LiteralNodeValue<bool>::operator*() const {
			return value ? "true" : "false";
		}

        template <>
        std::string LiteralNodeValue<std::string>::operator*() const;

        template <>
        std::string LiteralNodeValue<std::string>::toString() const;

        using LiteralNodePt = std::unique_ptr<LiteralNode>;

        class LiteralNodeIterator
            : public ICustomIterator
        {
        public:
            LiteralNodeIterator(const LiteralNode& node);

            ConfigurationItem operator*() override;
            ICustomIterator& operator++() override;
            bool operator==(const ICustomIterator& rhs) const override;
            bool operator==(const LiteralNodeIterator& rhs) const override;
        private:
            const LiteralNode& node;
        };

        // Useful operators
        ConfigurationNodePt operator "" _s(const char* value, size_t size);
    }
}


#endif //CANDELA_LITERALNODE_H

//
// Created by kvnna on 28/10/2017.
//

#ifndef CANDELA_OBJECTNODE_H
#define CANDELA_OBJECTNODE_H

#include "ConfigurationNode.h"
#include <map>
#include <memory>

namespace Candela
{
    namespace Configuration
    {
        using ObjectNodeCollection = std::map<std::string, std::unique_ptr<ConfigurationNode>>;
        using ObjectNodeMapIteratorType = ObjectNodeCollection::const_iterator;

        class ObjectNode
            : public ConfigurationNode
        {
        public:

            // Represent lists and objects
            const ConfigurationNodePt& operator[](const std::string& key) const override;
            ConfigurationNodePt& operator[](const std::string& key) override;
            bool keyExists(const std::string& key) const override;
            std::string toString() const override;

            // size
            size_t size() const override;

            // Is Leaf
            bool isLeaf() const override;
            bool isDictionaryBased() const override;

            // Return iterator
            ConfigurationIterator begin() const override;
            ConfigurationIterator end() const override;
        private:
            ObjectNodeCollection objectMap;
        };

        using ObjectNodePt = std::unique_ptr<ObjectNode>;


        class ObjectNodeIterator
            : public ICustomIterator
        {
        public:

            ObjectNodeIterator(ObjectNodeMapIteratorType iterator);

            ConfigurationItem operator*() override;
            ICustomIterator& operator++() override;
            bool operator==(const ICustomIterator& rhs) const override;
            bool operator==(const ObjectNodeIterator& rhs) const override;
        private:
            ObjectNodeMapIteratorType iterator;
        };
    }
}





#endif //CANDELA_OBJECTNODE_H

//
// Created by kvnna on 28/10/2017.
//

#ifndef CANDELA_LISTNODE_H
#define CANDELA_LISTNODE_H

#include <vector>
#include <initializer_list>
#include <memory>
#include "ConfigurationNode.h"

namespace Candela
{
    namespace Configuration
    {

        using ListNodeCollection = std::vector<std::unique_ptr<ConfigurationNode>>;
        using ListNodeMapIteratorType = ListNodeCollection::const_iterator;

        class ListNode
            : public ConfigurationNode
        {
        public:

            static ConfigurationNodePt fromList(const std::initializer_list<std::string>& values);
            static ConfigurationNodePt fromList(const std::vector<std::string>& values);

            ListNode();
            ListNode(const std::vector<std::string>& values);
            ListNode(const std::initializer_list<std::string>& values);

            // add to list
            void add(ConfigurationNodePt node);
            void add(const std::string& value);

            // Represent lists and objects
            const ConfigurationNodePt& operator[](const std::string& key) const override;
            ConfigurationNodePt& operator[](const std::string& key) override;
            bool keyExists(const std::string& key) const override;
            std::string toString() const override;

            // size
            size_t size() const override;

            // Is Leaf
            bool isLeaf() const override ;
            bool isDictionaryBased() const override;

            ConfigurationIterator begin() const override;
            ConfigurationIterator end() const override;
        private:
            ListNodeCollection list;
        };

        using ListNodePt = std::unique_ptr<ListNode>;

        class ListNodeIterator
            : public ICustomIterator
        {
        public:

            ListNodeIterator(ListNodeMapIteratorType iterator);

            ConfigurationItem operator*() override;
            ICustomIterator& operator++() override;
            bool operator==(const ICustomIterator& rhs) const override;
            bool operator==(const ListNodeIterator& rhs) const override;
        private:
            ListNodeMapIteratorType iterator;
            size_t position;
        };
    }
}

#endif //CANDELA_LISTNODE_H

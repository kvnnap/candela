//
// Created by kvnna on 28/10/2017.
//

#include <vector>
#include "ListNode.h"
#include "LiteralNode.h"

using namespace std;
using namespace Candela::Configuration;

ConfigurationNodePt ListNode::fromList(const initializer_list<string>& values) {
    return ConfigurationNodePt(new ListNode(values));
}

ConfigurationNodePt ListNode::fromList(const vector<string>& values) {
    return ConfigurationNodePt(new ListNode(values));
}

ListNode::ListNode() {}

ListNode::ListNode(const vector<string> & values)
{
    for (const auto& value : values) {
        add(value);
    }
}

ListNode::ListNode(const initializer_list<string>& values)
    //: ListNode(vector<string>(values))
{
    // copying code for performance reasons
    for (const auto& value : values) {
        add(value);
    }
}

const ConfigurationNodePt& ListNode::operator[](const string &key) const {
    return list.at(static_cast<size_t>(atoi(key.c_str())));
}

ConfigurationNodePt& ListNode::operator[](const string &key) {
    // can edit item, but not add
    return list.at(static_cast<size_t>(atoi(key.c_str())));
}

bool ListNode::keyExists(const std::string &key) const {
    return static_cast<size_t>(atoi(key.c_str())) < list.size();
}

string ListNode::toString() const {
    string returnString;
    returnString += '[';
    size_t i = 0;
    for (const auto& child : *this) {
        returnString += child.value.toString();
        if (++i != size()) {
            returnString += ", ";
        }
    }
    returnString += ']';
    return returnString;
}

size_t ListNode::size() const {
    return list.size();
}

bool ListNode::isLeaf() const {
    return false;
}

bool ListNode::isDictionaryBased() const {
    return false;
}

ConfigurationIterator ListNode::begin() const {
    return ConfigurationIterator(PtCustomIterator(new ListNodeIterator(list.begin())));
}

ConfigurationIterator ListNode::end() const {
    return ConfigurationIterator(PtCustomIterator(new ListNodeIterator(list.end())));
}

void ListNode::add(ConfigurationNodePt node) {
    list.push_back(move(node));
}

void ListNode::add(const string & value) {
    add(LiteralNode::fromValue(value));
}

ListNodeIterator::ListNodeIterator(ListNodeMapIteratorType iterator)
    : iterator (iterator), position ()
{}

ConfigurationItem ListNodeIterator::operator*() {
    return ConfigurationItem(to_string(position), **iterator);
}

ICustomIterator &ListNodeIterator::operator++() {
    ++iterator;
    ++position;
    return *this;
}

bool ListNodeIterator::operator==(const ICustomIterator &rhs) const {
    return rhs == *this;
}

bool ListNodeIterator::operator==(const ListNodeIterator &rhs) const {
    return iterator == rhs.iterator;
}


/* 
 * File:   ICamera.cpp
 * Author: Kevin
 * 
 * Created on 19 March 2014, 11:06
 */

#include "Camera/ICamera.h"
#include <sstream>

using namespace Candela::Camera;

ICamera::~ICamera() {
}

std::string ICamera::toString() const {
    using namespace std;
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << "ICamera - Address = " << this
         << "\n\tName: " << getName();
    return sstr.str();
}

std::ostream& Candela::Camera::operator <<(std::ostream& strm, const ICamera& camera)
{
    return strm << camera.toString();
}
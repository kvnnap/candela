//
// Created by Kevin on 06/12/2017.
//

#ifndef CANDELA_PINHOLECAMERAFACTORY_H
#define CANDELA_PINHOLECAMERAFACTORY_H

#include "BaseCameraFactory.h"
#include "Factory/Factory.h"

namespace Candela
{
    namespace Camera
    {
        class PinholeCameraFactory
            : public BaseCameraFactory
        {
        public:
            std::unique_ptr<ICamera> create() const override;
            std::unique_ptr<ICamera> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}



#endif //CANDELA_PINHOLECAMERAFACTORY_H

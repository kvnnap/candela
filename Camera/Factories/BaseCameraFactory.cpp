//
// Created by kvnna on 29/12/2017.
//

#include "BaseCameraFactory.h"

#include "Camera/Factories/CameraPlaneFactory.h"
#include "Mathematics/Factories/VectorFactory.h"
#include "Mathematics/Factories/RectangleFactory.h"

using namespace std;
using namespace Candela::Camera;
using namespace Candela::Configuration;
using namespace Candela::Mathematics;

void BaseCameraFactory::setupBaseCamera(BaseCamera *baseCamera, const ConfigurationNode &config) {
    // Get position - float vector
    const Vector position = *VectorFactory().create(*config["Position"]);

    // Get direction - float vector
    const Vector direction = *VectorFactory().create(*config["Direction"]);

    // Get up direction - float vector
    const Vector up = *VectorFactory().create(*config["Up"]);

    // Get camera plane
    const CameraPlane cameraPlane = *CameraPlaneFactory().create(*config["CameraPlane"]);

    // Get pixelRect
    const Rectangle_I pixelDimensions = *Rectangle_I_Factory().create(*config["PixelDimensions"]);

    // unique_ptr<ICamera> pinholeCamera = make_unique<PinholeCamera>(direction, cameraPlane, pixelDimensions);
    baseCamera->setName(**config["Name"]);
    baseCamera->setFilmPlane(cameraPlane);
    baseCamera->setSensorPixelDimensions(pixelDimensions);
    baseCamera->setCameraPosition(position);
    baseCamera->lookAt(direction, up);

    if (config.keyExists("Aperture")) {
        const string apertureType = **config["Aperture"];
        baseCamera->setAperture(apertureType == "Infinitesimal" ? ICamera::Infinitesimal :
                                apertureType == "Circular" ? ICamera::Circular :
                                apertureType == "Square" ? ICamera::Square :
                                throw runtime_error("Unknown Aperture type: " + apertureType)
        );
    }

    if (config.keyExists("ApertureSize")) {
        baseCamera->setApertureSize(config["ApertureSize"]->read<double>());
    }
}

//
// Created by Kevin on 06/12/2017.
//

#ifndef CANDELA_CAMERAPLANEFACTORY_H
#define CANDELA_CAMERAPLANEFACTORY_H

#include "Camera/CameraPlane.h"
#include "Factory/Factory.h"

namespace Candela
{
    namespace Camera
    {
        class CameraPlaneFactory
                : public Factory::Factory<CameraPlane>
        {
        public:
            std::unique_ptr<CameraPlane> create() const override;
            std::unique_ptr<CameraPlane> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}


#endif //CANDELA_CAMERAPLANEFACTORY_H

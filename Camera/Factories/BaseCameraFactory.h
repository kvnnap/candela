//
// Created by kvnna on 29/12/2017.
//

#ifndef CANDELA_BASECAMERAFACTORY_H
#define CANDELA_BASECAMERAFACTORY_H

#include "Camera/BaseCamera.h"
#include "Factory/Factory.h"

namespace Candela
{
    namespace Camera
    {
        class BaseCameraFactory
                : public Factory::Factory<ICamera>
        {
        public:
            static void setupBaseCamera(BaseCamera* baseCamera, const Configuration::ConfigurationNode& config);
        };
    }
}

#endif //CANDELA_BASECAMERAFACTORY_H

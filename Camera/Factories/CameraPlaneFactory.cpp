//
// Created by Kevin on 06/12/2017.
//

#include "CameraPlaneFactory.h"
#include "Mathematics/Factories/RectangleFactory.h"

using namespace std;
using namespace Candela::Camera;
using namespace Candela::Configuration;
using namespace Candela::Mathematics;

unique_ptr<CameraPlane> CameraPlaneFactory::create() const {
    return unique_ptr<CameraPlane>();
}

unique_ptr<CameraPlane> CameraPlaneFactory::create(const ConfigurationNode &config) const {
    const Rectangle_F cameraPlaneRect = *Rectangle_F_Factory().create(*config["Dimensions"]);
    const float cpDistance = static_cast<float>(config["Distance"]->read<double>());
    return make_unique<CameraPlane>(cameraPlaneRect, cpDistance);
}

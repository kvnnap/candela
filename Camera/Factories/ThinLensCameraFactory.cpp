//
// Created by kvnna on 07/01/2018.
//

#include "ThinLensCameraFactory.h"
#include "Camera/ThinLensCamera.h"

using namespace std;
using namespace Candela::Camera;
using namespace Candela::Configuration;
using namespace Candela::Mathematics;


unique_ptr<ICamera> ThinLensCameraFactory::create() const {
    return unique_ptr<ICamera>();
}

unique_ptr<ICamera> ThinLensCameraFactory::create(const ConfigurationNode &config) const {
    unique_ptr<ThinLensCamera> thinLensCamera = make_unique<ThinLensCamera>(Vector::UnitZ, CameraPlane(), Rectangle_I());
    setupBaseCamera(thinLensCamera.get(), config);

    // Set focal length
    thinLensCamera->setFocalLength(static_cast<float>(config["FocalLength"]->read<double>()));

    // Set F-Number (optional) - this overrides the aperture
    if (config.keyExists("FNumber")) {
        thinLensCamera->setFNumber(static_cast<float>(config["FNumber"]->read<double>()));
    }

    // Changes the film-plane distance so as to put an object in focus
    // TODO: Change this to allow change of focal length instead!
    if (config.keyExists("ObjectPlaneDistance")) {
        thinLensCamera->setObjectPlaneDistance(static_cast<float>(config["ObjectPlaneDistance"]->read<double>()));
    }

    return thinLensCamera;
}
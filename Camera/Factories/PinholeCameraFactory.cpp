//
// Created by Kevin on 06/12/2017.
//

#include "Camera/PinholeCamera.h"
#include "PinholeCameraFactory.h"

using namespace std;
using namespace Candela::Camera;
using namespace Candela::Configuration;
using namespace Candela::Mathematics;


unique_ptr<ICamera> PinholeCameraFactory::create() const {
    return unique_ptr<PinholeCamera>();
}

unique_ptr<ICamera> PinholeCameraFactory::create(const ConfigurationNode &config) const {
    unique_ptr<PinholeCamera> pinholeCamera = make_unique<PinholeCamera>(Vector::UnitZ, CameraPlane(), Rectangle_I());
    setupBaseCamera(pinholeCamera.get(), config);
    return pinholeCamera;
}

//
// Created by kvnna on 29/12/2017.
//

#include "RealPinholeCameraFactory.h"
#include "Camera/RealPinholeCamera.h"

using namespace std;
using namespace Candela::Camera;
using namespace Candela::Configuration;
using namespace Candela::Mathematics;


unique_ptr<ICamera> RealPinholeCameraFactory::create() const {
    return unique_ptr<ICamera>();
}

unique_ptr<ICamera> RealPinholeCameraFactory::create(const ConfigurationNode &config) const {
    unique_ptr<RealPinholeCamera> pinholeCamera = make_unique<RealPinholeCamera>(Vector::UnitZ, CameraPlane(), Rectangle_I());
    setupBaseCamera(pinholeCamera.get(), config);
    return pinholeCamera;
}
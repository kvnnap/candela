//
// Created by kvnna on 07/01/2018.
//

#ifndef CANDELA_THINLENSCAMERAFACTORY_H
#define CANDELA_THINLENSCAMERAFACTORY_H

#include "BaseCameraFactory.h"
#include "Factory/Factory.h"

namespace Candela
{
    namespace Camera
    {
        class ThinLensCameraFactory
                : public BaseCameraFactory
        {
        public:
            std::unique_ptr<ICamera> create() const override;
            std::unique_ptr<ICamera> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}

#endif //CANDELA_THINLENSCAMERAFACTORY_H

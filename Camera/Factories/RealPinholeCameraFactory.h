//
// Created by kvnna on 29/12/2017.
//

#ifndef CANDELA_REALPINHOLECAMERAFACTORY_H
#define CANDELA_REALPINHOLECAMERAFACTORY_H

#include "BaseCameraFactory.h"
#include "Factory/Factory.h"

namespace Candela
{
    namespace Camera
    {
        class RealPinholeCameraFactory
                : public BaseCameraFactory
        {
        public:
            std::unique_ptr<ICamera> create() const override;
            std::unique_ptr<ICamera> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}

#endif //CANDELA_REALPINHOLECAMERAFACTORY_H

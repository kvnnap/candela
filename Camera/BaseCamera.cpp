/* 
 * File:   IMappedCamera.cpp
 * Author: Kevin
 * 
 * Created on 29 March 2014, 11:35
 */

#include "BaseCamera.h"
#include "Mathematics/Funtions.h"
#include <stdexcept>
#include <sstream>
#include <Mathematics/Constants.h>

using namespace Candela;
using namespace Candela::Camera;
using namespace Candela::Mathematics;

BaseCamera::BaseCamera(const Vector& direction,
        const CameraPlane& cp, Aperture aperture, const Rectangle_I& pixelRect)
        : filmPlane_ ( cp ), aperture_ ( aperture ), apertureSize_ ( ), pixelRect_ ( pixelRect )
{
    lookAt(direction);
    setRatio();
}

void BaseCamera::changeListener() {}

void BaseCamera::setCameraPosition(const Vector& position) {
    position_ = position;
    changeListener();
}

void BaseCamera::setFilmPlaneDistance(float distance) {
    filmPlane_.distance = distance;
    changeListener();
}

void BaseCamera::lookAt(const Vector& direction, const Vector& up) {
    //set orthonormal basis
    w_ = ~direction;
    u_ = -~(up ^ direction);
    v_ = -~(w_ ^ u_);
    changeListener();
}

void BaseCamera::setAperture(ICamera::Aperture aperture) {
    aperture_ = aperture;
}

void BaseCamera::setApertureSize(float apertureSize) {
    if(aperture_ != Infinitesimal)
    {
        apertureSize_ = apertureSize;
        changeListener();
    }
}

const CameraPlane& BaseCamera::getFilmPlane() const {
    return filmPlane_;
}

const ICamera::Aperture BaseCamera::getAperture() const {
    return aperture_;
}

float BaseCamera::getApertureSize() const {
    return apertureSize_;
}

const CameraPlane& BaseCamera::getObjectPlane() const {
    return filmPlane_;
}

const std::string &BaseCamera::getName() const {
    return name;
}

void BaseCamera::setName(const std::string &name) {
    this->name = name;
}

////////////////

void BaseCamera::setSensorPixelDimensions(const Rectangle_I &pixelRect) {
    pixelRect_ = pixelRect;
    setRatio();
}

void BaseCamera::setRatio() {
    filmToPixelRatioX_ = filmPlane_.rect.getWidth() / pixelRect_.getWidth();
    filmToPixelRatioY_ = -filmPlane_.rect.getHeight() / pixelRect_.getHeight();
}

//overridden
void BaseCamera::setFilmPlane(const CameraPlane& cp) {
    filmPlane_ = cp;
    changeListener();
    setRatio();
}

void BaseCamera::getRay(float x, float y, BoundedRay& ray) const {
    getRay(x, y, 0.f, 0.f, ray);
}

void BaseCamera::getRayPixel(int x, int y, BoundedRay& ray) const {
    getRay(filmPlane_.rect.left + (x + 0.5f) * filmToPixelRatioX_,
           filmPlane_.rect.top  + (y + 0.5f) * filmToPixelRatioY_, ray);
}

void BaseCamera::getRayPixel(int x, int y, float pixelVarX, float pixelVarY, BoundedRay& ray) const {
    getRay(filmPlane_.rect.left + (x + pixelVarX) * filmToPixelRatioX_,
           filmPlane_.rect.top  + (y + pixelVarY) * filmToPixelRatioY_, ray);
}

void BaseCamera::getRayPixel(int x, int y, float pixelVarX, float pixelVarY, float apertureVarX, float apertureVarY, BoundedRay& ray) const {
    if(aperture_ == Infinitesimal){
        getRayPixel(x, y, pixelVarX, pixelVarY, ray);
    }else
    {
        if(apertureSize_ == 0.f)
        {
            if(apertureVarX == 0.f && apertureVarY == 0.f)
            {
                //be graceful
                getRayPixel(x, y, pixelVarX, pixelVarY, ray);
            }else
            {
                //something is wrong, throw exception
                throw std::runtime_error("Aperture is enabled, but aperture size is zero and aperture variations other than zero have been inputted.");
            }
        }else
        {
            // Map to circle
            if (aperture_ == Circular) {
                const float sqrtR = 0.5f * Functions::sqrt(apertureVarX);
                const float theta = apertureVarY * 2.f * Constants::Pi;
                apertureVarX = sqrtR * Functions::cos(theta);
                apertureVarY = sqrtR * Functions::sin(theta);
            } else {
                apertureVarX -= 0.5f;
                apertureVarY -= 0.5f;
            }

            //calculate proper stuff
            getRay(filmPlane_.rect.left + (x + pixelVarX) * filmToPixelRatioX_,
                   filmPlane_.rect.top +  (y + pixelVarY) * filmToPixelRatioY_,
                    apertureSize_ * apertureVarX,
                    apertureSize_ * apertureVarY, ray);
        }
    }
}

std::string BaseCamera::toString() const {
    using namespace std;
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << ICamera::toString()
         << "\n\tOrigin: " << position_
         << "\n\tOrthonormal: " << u_ << "\n\t" << v_ << "\n\t" << w_
         << "\n\tFilm Plane: " << filmPlane_
         << "\n\tAperture Type: " << aperture_
         << "\n\tAperture Size: " << apertureSize_
         << "\n\tPixel Rectangle: " << pixelRect_
         << "\n\tFilm to Pixel Ratio = (" << filmToPixelRatioX_ << ", " << filmToPixelRatioY_
            << ")"
         ;
    return sstr.str();
}

const Rectangle_I& BaseCamera::getSensorPixelDimensions() const {
    return pixelRect_;
}




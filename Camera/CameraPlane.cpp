/* 
 * File:   CameraPlane.cpp
 * Author: Kevin
 * 
 * Created on 26 March 2014, 13:55
 */

#include "Camera/CameraPlane.h"
#include <sstream>

using namespace Candela::Camera;
using namespace Candela::Mathematics;

CameraPlane::CameraPlane() 
        : distance ()
{
}

CameraPlane::CameraPlane(const Rectangle_F& pRect, float pDistance) 
        : rect ( pRect ), distance ( pDistance )
{
}

std::string CameraPlane::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "CameraPlane - Address = " << this
         << "\n\t" << rect
         << "\n\tDistance: " << distance;
    return sstr.str();
}

std::ostream& Candela::Camera::operator<<(std::ostream& strm, const CameraPlane& cameraPlane)
{
    return strm << cameraPlane.toString();
}
/* 
 * File:   ThinLensCamera.cpp
 * Author: Kevin
 * 
 * Created on 26 March 2014, 21:38
 */

#include "Camera/ThinLensCamera.h"
#include "Mathematics/Float.h"
#include <sstream>

using namespace Candela::Camera;
using namespace Candela::Mathematics;

ThinLensCamera::ThinLensCamera(const Vector& direction, 
        const CameraPlane& cp, const Rectangle_I& pixelRect)
        : BaseCamera(direction, cp, ICamera::Square, pixelRect)
        , focalLength_ ( 50E-3f ), fNumber_ ( 5.6f )
{
    update();
}

void ThinLensCamera::update() {
    apertureSize_ = focalLength_ / fNumber_;
    magnification_ = focalLength_ 
                         / (filmPlane_.distance - focalLength_);
    objectPlane_.distance = filmPlane_.distance * magnification_;
    
    objectPlane_.rect.setRect(filmPlane_.rect.left * magnification_,
                              filmPlane_.rect.right * magnification_,
                              filmPlane_.rect.bottom * magnification_,
                              filmPlane_.rect.top * magnification_);
    objectPlaneAperture_ = position_ + w_ * objectPlane_.distance;
}


void ThinLensCamera::setFocalLength(float focalLength) {
    focalLength_ = focalLength;
    //update();
    changeListener();
}

void ThinLensCamera::setFNumber(float fNumber) {
    fNumber_ = fNumber;
    //update();
    changeListener();
}

void ThinLensCamera::setApertureSize(float apertureSize) 
{
    fNumber_ = focalLength_ / apertureSize;
    changeListener();
}

void ThinLensCamera::setObjectPlaneDistance(float distance) {
    setFilmPlaneDistance(distance * (focalLength_ / (distance - focalLength_)));
}

void ThinLensCamera::changeListener() {
    BaseCamera::changeListener();
    update();
}

void ThinLensCamera::getRay(float x, float y, BoundedRay& ray) const {
    Vector target = objectPlaneAperture_ + (u_ * x + v_ * y) * magnification_;
    ray.setRay(position_, target - position_);
    ray.setMin(0.f);
    ray.setMax(Float::Infinity);
}

void ThinLensCamera::getRay(float x, float y, float apertureVarX, float apertureVarY, BoundedRay& ray) const {
    Vector origin = position_ + u_ * apertureVarX
                         + v_ * apertureVarY;
    Vector target = objectPlaneAperture_ + (u_ * x + v_ * y) * magnification_;
    ray.setRay(origin, target - origin);
    ray.setMin(0.f);
    ray.setMax(Float::Infinity);
}


std::string ThinLensCamera::toString() const {
    using namespace std;
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << BaseCamera::toString()
         << "\n\tThinLens Camera: " << this
         << "\n\tObject Plane: " << objectPlane_
         << "\n\tObject Plane Corner: " << objectPlaneAperture_
         << "\n\tFocal Length: " << focalLength_ << " F-Number: " << fNumber_
         << "\n\tAperture Size: " << apertureSize_ << " Magnification: " << magnification_
         ;
    return sstr.str();
}

/* 
 * File:   ICamera.h
 * Author: Kevin
 *
 * Created on 19 March 2014, 11:06
 */

#ifndef ICAMERA_H
#define	ICAMERA_H

#include "Mathematics/Vector3.h"
#include "Mathematics/BoundedRay.h"

#include "CameraPlane.h"

namespace Candela
{
    namespace Camera
    {
        class ICamera {
        public:

            enum Aperture{
                Infinitesimal = 0, //Ideal
                Circular = 1,
                Square = 2 //This will mostly be used
            };

            virtual ~ICamera();

            //Describes this camera
            virtual std::string toString() const;

            virtual const std::string& getName() const = 0;
            virtual void setName(const std::string& name) = 0;

            /**
             * Sets the position of the centre of the Aperture of the camera.
             * @param position The position of the camera
             */
            virtual void setCameraPosition(const Mathematics::Vector& position) = 0;

            /**
             * Sets the film plane distance (a.k.a focal length) of the camera
             * @param distance The distance between the sensor and the lens
             */
            virtual void setFilmPlaneDistance(float distance) = 0;

            /**
             * Sets the aperture size of the lens (diamater)
             * @param apertureSize The aperture size of the lens
             */
            virtual void setApertureSize(float apertureSize) = 0;

            /**
             * Set the direction of where the camera is pointing at
             * along with its orientation
             * @param direction The direction where the camera is looking at
             * @param up The up vector rotates the camera about its direction vector
             */
            virtual void lookAt(const Mathematics::Vector& direction,
                        const Mathematics::Vector& up = Mathematics::Vector::UnitY) = 0;

            /**
             * Get the image sensor dimensions and distance from the lens
             * @return The image sensor dimensions and distance from the lens
             */
            virtual const CameraPlane& getFilmPlane() const = 0;

            /**
             * Get the type of Aperture used by this lens
             * @return The type of Aperture used by this lens
             */
            virtual const Aperture getAperture() const = 0;

            /**
             * The size of the lens aperture
             * @return the size of the lens aperture
             */
            virtual float getApertureSize() const = 0;

            /**
             * Get the size of the Rectangular Camera Sensor in pixels
             * @return The size of the Rectangle Camera Sensor in pixels
             */
            virtual const Mathematics::Rectangle_I& getSensorPixelDimensions() const = 0;

            /**
             * No sampling is assumed, the ray will trace the scene to finally recover
             * the colours in the (x,y) point of the pixel plane
             * @param x - x coordinate on the pixel plane
             * @param y - y coordinate on the pixel plane
             * @param ray - The ray to be traced
             */
            virtual void getRayPixel(int x, int y, Mathematics::BoundedRay& ray) const = 0;

            /**
             * Pixel sampling is assumed, the ray will trace the scene to finally recover
             * the colours in the (x + pixelVarX,y + pixelVarY) point of the film plane
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param pixelVarX - A variation of x bounded from 0 to 1
             * @param pixelVarY - A variation of y bounded from 0 to 1
             * @param ray - The ray to be traced
             */
            virtual void getRayPixel(int x, int y, float pixelVarX, float pixelVarY,
                             Mathematics::BoundedRay& ray) const = 0;

            /**
             * Pixel and aperture sampling are assumed. The ray will trace the scene to finally recover
             * the colours in the (x + pixelVarX,y + pixelVarY) point of the film plane, which will
             * also pass from (centreX + apertureVarX, centreY + apertureVarY) of aperture
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param pixelVarX - A variation of x bounded from 0 to 1
             * @param pixelVarY - A variation of y bounded from 0 to 1
             * @param apertureVarX - A variation of x bounded from 0 to 1
             * @param apertureVarY - A variation of y bounded from 0 to 1
             * @param ray - The ray to be traced
             */
            virtual void getRayPixel(int x, int y, float pixelVarX, float pixelVarY,
                             float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const = 0;
        };
        
        std::ostream& operator<< (std::ostream& strm, const ICamera& camera);
    }
}

#endif	/* ICAMERA_H */


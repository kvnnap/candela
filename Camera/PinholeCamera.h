/* 
 * File:   PinholeCamera.h
 * Author: Kevin
 *
 * Created on 25 March 2014, 21:12
 */

#ifndef PINHOLECAMERA_H
#define	PINHOLECAMERA_H

#include "BaseCamera.h"

namespace Candela
{
    namespace Camera
    {
        class PinholeCamera 
                : public BaseCamera
        {
        public:
            PinholeCamera(const Mathematics::Vector& direction, 
                    const CameraPlane& cp, const Mathematics::Rectangle_I& pixelRect);
            
            void changeListener() override;
            
            void getRay(float x, float y, Mathematics::BoundedRay& ray) const override;
            void getRay(float x, float y,
                                float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const override;
            
            std::string toString() const override;
        protected:
            PinholeCamera(const Mathematics::Vector& direction, 
                    const CameraPlane& cp, const Mathematics::Rectangle_I& pixelRect, 
                    BaseCamera::Aperture aperture);
        protected:
            Mathematics::Vector objectPlaneAperture_;
        };
    }
}



#endif	/* PINHOLECAMERA_H */


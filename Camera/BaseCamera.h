/* 
 * File:   IMappedCamera.h
 * Author: Kevin
 *
 * Created on 29 March 2014, 11:35
 */

#ifndef IMAPPEDCAMERA_H
#define	IMAPPEDCAMERA_H

#include "ICamera.h"
#include "Mathematics/Rectangle.h"

namespace Candela
{
    namespace Camera
    {
        class BaseCamera
                : public ICamera
        {
        public:
            BaseCamera(const Mathematics::Vector& direction, const CameraPlane& cp,
                          ICamera::Aperture aperture, const Mathematics::Rectangle_I& pixelRect);

            const std::string& getName() const override;
            void setName(const std::string& name) override;

            //Describes this camera
            std::string toString() const override;

            virtual void changeListener();

            void lookAt(const Mathematics::Vector& direction,
                        const Mathematics::Vector& up = Mathematics::Vector::UnitY) override;
            virtual void setCameraPosition(const Mathematics::Vector& position) override;
            virtual void setFilmPlaneDistance(float distance) override;
            virtual void setApertureSize(float apertureSize) override;
            virtual void setAperture(Aperture aperture);
            virtual void setFilmPlane(const CameraPlane& cp);
            virtual void setSensorPixelDimensions(const Mathematics::Rectangle_I &pixelRect);

            const CameraPlane& getFilmPlane() const override;
            const Aperture getAperture() const override;
            float getApertureSize() const override;
            const Mathematics::Rectangle_I& getSensorPixelDimensions() const override;
            virtual const CameraPlane& getObjectPlane() const;

            /**
             * No sampling is assumed, the ray will trace the scene to finally recover
             * the colours in the (x,y) point of the film plane
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param ray - The ray to be traced
             */
            virtual void getRay(float x, float y, Mathematics::BoundedRay& ray) const;

            /**
             * Pixel sampling is assumed, the ray will trace the scene to finally recover
             * the colours in the (x + pixelVarX,y + pixelVarY) point of the film plane
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param pixelVarX - A small change in x to vary from original x position
             * @param pixelVarY - - A small change in y to vary from original x position
             * @param ray - The ray to be traced
             */
//            virtual void getRay(float x, float y, float pixelVarX, float pixelVarY,
//                                Mathematics::BoundedRay& ray) const;

            /**
             * Pixel and aperture sampling are assumed. The ray will trace the scene to finally recover
             * the colours in the (x + pixelVarX,y + pixelVarY) point of the film plane, which will
             * also pass from (centreX + apertureVarX, centreY + apertureVarY) of aperture
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param apertureVarX - A small change in x to vary from original centre x position of aperture
             * @param apertureVarY - A small change in y to vary from original centre y position of aperture
             * @param ray
             */
            virtual void getRay(float x, float y,
                                float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const = 0;

            /**
             * No sampling is assumed, the ray will trace the scene to finally recover
             * the colours in the (x,y) point of the pixel plane
             * @param x - x coordinate on the pixel plane
             * @param y - y coordinate on the pixel plane
             * @param ray - The ray to be traced
             */
            void getRayPixel(int x, int y, Mathematics::BoundedRay& ray) const override;
            
            /**
             * Pixel sampling is assumed, the ray will trace the scene to finally recover
             * the colours in the (x + pixelVarX,y + pixelVarY) point of the film plane
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param pixelVarX - A variation of x bounded from 0 to 1
             * @param pixelVarY - A variation of y bounded from 0 to 1
             * @param ray - The ray to be traced
             */
            void getRayPixel(int x, int y, float pixelVarX, float pixelVarY, 
                                Mathematics::BoundedRay& ray) const override;
            
            /**
             * Pixel and aperture sampling are assumed. The ray will trace the scene to finally recover
             * the colours in the (x + pixelVarX,y + pixelVarY) point of the film plane, which will
             * also pass from (centreX + apertureVarX, centreY + apertureVarY) of aperture
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param pixelVarX - A variation of x bounded from 0 to 1
             * @param pixelVarY - A variation of y bounded from 0 to 1
             * @param apertureVarX - A variation of x bounded from 0 to 1
             * @param apertureVarY - A variation of y bounded from 0 to 1
             * @param ray - The ray to be traced
             */
            void getRayPixel(int x, int y, float pixelVarX, float pixelVarY, 
                                float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const override;

        protected:

            // The name of this camera
            std::string name;

            // Camera position
            Mathematics::Vector position_;

            // The filmPlane represents the image sensor
            CameraPlane filmPlane_;

            // The Aperture type and size
            Aperture aperture_;
            float apertureSize_;

            // Orthonormal set - u,v represent the x,y vectors
            // of the image sensor. w represents direction
            Mathematics::Vector u_, v_, w_;

        private:
            
            void setRatio();
            
            Mathematics::Rectangle_I pixelRect_;
            float filmToPixelRatioX_, filmToPixelRatioY_;
        };
    }
}

#endif	/* IMAPPEDCAMERA_H */


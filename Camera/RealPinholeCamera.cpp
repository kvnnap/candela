/* 
 * File:   RealPinholeCamera.cpp
 * Author: Kevin
 * 
 * Created on 30 March 2014, 15:07
 */

#include "Camera/RealPinholeCamera.h"
#include "Mathematics/Float.h"
#include <sstream>

using namespace Candela::Camera;
using namespace Candela::Mathematics;

RealPinholeCamera::RealPinholeCamera(const Vector& direction, 
        const CameraPlane& cp, const Rectangle_I& pixelRect)
        : PinholeCamera(direction, cp, pixelRect, ICamera::Square)
{
}

void RealPinholeCamera::getRay(float x, float y,
        float apertureVarX, float apertureVarY, BoundedRay& ray) const 
{
    Vector target = position_ + u_ * apertureVarX 
                         + v_ * apertureVarY;
    //invert image
    //x = filmPlane_.rect.getWidth() - x;
    //y = - y - filmPlane_.rect.getHeight();
    x = filmPlane_.rect.left + filmPlane_.rect.right - x;
    y = filmPlane_.rect.bottom + filmPlane_.rect.top - y;
    Vector origin = position_ + u_ * x +
                                v_ * y -
                                w_ * filmPlane_.distance;
    ray.setRay(origin, target - origin);
    ray.setMin(1.f);
    ray.setMax(Float::Infinity);
}

std::string RealPinholeCamera::toString() const {
    using namespace std;
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << PinholeCamera::toString()
         << "\n\tReal Pinhole Camera: " << this;
    return sstr.str();
}


/* 
 * File:   RealPinholeCamera.h
 * Author: Kevin
 *
 * Created on 30 March 2014, 15:07
 */

#ifndef REALPINHOLECAMERA_H
#define	REALPINHOLECAMERA_H

#include "PinholeCamera.h"

namespace Candela
{
    namespace Camera
    {

        class RealPinholeCamera 
                : public PinholeCamera
        {
        public:
            RealPinholeCamera(const Mathematics::Vector& direction, 
                            const CameraPlane& cp, const Mathematics::Rectangle_I& pixelRect);
            
            void getRay(float x, float y,
                                float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const override;
            
            std::string toString() const override ;
        private:

        };

    }
}

#endif	/* REALPINHOLECAMERA_H */


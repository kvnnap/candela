/* 
 * File:   ThinLensCamera.h
 * Author: Kevin
 *
 * Created on 26 March 2014, 21:38
 */

#ifndef THINLENSCAMERA_H
#define	THINLENSCAMERA_H

#include "BaseCamera.h"

namespace Candela
{
    namespace Camera
    {
        class ThinLensCamera 
                : public BaseCamera
        {
        public:
            ThinLensCamera(const Mathematics::Vector& direction, 
                    const CameraPlane& cp, const Mathematics::Rectangle_I& pixelRect);
            
            void setFocalLength(float focalLength);
            void setFNumber(float fNumber);
            void setApertureSize(float apertureSize) override;
            void setObjectPlaneDistance(float distance);
            
            void changeListener() override;
            
            void getRay(float x, float y, Mathematics::BoundedRay& ray) const override;
            void getRay(float x, float y,
                                float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const override;
            
            /*override integer getRay methods to obtain more precision if neccessary*/
            
            std::string toString() const;
            
        private:
            
            void update();
            
            CameraPlane objectPlane_;
            Mathematics::Vector objectPlaneAperture_;
            float focalLength_;
            float fNumber_;
            float magnification_;
        }; 
    }
}


#endif	/* THINLENSCAMERA_H */


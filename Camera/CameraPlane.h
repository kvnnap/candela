/* 
 * File:   CameraPlane.h
 * Author: Kevin
 *
 * Created on 26 March 2014, 13:55
 */

#ifndef CAMERAPLANE_H
#define	CAMERAPLANE_H

#include "Mathematics/Rectangle.h"
#include <string>

namespace Candela
{
    namespace Camera
    {
        class CameraPlane {
        public:
            CameraPlane();
            CameraPlane(const Mathematics::Rectangle_F& pRect, float pDistance);
            
            std::string toString() const;
            
            Mathematics::Rectangle_F rect;
            float distance;
        };
        
        std::ostream& operator<< (std::ostream& strm, const CameraPlane& cameraPlane);
    }
}


#endif	/* CAMERAPLANE_H */


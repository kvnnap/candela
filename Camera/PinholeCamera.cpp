/* 
 * File:   PinholeCamera.cpp
 * Author: Kevin
 * 
 * Created on 25 March 2014, 21:12
 */

#include "Camera/PinholeCamera.h"
#include "Mathematics/Float.h"
#include <sstream>

using namespace Candela::Camera;
using namespace Candela::Mathematics;

PinholeCamera::PinholeCamera(const Vector& direction, 
                             const CameraPlane& cp, const Rectangle_I& pixelRect)
        : BaseCamera(direction, cp, ICamera::Infinitesimal, pixelRect)
{
    changeListener();
}

PinholeCamera::PinholeCamera(const Vector& direction, const CameraPlane& cp, 
        const Rectangle_I& pixelRect, ICamera::Aperture aperture)
        : BaseCamera(direction, cp, aperture, pixelRect)
{
    changeListener();
}


void PinholeCamera::changeListener() {
    BaseCamera::changeListener();
    objectPlaneAperture_ = position_ + w_ * filmPlane_.distance;
}

void PinholeCamera::getRay(float x, float y, BoundedRay& ray) const {
    Vector target = objectPlaneAperture_ + u_ * x + v_ * y;
    ray.setRay(position_, target - position_);
    ray.setMin(0.f);
    ray.setMax(Float::Infinity);
}

void PinholeCamera::getRay(float x, float y, float apertureVarX, float apertureVarY, BoundedRay& ray) const {
    getRay(x, y, ray);
}


std::string PinholeCamera::toString() const {
    using namespace std;
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << BaseCamera::toString()
         << "\n\tIdeal Pinhole Camera: " << this
         << "\n\tObject Plane Corner: " << objectPlaneAperture_
         ;
    return sstr.str();
}



//
// Created by kvnna on 02/07/2016.
//

#ifndef MSC_CANDELA_KDTREEMESH_H
#define MSC_CANDELA_KDTREEMESH_H

#include <memory>
#include <set>
#include "Mesh.h"
#include "Shape/AxisAlignedBoundingBox.h"

namespace Candela
{
    namespace Mesh
    {

        struct KDTreeMeshNode
        {
            KDTreeMeshNode();
            std::unique_ptr<std::vector<const Shape::IShape*>> ShapeList;
            std::unique_ptr<KDTreeMeshNode> LeftChild;
            std::unique_ptr<KDTreeMeshNode> RightChild;
            float Partition;
            uint8_t Axis;
        };

        class IKdTreeVisitor{
        public:
            virtual void print() const {};
            virtual void visit(const KDTreeMeshNode& kdTreeMeshNode) = 0;
        };

        class StatisticsPrinter
                : public IKdTreeVisitor {
        public:
            StatisticsPrinter(size_t shapesCount);
            void print() const override;
            void visit(const KDTreeMeshNode& kdTreeMeshNode) override;

            size_t minShapesInNode;
            size_t maxShapesInNode;
            size_t leafNodes;
            size_t leafShapeCount;

        private:
            size_t allShapesCount;
            std::set<const Shape::IShape*> shapes;
        };

        struct StackElem {
            KDTreeMeshNode * node;
            float a, b;
        };

        class KdTreeMesh
            : public Mesh
        {
        public:

            enum SplittingStrategy {
                Mean,
                Median,
                SAH // Surface Area Heuristic
            };

            KdTreeMesh(size_t p_maxDepth, size_t p_maxShapesInLeaf, SplittingStrategy p_splittingStrategy);

            /* IShape */
            bool intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection) const override;
            float getSurfaceArea() const override;
            void getBounds(Shape::AxisAlignedBoundingBox& aabb) const override;
            Mathematics::Vector getUnitNormal(const Shape::Intersection& intersection) const override;
            Mathematics::Vector getCentroid() const override;


            /* IBoundingShape */
            float getVolume() const override;
            void contain(const Shape::IShape& shape) override;
            void contain(const std::vector<const Shape::IShape*>& shapes) override;


            /* IMesh */
            bool intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection, Shape::IShape* excludedShape) const override;
            void add(const std::vector<const Shape::IShape*>& shapes) override;
            void add(const Shape::IShape& shape) override;
            bool remove(const Shape::IShape& shape) override;
            void clear() override;
            std::string toString() const override;

            /* KdTreeMesh */
            bool intersectSimple(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection) const;
            static KDTreeMeshNode * getNodeFromPoint(KDTreeMeshNode * node,
                                                     Shape::AxisAlignedBoundingBox & aabb,
                                                     const Mathematics::BoundedRay& ray);

        private:

            void build(KDTreeMeshNode* node,
                       const std::vector<const Shape::IShape*>& shapes,
                       const Shape::AxisAlignedBoundingBox& aabb,
                       size_t depth);

            // Splitting functions
            float split(const std::vector<const Shape::IShape*>& shapes,
                        const Shape::AxisAlignedBoundingBox& aabb, uint8_t axis);
            static float splitMean(const Shape::AxisAlignedBoundingBox& aabb,
                              uint8_t axis);
            static std::vector<float> generateSplittingPlanes(const std::vector<const Shape::IShape*>& shapes,
                                                              const Shape::AxisAlignedBoundingBox& aabb,
                                                              uint8_t axis, bool removeDuplicates);
            static size_t countShapesInVoxel(const std::vector<const Shape::IShape*>& shapes,
                                             const Shape::AxisAlignedBoundingBox& aabb);
            static float splitMedian(const std::vector<const Shape::IShape*>& shapes,
                              const Shape::AxisAlignedBoundingBox& aabb,
                              uint8_t axis);
            static float splitSAH(const std::vector<const Shape::IShape*>& shapes,
                              const Shape::AxisAlignedBoundingBox& aabb,
                              uint8_t axis);

            // Statistics and integrity checks
            static void visitTree(std::unique_ptr<KDTreeMeshNode>& root, IKdTreeVisitor& visitor);

            std::unique_ptr<KDTreeMeshNode> root;
            Shape::AxisAlignedBoundingBox bounds;
            size_t maxDepth;
            size_t maxShapesInLeaf;
            SplittingStrategy splittingStrategy;
        };

    }
}




#endif //MSC_CANDELA_KDTREEMESH_H

//
// Created by Kevin on 18/12/2017.
//

#include "LinearMeshFactory.h"

using namespace std;
using namespace Candela::Mesh;
using namespace Candela::Factory;
using namespace Candela::Configuration;

unique_ptr<IMesh> LinearMeshFactory::create() const {
    return make_unique<LinearMesh>();
}

unique_ptr<IMesh> LinearMeshFactory::create(const ConfigurationNode &config) const {
    unique_ptr<IMesh> mesh = make_unique<LinearMesh>();
    mesh->setName(**config["Name"]);
    return mesh;
}

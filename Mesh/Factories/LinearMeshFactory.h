//
// Created by Kevin on 18/12/2017.
//

#ifndef CANDELA_LINEARMESHFACTORY_H
#define CANDELA_LINEARMESHFACTORY_H

#include "Factory/Factory.h"
#include "Mesh/LinearMesh.h"

namespace Candela
{
    namespace Mesh
    {
        class LinearMeshFactory
            : public Factory::Factory<IMesh>
        {
        public:
            std::unique_ptr<IMesh> create() const override;
            std::unique_ptr<IMesh> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}




#endif //CANDELA_LINEARMESHFACTORY_H

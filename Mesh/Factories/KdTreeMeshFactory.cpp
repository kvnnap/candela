//
// Created by Kevin on 18/12/2017.
//

#include "KdTreeMeshFactory.h"

using namespace std;
using namespace Candela::Mesh;
using namespace Candela::Factory;
using namespace Candela::Configuration;

unique_ptr<IMesh> KdTreeMeshFactory::create() const {
    return unique_ptr<IMesh>();
}

unique_ptr<IMesh> KdTreeMeshFactory::create(const ConfigurationNode &config) const {
    KdTreeMesh::SplittingStrategy s = KdTreeMesh::SplittingStrategy::Median;

    if (config.keyExists("SplittingStrategy")) {
        string splittingStrategy = **config["SplittingStrategy"];
        if (splittingStrategy == "Mean") {
            s = KdTreeMesh::SplittingStrategy::Mean;
        } else if (splittingStrategy == "Median") {
            s = KdTreeMesh::SplittingStrategy::Median;
        } else if (splittingStrategy == "SAH") {
            s = KdTreeMesh::SplittingStrategy::SAH;
        } else {
            throw runtime_error("Invalid SplittingStrategy for Kd-Tree");
        }
    }

    unique_ptr<IMesh> mesh = make_unique<KdTreeMesh>(
            static_cast<size_t>(config["MaxDepth"]->read<uint64_t>()),
            static_cast<size_t>(config["MaxShapesInLeaf"]->read<uint64_t>()),
            s);

    mesh->setName(**config["Name"]);
    return mesh;
}

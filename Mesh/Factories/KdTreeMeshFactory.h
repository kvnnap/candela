//
// Created by Kevin on 18/12/2017.
//

#ifndef CANDELA_KDTREEMESHFACTORY_H
#define CANDELA_KDTREEMESHFACTORY_H

#include "Factory/Factory.h"
#include "Mesh/KdTreeMesh.h"

namespace Candela
{
    namespace Mesh
    {
        class KdTreeMeshFactory
                : public Factory::Factory<IMesh>
        {
        public:
            std::unique_ptr<IMesh> create() const override;
            std::unique_ptr<IMesh> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}

#endif //CANDELA_KDTREEMESHFACTORY_H

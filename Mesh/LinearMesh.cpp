/* 
 * File:   LinearMesh.cpp
 * Author: Kevin
 * 
 * Created on 14 September 2014, 16:06
 */

#include <stdexcept>

#include "Mesh/LinearMesh.h"

using namespace std;
using namespace Candela::Shape;
using namespace Candela::Mesh;
using namespace Candela::Mathematics;

float LinearMesh::getSurfaceArea() const {
    return 0.f;
}

bool LinearMesh::intersect(const BoundedRay& ray, Intersection& intersection) const {
    return intersect(ray, intersection, nullptr);
}

bool LinearMesh::intersect(const BoundedRay &ray, Intersection &intersection, IShape *excludedShape) const {
    Mathematics::BoundedRay tempRay ( ray );

    for (const IShape* shape : meshList_) {
        if(shape != excludedShape && shape->intersect(tempRay, intersection)){
            tempRay.setMax(intersection.Distance);
        }
    }

    //check if any intersections
    return intersection.Type != Shape::Intersection::None;
}

void LinearMesh::add(const IShape& shape) {
    meshList_.push_back(&shape);
}

bool LinearMesh::remove(const IShape& shape) {
    if(meshList_.size() != 0)
    {
        for(unsigned i = 0; i < meshList_.size(); ++i)
        {
            if(meshList_[i] == &shape)
            {
                meshList_.erase(meshList_.begin() + i);
                return true;
            }
        }
        return false;
    }
    else
    {
        return false;
    }
}

void LinearMesh::clear() {
    meshList_.clear();
}

float LinearMesh::getVolume() const {
    return 0.f;
}

void LinearMesh::contain(const IShape &shape) {
    throw runtime_error("Linear Mesh, not implemented");
}

void LinearMesh::contain(const vector<const IShape *> &shapes) {
    throw runtime_error("Linear Mesh, not implemented");
}

void LinearMesh::add(const vector<const IShape *> &shapes) {
    meshList_.insert(meshList_.end(), shapes.begin(), shapes.end());
}

Vector LinearMesh::getUnitNormal(const Intersection &intersection) const {
    return Mathematics::Vector3<float>();
}

string LinearMesh::toString() const {
    return IMesh::toString() + " - LinearMesh";
}

// TODO
Vector LinearMesh::getCentroid() const {
    return Vector();
}


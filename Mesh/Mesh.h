//
// Created by Kevin on 18/12/2017.
//

#ifndef CANDELA_MESH_H
#define CANDELA_MESH_H

#include "Mesh/IMesh.h"

namespace Candela
{
    namespace Mesh
    {
        class Mesh
            : public IMesh
        {
        public:
            std::string getName() const override ;
            void setName(const std::string& name) override;

        private:
            std::string name;
        };
    }
}




#endif //CANDELA_MESH_H

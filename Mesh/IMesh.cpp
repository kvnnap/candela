/* 
 * File:   IMesh.cpp
 * Author: Kevin
 * 
 * Created on 14 September 2014, 15:38
 */

#include "Mesh/IMesh.h"
#include <sstream>

using namespace Candela::Mesh;

IMesh::IMesh() {}

std::string IMesh::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "IMesh - Address = " << this << endl;
    sstr << "Name: " << getName();
    return sstr.str();
}

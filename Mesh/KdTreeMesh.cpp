//
// Created by kvnna on 02/07/2016.
//

#include <algorithm>
#include <memory>
#include <sstream>
#include <iostream>
#include <iterator>
#include <Mathematics/Funtions.h>
#include "Mathematics/Float.h"
#include "KdTreeMesh.h"

using namespace std;
using namespace Candela::Mesh;
using namespace Candela::Shape;
using namespace Candela::Mathematics;

KDTreeMeshNode::KDTreeMeshNode()
    : Partition (), Axis ()
{ }

// KD TREE

KdTreeMesh::KdTreeMesh(size_t p_maxDepth, size_t p_maxShapesInLeaf, SplittingStrategy p_splittingStrategy)
        : maxDepth (p_maxDepth), maxShapesInLeaf (p_maxShapesInLeaf), splittingStrategy (p_splittingStrategy)
{ }

void KdTreeMesh::getBounds(AxisAlignedBoundingBox &aabb) const {
    aabb = bounds;
}

void KdTreeMesh::add(const vector<const IShape *> &shapes) {
    // We have shapes, Construct bounding box
    root = make_unique<KDTreeMeshNode>();
    bounds.contain(shapes);
    build(root.get(), shapes, bounds, 0);

    // Show stats
    StatisticsPrinter sp (shapes.size());
    visitTree(root, sp);
    sp.print();
}

void KdTreeMesh::build(KDTreeMeshNode *node, const vector<const IShape *> &shapes,
                       const AxisAlignedBoundingBox &aabb, size_t depth)
{
    if(depth != maxDepth && shapes.size() > maxShapesInLeaf)
    {
        auto axis = static_cast<uint8_t>(depth % 3);

        // split plane using some algorithm
        node->Axis = axis;
        node->Partition = split(shapes, aabb, axis);

        // split the AABB
        AxisAlignedBoundingBox left, right;
        aabb.splitUsingPartitionInAxis(axis, node->Partition, left, right);

        // Partition the shapes
        vector<const IShape *> leftShapes, rightShapes;
        for (const IShape* shape : shapes) {
            bool lost = true;
            if (shape->partOf(left)) {
                leftShapes.push_back(shape);
                lost = false;
            }
            if (shape->partOf(right)) {
                rightShapes.push_back(shape);
                lost = false;
            }
            if (lost) {
                cout << "Lost a shape while building Kd-Tree" << endl;
            }
        }

        // Recurse
        node->LeftChild = make_unique<KDTreeMeshNode>();
        node->RightChild = make_unique<KDTreeMeshNode>();
        build(node->LeftChild.get(), leftShapes, left, depth + 1);
        build(node->RightChild.get(), rightShapes, right, depth + 1);

    } else {
        // base case
        node->ShapeList = make_unique<vector<const IShape*>>();
        (*node->ShapeList) = shapes;
    }
}

float KdTreeMesh::split(const vector<const IShape *> &shapes, const AxisAlignedBoundingBox &aabb, uint8_t axis) {
    switch (splittingStrategy) {
        case Mean:
            return splitMean(aabb, axis);
        case Median:
            return splitMedian(shapes, aabb, axis);
        case SAH:
            return splitSAH(shapes, aabb, axis);
        default:
            throw runtime_error("Unsupported Split Request");
    }
}

float KdTreeMesh::splitMean(const AxisAlignedBoundingBox &aabb,
                            uint8_t axis) {
    return (aabb.getMaxPosition().xyz[axis] + aabb.getMinPosition().xyz[axis]) * 0.5f;
}

vector<float> KdTreeMesh::generateSplittingPlanes(const vector<const IShape *> &shapes,
                              const AxisAlignedBoundingBox &aabb, uint8_t axis, bool removeDuplicates) {
    vector<float> splittingPlanes;
    splittingPlanes.reserve(shapes.size() * 2);
    const float ra = aabb.getMinPosition().xyz[axis];
    const float rb = aabb.getMaxPosition().xyz[axis];

    for(const auto& shape : shapes) {
        AxisAlignedBoundingBox tempAabb;

        // Clip the the shape to aabb
        shape->getBounds(tempAabb);
//        shape->clipToAabb(aabb);
        const float a = tempAabb.getMinPosition().xyz[axis];
        const float b = tempAabb.getMaxPosition().xyz[axis];

        // Excluding boundaries as splitting on the boundary is the same as not splitting at all
        if (a > ra && a < rb) {
            splittingPlanes.push_back(a);
        }

        if (b > ra && b < rb) {
            splittingPlanes.push_back(b);
        }
    }

    sort(splittingPlanes.begin(), splittingPlanes.end());
    if (removeDuplicates) {
        splittingPlanes.erase(unique(splittingPlanes.begin(), splittingPlanes.end()), splittingPlanes.end());
    }

    return splittingPlanes;
}

size_t KdTreeMesh::countShapesInVoxel(const vector<const IShape *> &shapes, const AxisAlignedBoundingBox &aabb) {
    size_t shapesInVoxel = 0;

    for (const IShape* shape : shapes) {
        if (shape->partOf(aabb)) {
            ++shapesInVoxel;
        }
    }

    return shapesInVoxel;
}

float KdTreeMesh::splitMedian(const vector<const IShape *> &shapes,
                              const AxisAlignedBoundingBox &aabb, uint8_t axis) {
    // Generate a list of all possible splitting planes
    vector<float> splittingPlanes = generateSplittingPlanes(shapes, aabb, axis, false);
    if (splittingPlanes.empty()) {
        return splitMean(aabb, axis);
    }

    // Sort on axis
    return splittingPlanes.at(splittingPlanes.size() / 2);
}

float KdTreeMesh::splitSAH(const vector<const IShape *> &shapes,
                           const AxisAlignedBoundingBox &aabb, uint8_t axis) {
    // Generate a list of all possible splitting planes
    vector<float> splittingPlanes = generateSplittingPlanes(shapes, aabb, axis, true);
    // TODO: better handle no splitting planes scenario..
    if (splittingPlanes.empty()) {
        return splitMean(aabb, axis);
    }

    AxisAlignedBoundingBox left, right;
    float minCost = Float::Infinity;
    float minSplittingPlane = aabb.getMinPosition().xyz[axis];

    for(auto splittingPlane : splittingPlanes) {
        // get cost
        aabb.splitUsingPartitionInAxis(axis, splittingPlane, left, right);
        const size_t leftCount = countShapesInVoxel(shapes, left);
        const size_t rightCount = countShapesInVoxel(shapes, right);
        const float cost = (1.f + 3.f * (leftCount * left.getSurfaceArea() + rightCount * right.getSurfaceArea())) *
                           (leftCount == 0 || rightCount == 0 ? 0.8f : 1.f);
        if (cost < minCost) {
            minCost = cost;
            minSplittingPlane = splittingPlane;
        }
    }

    return minSplittingPlane;
}

// TODO: Can decrease numerical errors - checkout Havran thesis p.159
bool KdTreeMesh::intersect(const BoundedRay &ray, Intersection &intersection) const {
//    return intersectSimple(ray, intersection);
    float a, b, t;

    // Get min and max
    if (!bounds.intersect(ray, a, b))
        return false;

    StackElem stack[32];
    uint8_t stackPointer ( 0 );
    KDTreeMeshNode *farChild = nullptr, *currNode = nullptr;
    const Vector& rayPosition = ray.getPosition();
    const Vector& rayDirection = ray.getDirection();
    BoundedRay r = ray;

    // Init stack
    stack[0].node = root.get();
    stack[0].a = a;
    stack[0].b = b;
    ++stackPointer;

    while (stackPointer) {
        // Pop the stack
        {
            const StackElem& p = stack[--stackPointer];
            currNode = p.node; a = p.a; b = p.b;
        }

        // Not leaf
        while(currNode->LeftChild) {
            const float splitVal = currNode->Partition;
            const uint8_t axis = currNode->Axis;

            // Get axis-related point
            const float entryPoint = rayPosition.xyz[axis] + rayDirection.xyz[axis] * a;
            const float exitPoint  = rayPosition.xyz[axis] + rayDirection.xyz[axis] * b;

            // Figure out currentNode and farChild
            // Potential bugs should be handled in this if-then-else section
            if (entryPoint <= splitVal) {
                if (exitPoint < splitVal) {
                    currNode = currNode->LeftChild.get();
                    continue;
                }
                farChild = currNode->RightChild.get();
                currNode = currNode->LeftChild.get();
            } else { // entryPoint > splitVal
                if (exitPoint > splitVal) {
                    currNode = currNode->RightChild.get();
                    continue;
                }
                farChild = currNode->LeftChild.get();
                currNode = currNode->RightChild.get();
            }

            // Far child exists - stack it
            t = (splitVal - rayPosition.xyz[axis]) / rayDirection.xyz[axis];
            if (stackPointer == 32) {
                throw runtime_error("KdTreeMesh: Stack size exceeded");
            }
            stack[stackPointer].node = farChild;
            stack[stackPointer].a = t;
            stack[stackPointer++].b = b;
            b = t;
        }

        // Leaf - Check Prim List
        if (currNode->ShapeList->size() > 0) {
            r.setMin(a);
            r.setMax(b);
            bool intersected = false;
            for (const IShape* shape : *currNode->ShapeList) {
                if (shape->intersect(r, intersection)) {
                    r.setMax(intersection.Distance);
                    intersected = true;
                }
            }

            if (intersected) {
                return true;
            }
        }
    }

    return false;
}

float KdTreeMesh::getSurfaceArea() const {
    return bounds.getSurfaceArea();
}

float KdTreeMesh::getVolume() const {
    return bounds.getVolume();
}

// Todo: Check and implement or remove
void KdTreeMesh::contain(const IShape &shape) {
    throw runtime_error("Unsupported for Kd Tree");
}

void KdTreeMesh::contain(const vector<const IShape *> &shapes) {
    throw runtime_error("Unsupported for Kd Tree");
}

void KdTreeMesh::add(const IShape &shape) {
    throw runtime_error("Unsupported for Kd Tree");
}

bool KdTreeMesh::remove(const IShape &shape) {
    return false;
}

void KdTreeMesh::clear() {
    bounds = AxisAlignedBoundingBox();
    root.reset();
}

Vector KdTreeMesh::getUnitNormal(const Intersection &intersection) const {
    return Vector();
}

bool KdTreeMesh::intersect(const BoundedRay &ray, Intersection &intersection, IShape *excludedShape) const {
    throw runtime_error("Kd-Tree excluded intersection Not implemented");
}

bool KdTreeMesh::intersectSimple(const BoundedRay &ray, Intersection &intersection) const {

    BoundedRay rayBox = ray;
    BoundedRay advancedRayBox = ray;

    float b;
    // Find bounds
    if (bounds.contains(ray.getMinPoint())) {
        // Inside the bounding box
        Candela::Shape::Intersection localIntersection;
        b = bounds.intersect(rayBox, localIntersection) ?
            localIntersection.Distance : rayBox.getMax();
    } else {
        // Outside the bounding box
        Candela::Shape::Intersection localIntersection;
        if (bounds.intersect(rayBox, localIntersection)) {
            rayBox.setMin(localIntersection.Distance);
            advancedRayBox.setMinAndAdvanceByEpsilon(localIntersection.Distance);
            b = bounds.intersect(advancedRayBox, localIntersection) ?
                localIntersection.Distance : rayBox.getMax();
        } else {
            return false;
        }
    }

    while (rayBox.getMin() < b) {

        rayBox.setMax(b);
        advancedRayBox.setMax(b);

        AxisAlignedBoundingBox aabb = bounds;
        KDTreeMeshNode *node = getNodeFromPoint(root.get(), aabb, advancedRayBox);
        if (node == nullptr)
            return false;

        // Intersect the box
        Candela::Shape::Intersection boxIntersection;

        // Get next maxDistance if available
        if (aabb.intersect(advancedRayBox, boxIntersection)) {
//                rayBox.setMaxSafe(boxIntersection.Distance);
            rayBox.setMax(boxIntersection.Distance);
        }

        bool shapeIntersected = false;

        // Intersect the shapes
        if (node->ShapeList->size() > 0) {
            for (const IShape *shape : *node->ShapeList) {
                if (shape->intersect(rayBox, intersection)) {
                    rayBox.setMax(intersection.Distance);
                    shapeIntersected = true;
                }
            }
        }

        if (shapeIntersected) {
            return true;
        }

        rayBox.setMin(rayBox.getMax());
        advancedRayBox.setMinAndAdvanceByEpsilon(rayBox.getMin());
    }

    return false;
}

KDTreeMeshNode *KdTreeMesh::getNodeFromPoint(KDTreeMeshNode *node,
                                             AxisAlignedBoundingBox &aabb,
                                             const BoundedRay& ray
) {
    const Vector& point = ray.getMinPoint();

    if (!aabb.contains(point))
        return nullptr;

    uint8_t axis = 0;
    while (node && node->LeftChild) {
        // Use direction to determine which child to use whenever point stays on partition.
        // In reality, both children will work, however choosing intelligently might speed this up.
        const bool chooseLeft =  point.xyz[axis] < node->Partition ||
                                (point.xyz[axis] == node->Partition && ray.getDirection().xyz[axis] < 0.f);

        if (chooseLeft) {
            aabb.setMaxAxis(axis, node->Partition);
            node = node->LeftChild.get();
        } else {
            aabb.setMinAxis(axis, node->Partition);
            node = node->RightChild.get();
        }

        axis = static_cast<uint8_t>((axis + 1) % 3);
    }

    return node;
}

string KdTreeMesh::toString() const {
    ostringstream sstr;
    sstr << IMesh::toString()
         << "\n\tKdTreeMesh: " << this
         << "\n\tBounds: " << bounds
         << "\n\tMax Depth: " << maxDepth
         << "\n\tMax Shapes in Leaf: " << maxShapesInLeaf
            ;
    return sstr.str();
}

Vector KdTreeMesh::getCentroid() const {
    return bounds.getCentroid();
}

void KdTreeMesh::visitTree(std::unique_ptr<KDTreeMeshNode> &root, IKdTreeVisitor &visitor) {
    if (!root) {
        return;
    }

    visitor.visit(*root);
    visitTree(root->LeftChild, visitor);
    visitTree(root->RightChild, visitor);
}

// Statistics Visitor
StatisticsPrinter::StatisticsPrinter(size_t shapesCount)
    : minShapesInNode(std::numeric_limits<size_t>::max()), maxShapesInNode(std::numeric_limits<size_t>::min()),
      leafNodes(), leafShapeCount(), allShapesCount (shapesCount)

{}

void StatisticsPrinter::visit(const KDTreeMeshNode& kdTreeMeshNode) {
    if (!kdTreeMeshNode.ShapeList) {
        return;
    }
    minShapesInNode = Mathematics::Functions::min(minShapesInNode, kdTreeMeshNode.ShapeList->size());
    maxShapesInNode = Mathematics::Functions::max(maxShapesInNode, kdTreeMeshNode.ShapeList->size());
    std::copy( kdTreeMeshNode.ShapeList->begin(), kdTreeMeshNode.ShapeList->end(), std::inserter( shapes, shapes.end() ) );
    ++leafNodes;
    leafShapeCount += kdTreeMeshNode.ShapeList->size();
}

void StatisticsPrinter::print() const {
    cout << "KDTree Statistics Printer: " << endl
         << "Min shapes in node: " << minShapesInNode << endl
         << "Max shapes in node: " << maxShapesInNode << endl
         << "All shapes in structure: " << (shapes.size() == allShapesCount ? "Yes" : "No") << " - " << shapes.size() << "/" << allShapesCount << endl
         << "Total Leaf Nodes: " << leafNodes << endl
         << "Total Shape references in all leaf nodes: " << leafShapeCount << endl
         << "Average shapes per leaf node: " << (static_cast<float>(leafShapeCount) / leafNodes) << endl;
}

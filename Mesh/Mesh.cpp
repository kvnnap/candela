//
// Created by Kevin on 18/12/2017.
//

#include "Mesh.h"

using namespace std;
using namespace Candela::Mesh;

string Mesh::getName() const {
    return name;
}

void Mesh::setName(const string &name) {
    this->name = name;
}



/* 
 * File:   LinearMesh.h
 * Author: Kevin
 *
 * Created on 14 September 2014, 16:06
 */

#ifndef LINEARMESH_H
#define	LINEARMESH_H

#include <vector>
#include "Mesh.h"
#include "Shape/AxisAlignedBoundingBox.h"
#include "Shape/IShape.h"

namespace Candela
{
    namespace Mesh
    {
        class LinearMesh 
            : public Mesh
        {
        public:
            /* IShape */
            bool intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection) const override;
            float getSurfaceArea() const override;
            Mathematics::Vector getUnitNormal(const Shape::Intersection& intersection) const override;
            Mathematics::Vector getCentroid() const override;

            /* IBoundingShape */
            float getVolume() const override;
            void contain(const Shape::IShape& shape) override;
            void contain(const std::vector<const Shape::IShape*>& shapes) override;

            /* IMesh */
            bool intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection, Shape::IShape* excludedShape) const override;
            void add(const std::vector<const Shape::IShape*>& shapes) override;
            void add(const Shape::IShape& shape) override;
            bool remove(const Shape::IShape& shape) override;
            void clear() override;
            std::string toString() const override;
        private:
            //Shape::AxisAlignedBoundingBox boundingBox_;
            std::vector<const Shape::IShape *> meshList_;
        };
    }
}

#endif	/* LINEARMESH_H */


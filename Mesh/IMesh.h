/* 
 * File:   IMesh.h
 * Author: Kevin
 *
 * Created on 14 September 2014, 15:38
 */

#ifndef IMESH_H
#define	IMESH_H

#include "Shape/IBoundingShape.h"

namespace Candela
{
    namespace Mesh
    {
        class IMesh
            : public Shape::IBoundingShape
        {
        public:
            IMesh();
            
            //Inherited from Shape
            using Shape::IShape::intersect;
            virtual std::string toString() const;

            // The name of this Mesh
            virtual std::string getName() const = 0;
            virtual void setName(const std::string& name) = 0;

            //Own
            virtual bool intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection, Shape::IShape* excludedShape) const = 0;
            virtual void add(const std::vector<const Shape::IShape*>& shapes) = 0;
            virtual void add(const Shape::IShape& shape) = 0;
            virtual bool remove(const Shape::IShape& shape) = 0;
            virtual void clear() = 0;
            
        private:
            
        };
    }
}

#endif	/* IMESH_H */


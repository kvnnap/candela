//
// Created by kvnna on 20/12/2017.
//

#include "MatteTextureFactory.h"
#include "Texture/MatteTexture.h"
#include "Environment/Environment.h"

using namespace std;
using namespace Candela::Texture;
using namespace Candela::Configuration;
using namespace Candela::Spectrum;

MatteTextureFactory::MatteTextureFactory(Candela::Environment::Environment &environment)
        : environment (environment)
{}

unique_ptr<ITexture> MatteTextureFactory::create() const {
    return unique_ptr<ITexture>();
}

unique_ptr<ITexture> MatteTextureFactory::create(const ConfigurationNode &config) const {
    const ConfigurationNode& spectrumNode = *config["Spectrum"];
    auto& spectrumFactoryManager = environment.getSpectrumManager().getFactoryManager();
    return make_unique<MatteTexture>(spectrumFactoryManager.getFactory(**spectrumNode["Type"]).create(spectrumNode));
}

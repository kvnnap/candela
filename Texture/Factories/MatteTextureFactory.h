//
// Created by kvnna on 20/12/2017.
//

#ifndef CANDELA_MATTETEXTUREFACTORY_H
#define CANDELA_MATTETEXTUREFACTORY_H

#include "Factory/Factory.h"
#include "Texture/ITexture.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Texture
    {
        class MatteTextureFactory
            : public Factory::Factory<ITexture>
        {
        public:
            MatteTextureFactory(Environment::Environment& environment);

            std::unique_ptr<ITexture> create() const override;
            std::unique_ptr<ITexture> create(const Configuration::ConfigurationNode& config) const override;

        private:
            Environment::Environment& environment;
        };
    }
}

#endif //CANDELA_MATTETEXTUREFACTORY_H

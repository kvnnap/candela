/* 
 * File:   ITexture.h
 * Author: Kevin
 *
 * Created on 13 April 2014, 19:22
 */

#ifndef ITEXTURE_H
#define	ITEXTURE_H

#include "Mathematics/Vector2.h"
#include "Spectrum/ISpectrum.h"

/*
 Needs spectrum class and a Vector2 class..
 */

namespace Candela
{
    namespace Texture
    {
        class ITexture {
        public:
            virtual ~ITexture();

            // Describes this texture
            virtual std::string toString() const;

            virtual const Spectrum::ISpectrum * GetSpectrum(const Mathematics::Vector2<float>& uv) const = 0;

        private:
        };

        std::ostream& operator<< (std::ostream& strm, const ITexture& texture);
    }
}

#endif	/* ITEXTURE_H */


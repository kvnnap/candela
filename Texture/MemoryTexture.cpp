//
// Created by kvnna on 30/06/2016.
//

#include <sstream>
#include <iostream>
#include <algorithm>
#include <png.h>


#include "MemoryTexture.h"
#include "Mathematics/Funtions.h"

using namespace std;
using namespace Candela::Texture;
using namespace Candela::Spectrum;
using namespace Candela::Mathematics;
//
//MemoryTexture::MemoryTexture()
//    : MemoryTexture(0, 0)
//{ }
//
//
//MemoryTexture::MemoryTexture(size_t width, size_t height)
//    : textureBuffer (width, height)
//{
//}

const ISpectrum *MemoryTexture::GetSpectrum(const Vector2<float> &uv) const {
    Vector2<float> mappedUv;
    // // map uv coordinates - repeat
    for (size_t i = 0; i < 2; ++i) {
        const float val = Functions::mod(uv.xy[i], 1.f);
        mappedUv.xy[i] = val < 0.f ? val + 1.f : val;
    }

    const size_t u = Functions::clamp<size_t>(static_cast<size_t>(mappedUv.x * textureBuffer.getWidth()), 0, textureBuffer.getWidth() - 1);
    const size_t v = Functions::clamp<size_t>(static_cast<size_t>(mappedUv.y * textureBuffer.getHeight()), 0, textureBuffer.getHeight() - 1);
    return &textureBuffer(u, v);
}

Candela::Renderer::Buffer2D<RgbSpectrum> &MemoryTexture::getTextureBuffer() {
    return textureBuffer;
}

void MemoryTexture::loadFromPng(const std::string &pngFilePath) {
    bool error = false;

    size_t x, y;
    size_t width, height;

    png_structp png_ptr = nullptr;
    png_infop info_ptr = nullptr;
    png_bytep * row_pointers = nullptr;
    png_byte color_type;
    png_byte bit_depth;
    bool gammaResult;
    double gammaValue;
//    int number_of_passes;

    unsigned char header[8];    // 8 is the maximum size that can be checked

    /* open file and test for it being a png */
    FILE *fp = fopen(pngFilePath.c_str(), "rb");
    if (!fp) {
        cout << "[read_png_file] File %s could not be opened for reading" << endl;
        error = true;
        goto stop;

    }

    {
        size_t hReadSize = fread(header, 1, 8, fp);
        if (hReadSize != 8 || png_sig_cmp(header, 0, 8)) {
            cout << "[read_png_file] File %s is not recognized as a PNG file or failed to read png header atomically"
                 << endl;
            error = true;
            goto stop;
        }
    }

    /* initialize stuff */
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr) {
        cout << "[read_png_file] png_create_read_struct failed" << endl;
        error = true;
        goto stop;
    }

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        cout << "[read_png_file] png_create_info_struct failed" << endl;
        error = true;
        goto stop;
    }

    if (setjmp(png_jmpbuf(png_ptr))) {
        cout << "[read_png_file] Error during init_io" << endl;
        error = true;
        goto stop;
    }

    png_init_io(png_ptr, fp);
    png_set_sig_bytes(png_ptr, 8);

    png_read_info(png_ptr, info_ptr);

    width = png_get_image_width(png_ptr, info_ptr);
    height = png_get_image_height(png_ptr, info_ptr);
    color_type = png_get_color_type(png_ptr, info_ptr);
    bit_depth = png_get_bit_depth(png_ptr, info_ptr);

    if (color_type != PNG_COLOR_TYPE_RGB || bit_depth != 8) {
        cout << "[read_png_file] Unimplemented format for reading png" << endl;
        error = true;
        goto stop;
    }

    gammaResult = png_get_gAMA(png_ptr, info_ptr, &gammaValue) == PNG_INFO_gAMA;
    if (gammaResult) {
        // Transform encoding gamma to decoding gamma
        gammaValue = 1 / gammaValue;
    }

//
//    number_of_passes = png_set_interlace_handling(png_ptr);
    png_read_update_info(png_ptr, info_ptr);

    /* read file */
    if (setjmp(png_jmpbuf(png_ptr))) {
        cout << "[read_png_file] Error during read_image" << endl;
        error = true;
        goto stop;
    }

    row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
    for (y=0; y<height; y++) {
        row_pointers[y] = (png_byte *) malloc(png_get_rowbytes(png_ptr, info_ptr));
    }

    png_read_image(png_ptr, row_pointers);

    // Process
    textureBuffer.clearAndReset(width, height);
    for (y=0; y<height; y++) {
        png_byte* row = row_pointers[y];
        for (x=0; x<width; x++) {
            png_byte* ptr = &(row[x*3]);
            RgbSpectrum rgbSpectrum = RgbSpectrum(
                    ptr[0] / 255.f,
                    ptr[1] / 255.f,
                    ptr[2] / 255.f
            );
            if (gammaResult) {
                // Untested
                rgbSpectrum.inverseAlphaCorrect(static_cast<float>(gammaValue));
            } else {
                rgbSpectrum.inverseAlphaCorrect();
            }
            textureBuffer(x, y) = rgbSpectrum;
        }
    }

    // Free memory
    stop:
    if(row_pointers) {
        for (y = 0; y < height; y++) {
            free(row_pointers[y]);
        }
        free(row_pointers);
    }
    if (info_ptr) {
        png_destroy_info_struct(png_ptr, &info_ptr);
    }

    if (png_ptr) {
        png_destroy_read_struct(&png_ptr, NULL, NULL);
    }
    if (fp) {
        fclose(fp);
    }
    if (error) {
        throw runtime_error("Something wrong happened while loading PNG file: " + pngFilePath);
    }
}

void MemoryTexture::loadFromFile(const std::string &filePath) {
    string format = filePath.substr(filePath.size() - 3, 3);
    std::transform(format.begin(), format.end(), format.begin(), ::tolower);
    if (format == "png") {
        loadFromPng(filePath);
    } else {
        throw runtime_error("Memory Texture loadFromFile: Unsupported file format: " + format + " for file: " + filePath);
    }
}

std::string MemoryTexture::toString() const {
    ostringstream sstr;
    sstr << ITexture::toString()
         << " - MemoryTexture";
    return sstr.str();
}

//
// Created by kvnna on 30/06/2016.
//

#ifndef MSC_CANDELA_MEMORYTEXTURE_H
#define MSC_CANDELA_MEMORYTEXTURE_H

#include "Renderer/Buffer2D.h"
#include "Spectrum/RgbSpectrum.h"
#include "Mathematics/Vector2.h"
#include "ITexture.h"

namespace Candela
{
    namespace Texture
    {
        class MemoryTexture
            : public ITexture
        {
        public:

//            MemoryTexture();
//            MemoryTexture(size_t width, size_t height);

            Renderer::Buffer2D<Spectrum::RgbSpectrum>& getTextureBuffer();
//            void setTextureBuffer(Renderer::Buffer2D<Spectrum::RgbSpectrum>& p_textureBuffer);

            const Spectrum::ISpectrum * GetSpectrum(const Mathematics::Vector2<float>& uv) const override;

            void loadFromFile(const std::string& filePath);
            void loadFromPng(const std::string& pngFilePath);

            // Describe this texture
            std::string toString() const override;
        private:
            // Buffer
            Renderer::Buffer2D<Spectrum::RgbSpectrum> textureBuffer;
        };
    }
}


#endif //MSC_CANDELA_MEMORYTEXTURE_H

/* 
 * File:   MatteTexture.cpp
 * Author: Kevin
 * 
 * Created on September 15, 2014, 10:06 PM
 */

#include <sstream>
#include "Texture/MatteTexture.h"
#include "Spectrum/RgbSpectrum.h"

using namespace std;
using namespace Candela::Texture;
using namespace Candela::Spectrum;
using namespace Candela::Mathematics;

MatteTexture::MatteTexture(std::unique_ptr<ISpectrum> a_spectrum)
    : spectrum ( move(a_spectrum) )
{

}

const ISpectrum* MatteTexture::GetSpectrum(const Vector2<float>& uv) const {
    return spectrum.get();
}

const MatteTexture MatteTexture::black (unique_ptr<RgbSpectrum>(new RgbSpectrum(0.f)));
const MatteTexture MatteTexture::white (unique_ptr<RgbSpectrum>(new RgbSpectrum(1.f)));

string MatteTexture::toString() const {
    ostringstream sstr;
    sstr << ITexture::toString()
         << " - MatteTexture";
    return sstr.str();
}

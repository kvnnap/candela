/* 
 * File:   MatteTexture.h
 * Author: Kevin
 *
 * Created on September 15, 2014, 10:06 PM
 */

#ifndef MATTETEXTURE_H
#define	MATTETEXTURE_H

#include "ITexture.h"

namespace Candela
{
    namespace Texture
    {
        class MatteTexture 
            : public ITexture
        {
        public:
            MatteTexture(std::unique_ptr<Spectrum::ISpectrum> a_spectrum);

            const Spectrum::ISpectrum * GetSpectrum(const Mathematics::Vector2<float>& uv) const override;

            // Describe this texture
            std::string toString() const override;

            // static
            static const MatteTexture black;
            static const MatteTexture white;
        private:
            
            std::unique_ptr<Spectrum::ISpectrum> spectrum;
        };

    }
}

#endif	/* MATTETEXTURE_H */


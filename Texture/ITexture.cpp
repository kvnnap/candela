/* 
 * File:   ITexture.cpp
 * Author: Kevin
 * 
 * Created on 13 April 2014, 19:22
 */

#include <sstream>
#include "Texture/ITexture.h"


using namespace std;
using namespace Candela::Texture;

ITexture::~ITexture() {}

string ITexture::toString() const {
    ostringstream sstr;
    sstr << "ITexture - Address = " << this;
    return sstr.str();
}

ostream& Candela::Texture::operator <<(ostream& strm, const ITexture& texture)
{
    return strm << texture.toString();
}

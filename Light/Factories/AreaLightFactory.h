//
// Created by kvnna on 25/03/2018.
//

#ifndef CANDELA_AREALIGHTFACTORY_H
#define CANDELA_AREALIGHTFACTORY_H


#include "Factory/Factory.h"
#include "Light/ILight.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Light
    {
        class AreaLightFactory
                : public Factory::Factory<ILight>
        {
        public:
            // Inject Spectrum Factory (best to inject Environment)
            AreaLightFactory(Environment::Environment& environment);
            std::unique_ptr<ILight> create() const override;
            std::unique_ptr<ILight> create(const Configuration::ConfigurationNode& config) const override;

        private:
            Environment::Environment& environment;
        };
    }
}


#endif //CANDELA_AREALIGHTFACTORY_H

//
// Created by Kevin on 18/12/2017.
//

#ifndef CANDELA_POINTLIGHTFACTORY_H
#define CANDELA_POINTLIGHTFACTORY_H

#include "Factory/Factory.h"
#include "Light/ILight.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Light
    {
        class PointLightFactory
            : public Factory::Factory<ILight>
        {
        public:
            // Inject Spectrum Factory (best to inject Environment)
            PointLightFactory(Environment::Environment& environment);
            std::unique_ptr<ILight> create() const override;
            std::unique_ptr<ILight> create(const Configuration::ConfigurationNode& config) const override;

        private:
            Environment::Environment& environment;
        };
    }
}

#endif //CANDELA_POINTLIGHTFACTORY_H

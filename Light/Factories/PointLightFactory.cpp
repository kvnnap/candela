//
// Created by Kevin on 18/12/2017.
//

#include "Light/PointLight.h"
#include "PointLightFactory.h"
#include "Mathematics/Factories/VectorFactory.h"
#include "Spectrum/Factories/RgbSpectrumFactory.h"
#include "Environment/Environment.h"

using namespace std;
using namespace Candela;
using namespace Candela::Light;
using namespace Candela::Mathematics;
using namespace Candela::Configuration;
using namespace Candela::Spectrum;
using namespace Candela::Factory;

PointLightFactory::PointLightFactory(Environment::Environment &environment)
        : environment (environment)
{}

unique_ptr<ILight> PointLightFactory::create() const {
    return unique_ptr<ILight>();
}

unique_ptr<ILight> PointLightFactory::create(const ConfigurationNode &config) const {
    // Get position - float vector
    const Vector position = *VectorFactory().create(*config["Position"]);

    // Get Spectrum node
    const ConfigurationNode& spectrumNode = *config["Spectrum"];

    auto& spectrumFactoryManager = environment.getSpectrumManager().getFactoryManager();
    auto spectrum = spectrumFactoryManager.getFactory(**spectrumNode["Type"]).create(spectrumNode);

    unique_ptr<ILight> light = make_unique<PointLight>(position, move(spectrum));
    light->setName(**config["Name"]);
    if (config.keyExists("Intensity")) {
        light->setIntensity(static_cast<float>(config["Intensity"]->read<double>()));
    }
    return light;
}

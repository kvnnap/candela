//
// Created by kvnna on 25/03/2018.
//

#include "Light/AreaLight.h"
#include "Light/PointLight.h"
#include "AreaLightFactory.h"
#include "Shape/Factories/TriangleFactory.h"
#include "Spectrum/Factories/RgbSpectrumFactory.h"
#include "Environment/Environment.h"

using namespace std;
using namespace Candela;
using namespace Candela::Light;
using namespace Candela::Mathematics;
using namespace Candela::Configuration;
using namespace Candela::Spectrum;
using namespace Candela::Factory;
using namespace Candela::Shape;

AreaLightFactory::AreaLightFactory(Environment::Environment &environment)
        : environment (environment)
{}

unique_ptr<ILight> AreaLightFactory::create() const {
    return unique_ptr<ILight>();
}

unique_ptr<ILight> AreaLightFactory::create(const ConfigurationNode &config) const {
    // Get position - float vector
    IShape& shape = environment.getShapeManager().getInstanceManager().getInstance(*config["Shape"]);
    Triangle* triangle = dynamic_cast<Triangle*>(&shape);
    if (triangle == nullptr) {
        throw runtime_error("AreaLight currently only supports Triangle as shape");
    }

    // Get Spectrum node
    const ConfigurationNode& spectrumNode = *config["Spectrum"];

    auto& spectrumFactoryManager = environment.getSpectrumManager().getFactoryManager();
    auto spectrum = spectrumFactoryManager.getFactory(**spectrumNode["Type"]).create(spectrumNode);

    unique_ptr<ILight> light = make_unique<AreaLight>(*triangle, move(spectrum));
    light->setName(**config["Name"]);
    if (config.keyExists("Intensity")) {
        light->setIntensity(static_cast<float>(config["Intensity"]->read<double>()));
    }
    return light;
}

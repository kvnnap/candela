//
// Created by Kevin on 18/12/2017.
//

#ifndef CANDELA_LIGHT_H
#define CANDELA_LIGHT_H

#include "ILight.h"

namespace Candela
{
    namespace Light
    {
        class Light
            : public ILight
        {
        public:
            Light();
            // The name of this light
            std::string getName() const override;
            void setName(const std::string& name) override;

            float getIntensity() const override;
            void setIntensity(float intensity) override;

        protected:
            float intensity;

        private:
            std::string name;
        };
    }
}

#endif //CANDELA_LIGHT_H

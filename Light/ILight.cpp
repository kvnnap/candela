/* 
 * File:   ILight.cpp
 * Author: Kevin
 * 
 * Created on August 24, 2014, 11:40 AM
 */

#include <sstream>
#include "Light/ILight.h"

using namespace std;
using namespace Candela::Light;

ILight::~ILight() {}

string ILight::toString() const {
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << "Light - Address = " << this
         << "\n\tName: " << getName();
    return sstr.str();
}

ostream& Candela::Light::operator <<(ostream& strm, const ILight& light)
{
    return strm << light.toString();
}
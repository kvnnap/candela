//
// Created by kvnna on 29/06/2016.
//

#ifndef MSC_CANDELA_AREALIGHT_H
#define MSC_CANDELA_AREALIGHT_H

#include "Light.h"
#include "Spectrum/Spectrum.h"
#include "Shape/Triangle.h"

namespace Candela
{
    namespace Light
    {
        class AreaLight
                : public Light{
        public:
            AreaLight(const Shape::Triangle& triangle, std::unique_ptr<Spectrum::ISpectrum> spectrum);

            Shape::IShape * getShape();

            float power() const override;
            void radiance(const Mathematics::Ray& ray, Spectrum::RgbSpectrum& a_spectrum) const override;
            const Mathematics::Vector getCentroid() const override;
            const Mathematics::Vector samplePoint(float r1, float r2, Mathematics::Vector& surfaceNormal) const;
            const float getPdf() const override;
            const float getArea() const override;

        private:
            Shape::Triangle triangle;
            std::unique_ptr<Spectrum::ISpectrum> spectrum;
        };
    }
}

#endif //MSC_CANDELA_AREALIGHT_H

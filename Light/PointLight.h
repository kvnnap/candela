/* 
 * File:   PointLight.h
 * Author: Kevin
 *
 * Created on August 25, 2014, 8:53 PM
 */

#ifndef POINTLIGHT_H
#define	POINTLIGHT_H

#include "Light.h"
#include "Spectrum/Spectrum.h"

namespace Candela
{
    namespace Light
    {
        class PointLight 
            : public Light {
        public:
            PointLight(const Mathematics::Vector& point, std::unique_ptr<Spectrum::ISpectrum> spectrum);

            float power() const override;
            void radiance(const Mathematics::Ray& ray, Spectrum::RgbSpectrum& a_spectrum) const override;
            const Mathematics::Vector getCentroid() const override;
            const Mathematics::Vector samplePoint(float r1, float r2, Mathematics::Vector& surfaceNormal) const override;
            const float getPdf() const override;
            const float getArea() const override;

            std::string toString() const override;

        private:
            const Mathematics::Vector point;
            std::unique_ptr<Spectrum::ISpectrum> spectrum;
        };
    }
}



#endif	/* POINTLIGHT_H */


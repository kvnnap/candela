//
// Created by Kevin on 18/12/2017.
//

#include "Light.h"

using namespace std;
using namespace Candela::Light;

Light::Light()
    : intensity ( 1.f )
{}

void Light::setName(const string &name) {
    this->name = name;
}

string Light::getName() const {
    return name;
}

float Light::getIntensity() const {
    return intensity;
}

void Light::setIntensity(float intensity) {
    this->intensity = intensity;
}

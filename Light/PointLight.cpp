/* 
 * File:   PointLight.cpp
 * Author: Kevin
 * 
 * Created on August 25, 2014, 8:53 PM
 */

#include "Light/PointLight.h"
#include "Mathematics/Constants.h"

using namespace std;
using namespace Candela::Light;
using namespace Candela::Spectrum;
using namespace Candela::Mathematics;

PointLight::PointLight(const Vector& point, unique_ptr<ISpectrum> a_spectrum)
    : point ( point ), spectrum ( move(a_spectrum) )
{
    //divide the power by a sphere total steradiance.
    //(*spectrum) *= Candela::Mathematics::Constants::OneOverFourPi;
}

float PointLight::power() const {
    //calculate power in watts using spectrum
    return 0.f;
}

void PointLight::radiance(const Ray& ray, RgbSpectrum& a_spectrum) const {
    //wherever the ray is coming from, we have same radiance,
    //no cosine required because a point Light is an infinitesimally small Sphere
    //Mathematics::Vector v = point - ray.getPosition();
    //spectrum->divide(v * v, a_spectrum);
    a_spectrum = spectrum->toTristimulusSRGB();
    a_spectrum *= intensity;
}

const Vector PointLight::getCentroid() const {
    return point;
}

string PointLight::toString() const {
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << Light::toString()
         << "\n\tPoint Light: " << this
         << "\n\tPosition: " << point
         //<< "\n\tSpectrum: " << (*spectrum)
            ;
    return sstr.str();
}

const Vector PointLight::samplePoint(float r1, float r2, Vector& surfaceNormal) const {
    return point;
}

const float PointLight::getPdf() const {
    return 0.f;
}

const float PointLight::getArea() const {
    return 0.f;
}



/* 
 * File:   ILight.h
 * Author: Kevin
 *
 * Created on August 24, 2014, 11:40 AM
 */

#ifndef ILIGHT_H
#define	ILIGHT_H

#include "Spectrum/RgbSpectrum.h"
#include "Mathematics/BoundedRay.h"

namespace Candela
{
    namespace Light
    {
        /**
         * Defines an Interface to all Light Sources having common methods
         */
        class ILight {
        public:
            virtual ~ILight();

            // Describes this light
            virtual std::string toString() const;

            // The name of this light
            virtual std::string getName() const = 0;
            virtual void setName(const std::string& name) = 0;

            // Light intensity
            virtual float getIntensity() const = 0;
            virtual void setIntensity(float intensity) = 0;

            /**
             * Calculates the total Power (watts) of this Luminaire
             * @return Power in Watts
             */
            virtual float power() const = 0;
            
            /**
             * Radiance (L) is the flux that leaves a surface, 
             * per unit projected area of the surface, per unit solid angle of direction
             * Ω = (S/r²)
             * 
             * Radiance is Watts per Surface Area (of source) per Steradian (of source)
             * Returns the radiance (Watts per Meters Squared per Steradian) along 
             * the ray. We will assume a differential surface area which is infinitesimally small
             * at the source and at the detector, hence dividing by Area can be omitted,
             * since the same Infinitesimal Area would be multiplied afterwards. This can
             * sometimes be seen as Radiant Intensity but it is not..
             * @param ray The unbounded incident ray  whose inverse (-ray) would become the ray going 
             * out of this luminaire.
             * @return The Spectrum containing the Radiance
             */
            virtual void radiance(const Mathematics::Ray& ray, Spectrum::RgbSpectrum& a_spectrum) const = 0;

            virtual const float getPdf() const = 0;
            virtual const float getArea() const = 0;
            virtual const Mathematics::Vector getCentroid() const = 0;
            virtual const Mathematics::Vector samplePoint(float r1, float r2, Mathematics::Vector& surfaceNormal) const = 0;
        };

        std::ostream& operator<< (std::ostream& strm, const ILight& light);
    }
}



#endif	/* ILIGHT_H */


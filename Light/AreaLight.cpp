//
// Created by kvnna on 29/06/2016.
//

#include "AreaLight.h"

using namespace Candela::Mathematics;
using namespace Candela::Light;
using namespace Candela::Shape;
using namespace Candela::Spectrum;

AreaLight::AreaLight(const Triangle &triangle, std::unique_ptr<Spectrum::ISpectrum> spectrum)
    : triangle ( triangle ), spectrum (move(spectrum))
{
//    *this->spectrum *= getPdf();
}

float AreaLight::power() const {
    return 0;
}

void AreaLight::radiance(const Ray &ray, RgbSpectrum &a_spectrum) const {
    a_spectrum = spectrum->toTristimulusSRGB();
    a_spectrum *= intensity; // / getPdf();
}

const Vector AreaLight::getCentroid() const {
    return triangle.getCentroid();
}

const Vector AreaLight::samplePoint(float r1, float r2, Vector& surfaceNormal) const {
    return triangle.samplePoint(r1, r2, surfaceNormal);
}

const float AreaLight::getPdf() const {
    return 1.f / triangle.getSurfaceArea();
}

const float AreaLight::getArea() const {
    return triangle.getSurfaceArea();
}

IShape *AreaLight::getShape() {
    return &triangle;
}


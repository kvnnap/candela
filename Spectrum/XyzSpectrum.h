/* 
 * File:   XyzSpectrum.h
 * Author: Kevin
 *
 * Created on 26 April 2014, 14:21
 */

#ifndef XYZSPECTRUM_H
#define	XYZSPECTRUM_H

#include "TriSpectrum.h"

namespace Candela
{
    namespace Spectrum
    {   
        class XyzSpectrum 
                : public TriSpectrum
        {
        public:
            XyzSpectrum();
            XyzSpectrum(float value);
            XyzSpectrum(const XyzSpectrum& xyz);
            XyzSpectrum(const Mathematics::Vector& xyz);
            XyzSpectrum(float x, float y, float z);

            // Virtual Overrides
            float getWaveLength(size_t sampleIndex) const override;
            float luminance() const override;
            RgbSpectrum toTristimulusSRGB() const override;
            XyzSpectrum toTristimulusXYZ() const override;
            
        private:

        };
    }
}

#endif	/* XYZSPECTRUM_H */


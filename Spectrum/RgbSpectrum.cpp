/* 
 * File:   RgbSpectrum.cpp
 * Author: Kevin
 * 
 * Created on 26 April 2014, 13:34
 */

#include "Mathematics/Matrix.h"
#include "Mathematics/Funtions.h"
#include "Spectrum/XyzSpectrum.h"
#include "Spectrum/RgbSpectrum.h"
#include "Mathematics/Funtions.h"

using namespace Candela::Spectrum;
using namespace Candela::Mathematics;

RgbSpectrum::RgbSpectrum()
{
}

RgbSpectrum::RgbSpectrum(float value) 
        : TriSpectrum( value )
{
}

RgbSpectrum::RgbSpectrum(const RgbSpectrum& rgb)
        : TriSpectrum(rgb)
{
}

RgbSpectrum::RgbSpectrum(const Vector& rgb)
        : TriSpectrum(rgb)
{
}

RgbSpectrum::RgbSpectrum(float red, float green, float blue)
        : TriSpectrum(red, green, blue)
{
}

float RgbSpectrum::getWaveLength(size_t sampleIndex) const {
    return sampleIndex == 0 ? 700.f : (sampleIndex == 1 ? 546.1f : 435.8f);
}

float RgbSpectrum::luminance() const {
    return 0.2126729f * abc_.x + 0.7151522f * abc_.y + 0.0721750f * abc_.z;
}

RgbSpectrum RgbSpectrum::toTristimulusSRGB() const {
    return *this;
}

/*
 * use this matrix
 *      0.4124564  0.3575761  0.1804375
        0.2126729  0.7151522  0.0721750
        0.0193339  0.1191920  0.9503041
 * and multiply it by this->rgb_
 */
XyzSpectrum RgbSpectrum::toTristimulusXYZ() const
{
    Matrix_F m (3, 3);
    
    m(0, 0) = 0.4124564f;
    m(0, 1) = 0.3575761f;
    m(0, 2) = 0.1804375f;
    
    m(1, 0) = 0.2126729f;
    m(1, 1) = 0.7151522f;
    m(1, 2) = 0.0721750f;
    
    m(2, 0) = 0.0193339f;
    m(2, 1) = 0.1191920f;
    m(2, 2) = 0.9503041f;
    
    return XyzSpectrum(m * abc_);
}

//Need to edit clamp function to desaturate function

//from linear to screen non-linear (normal rgb)
void RgbSpectrum::alphaCorrect(float decodingGamma)
{
    toneMap();
    const float encodingGamma = 1.f / decodingGamma;
    abc_.x = abc_.x <= 0.0031308f ? abc_.x * 12.92f : 1.055f * Mathematics::Functions::pow(abc_.x, encodingGamma) - 0.055f;
    abc_.y = abc_.y <= 0.0031308f ? abc_.y * 12.92f : 1.055f * Mathematics::Functions::pow(abc_.y, encodingGamma) - 0.055f;
    abc_.z = abc_.z <= 0.0031308f ? abc_.z * 12.92f : 1.055f * Mathematics::Functions::pow(abc_.z, encodingGamma) - 0.055f;
}

//from screen (normal rgb) to linear
void RgbSpectrum::inverseAlphaCorrect(float decodingGamma)
{
    clamp();
    abc_.x = abc_.x <= 0.04045f ? abc_.x / 12.92f : Mathematics::Functions::pow((abc_.x + 0.055f) / 1.055f, decodingGamma);
    abc_.y = abc_.y <= 0.04045f ? abc_.y / 12.92f : Mathematics::Functions::pow((abc_.y + 0.055f) / 1.055f, decodingGamma);
    abc_.z = abc_.z <= 0.04045f ? abc_.z / 12.92f : Mathematics::Functions::pow((abc_.z + 0.055f) / 1.055f, decodingGamma);
}

//RgbSpectrum &RgbSpectrum::operator=(const RgbSpectrum &spectrum) {
//    *((TriSpectrum*)(this)) = *((TriSpectrum*)(&spectrum));
//    return *this;
//}

const RgbSpectrum RgbSpectrum::Black (0.f);


// Read here http://frictionalgames.blogspot.com.mt/2012/09/tech-feature-hdr-lightning.html
// and here https://computergraphics.stackexchange.com/questions/6307/tone-mapping-bright-images
void RgbSpectrum::toneMap() {
//    abc_ *= 1.f / (luminance() + 1.f); // <-- (lum / (lum + 1)) / lum; simplifies to
//
//    const float max = Functions::max(Functions::max(abc_.x, abc_.y), abc_.z);
//    if (max > 1.f) {
//        abc_.x /= abc_.x + 1.f;
//        abc_.y /= abc_.y + 1.f;
//        abc_.z /= abc_.z + 1.f;
//    }


    //
    abc_.x /= abc_.x + 1.f;
    abc_.y /= abc_.y + 1.f;
    abc_.z /= abc_.z + 1.f;

    // John Hable's tone mapping
//    const float A = 0.15;
//    const float B = 0.50;
//    const float C = 0.10;
//    const float D = 0.20;
//    const float E = 0.02;
//    const float F = 0.30;
//
//    for (int i = 0; i < 3; ++i) {
//        float x = abc_.xyz[i];
//        abc_.xyz[i] = ((x * (A * x + C * B) + D * E) / (x * (A * x + B) + D * F)) - E / F;
//    }
}

/* 
 * File:   Spectrum.h
 * Author: Kevin
 *
 * Created on 26 April 2014, 18:46
 */

#ifndef SPECTRUM_H
#define	SPECTRUM_H

#include "ISpectrum.h"
#include "Mathematics/Algebra/IFunction.h"
#include <vector>

namespace Candela
{
    namespace Spectrum
    {  
        //assumes visible spectrum from 390nm to 730nm
        class Spectrum 
                : public ISpectrum
        {
        public:
            Spectrum(unsigned short startWaveLength, unsigned char stepSize, 
                        const std::vector<float>& samples);
            
            virtual float getWaveLength(unsigned sampleIndex) const;
            virtual float luminance() const;
            virtual RgbSpectrum toTristimulusSRGB() const;
            virtual XyzSpectrum toTristimulusXYZ() const;
            
            virtual void add(const ISpectrum& spectrum, ISpectrum& result) const;
            virtual void subtract(const ISpectrum& spectrum, ISpectrum& result) const;
            virtual void multiply(const ISpectrum& spectrum, ISpectrum& result) const;
            virtual void multiply(float scale, ISpectrum& result) const;
            virtual void divide(const ISpectrum& spectrum, ISpectrum& result) const;
            virtual void divide(float invScale, ISpectrum& result) const;
            
            virtual ISpectrum& operator += (const ISpectrum& spectrum);
            virtual ISpectrum& operator -= (const ISpectrum& spectrum);
            virtual ISpectrum& operator *= (const ISpectrum& spectrum);
            virtual ISpectrum& operator *= (float scale);
            virtual ISpectrum& operator /= (const ISpectrum& spectrum);
            virtual ISpectrum& operator /= (float invScale);
            
        private:
            //static Mathematics::Algebra::IFunction<float> * const R, * const G, * const B;
            static Mathematics::Algebra::IFunction<float> & X, & Y, & Z;
            
            unsigned short startWaveLength_;
            unsigned short stepSize_;
            std::vector<float> samples_;
            
            void operate(const ISpectrum& spectrum, ISpectrum& result, float(*op)(float, float)) const;
            void operate(float scale, ISpectrum& result, float(*op)(float, float)) const;
        };
    }
}

#endif	/* SPECTRUM_H */


/* 
 * File:   XyzSpectrum.cpp
 * Author: Kevin
 * 
 * Created on 26 April 2014, 14:21
 */

#include "Mathematics/Matrix.h"

#include "Spectrum/RgbSpectrum.h"
#include "Spectrum/XyzSpectrum.h"

using namespace Candela::Spectrum;
using namespace Candela::Mathematics;

XyzSpectrum::XyzSpectrum() {
}

XyzSpectrum::XyzSpectrum(float value) 
        : TriSpectrum( value )
{
}

XyzSpectrum::XyzSpectrum(const XyzSpectrum& xyz)
        : TriSpectrum(xyz)
{
}

XyzSpectrum::XyzSpectrum(const Vector& xyz)
        : TriSpectrum(xyz)
{
}

XyzSpectrum::XyzSpectrum(float x, float y, float z)
        : TriSpectrum(x, y, z)
{
}

float XyzSpectrum::getWaveLength(size_t sampleIndex) const {
    return 0;
}

float XyzSpectrum::luminance() const {
    return abc_.y;
}

/*
 * 3.2404542 -1.5371385 -0.4985314
 * -0.9692660  1.8760108  0.0415560
 * 0.0556434 -0.2040259  1.0572252
 */
RgbSpectrum XyzSpectrum::toTristimulusSRGB() const {
    Matrix_F m (3, 3);
    
    m(0, 0) = 3.2404542f;
    m(0, 1) = -1.5371385f;
    m(0, 2) = -0.4985314f;
    
    m(1, 0) = -0.9692660f;
    m(1, 1) = 1.8760108f;
    m(1, 2) = 0.0415560f;
    
    m(2, 0) = 0.0556434f;
    m(2, 1) = -0.2040259f;
    m(2, 2) = 1.0572252f;
    
    return RgbSpectrum(m * abc_);
}

XyzSpectrum XyzSpectrum::toTristimulusXYZ() const {
    return *this;
}
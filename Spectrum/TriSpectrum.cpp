/* 
 * File:   TriSpectrum.cpp
 * Author: Kevin
 * 
 * Created on 17 April 2014, 19:58
 */

#include "Spectrum/TriSpectrum.h"

using namespace Candela::Spectrum;
using namespace Candela::Mathematics;

TriSpectrum::TriSpectrum() 
{
}

TriSpectrum::TriSpectrum(float value) 
        : abc_ ( value )
{
}

TriSpectrum::TriSpectrum(const TriSpectrum& rgb)
        : abc_ ( rgb.abc_ )
{}

TriSpectrum::TriSpectrum(const Vector& rgb)
        : abc_ ( rgb )
{
}

TriSpectrum::TriSpectrum(float a, float b, float c) 
        : abc_ ( a, b, c )
{
}

void TriSpectrum::clamp(float min, float max)
{
    for(unsigned i = 0; i < 3; ++i)
    {
        if(abc_.xyz[i] < min)
        {
            abc_.xyz[i] = min;
        }else if(abc_.xyz[i] > max)
        {
            abc_.xyz[i] = max;
        }
    }
}

Vector& TriSpectrum::getAbcReference() {
    return abc_;
}

size_t TriSpectrum::getNumSamples() const {
    return 3;
}

ISpectrum& TriSpectrum::operator +=(const ISpectrum& spectrum) {
    abc_ += ((TriSpectrum&) spectrum).abc_;
    return *this;
}

ISpectrum& TriSpectrum::operator -=(const ISpectrum& spectrum) {
    abc_ -= ((TriSpectrum&) spectrum).abc_;
    return *this;
}

ISpectrum& TriSpectrum::operator *=(const ISpectrum& spectrum) {
    abc_.setHadamard(((TriSpectrum&) spectrum).abc_);
    return *this;
}

ISpectrum& TriSpectrum::operator *=(float scale) {
    abc_ *= scale;
    return *this;
}

ISpectrum& TriSpectrum::operator /=(const ISpectrum& spectrum) {
    abc_.x /= ((TriSpectrum&) spectrum).abc_.x;
    abc_.y /= ((TriSpectrum&) spectrum).abc_.y;
    abc_.z /= ((TriSpectrum&) spectrum).abc_.z;
    return *this;
}

ISpectrum& TriSpectrum::operator /=(float scale) {
    abc_ /= scale;
    return *this;
}

void TriSpectrum::add(const ISpectrum& spectrum, ISpectrum& result) const {
    ((TriSpectrum&) result).abc_ = abc_ + ((TriSpectrum&) spectrum).abc_;
}

void TriSpectrum::subtract(const ISpectrum& spectrum, ISpectrum& result) const {
    ((TriSpectrum&) result).abc_ = abc_ - ((TriSpectrum&) spectrum).abc_;
}

void TriSpectrum::multiply(const ISpectrum& spectrum, ISpectrum& result) const {
    ((TriSpectrum&) result).abc_ = abc_.hadamard(((TriSpectrum&) spectrum).abc_);
}

void TriSpectrum::multiply(float scale, ISpectrum& result) const {
    ((TriSpectrum&) result).abc_ = abc_ * scale;
}

void TriSpectrum::divide(const ISpectrum& spectrum, ISpectrum& result) const {
    ((TriSpectrum&) result).abc_.x = abc_.x / ((TriSpectrum&) spectrum).abc_.x;
    ((TriSpectrum&) result).abc_.y = abc_.y / ((TriSpectrum&) spectrum).abc_.y;
    ((TriSpectrum&) result).abc_.z = abc_.z / ((TriSpectrum&) spectrum).abc_.z;
}

void TriSpectrum::divide(float invScale, ISpectrum& result) const {
    ((TriSpectrum&) result).abc_ = abc_ / invScale;
}

/* 
 * File:   ISpectrum.h
 * Author: Kevin
 *
 * Created on 16 April 2014, 22:13
 */

#ifndef ISPECTRUM_H
#define	ISPECTRUM_H

#include <cstdint>
#include <memory>

//#include "Mathematics/Vector3.h"

/*
 Define RGBSpectrum and make it use vector3
 * Define operators for a spectrum..
 */

namespace Candela
{
    namespace Spectrum
    {

        class RgbSpectrum;
        class XyzSpectrum;
        
        class ISpectrum {
        public:
            virtual ~ISpectrum();
            
            virtual size_t getNumSamples() const = 0;
            virtual float getWaveLength(size_t sampleIndex) const = 0;
            
            /*
            * Brightness is exactly the Y component in the standard XYZ colour space
            * When you transform from sRGB to XYZ to get Y using matrices, you use
            * the second row of the transformation matrix, hence this is applied as below.
            * See http://www.brucelindbloom.com/index.html?Eqn_XYZ_to_RGB.html
            * under sRGB with the D65 reference white colour
            * Monitors use sRGB.. read also about gamma correction, etc.
            */
            //defines, how bright is it?
            virtual float luminance() const = 0;
            
            //the functions below convert a spectrum to three values which when
            //added together appear to be the same to the colour of the spectrum
            //..might need to convert to XYZ and then to SRGB, or might combine matrix
            virtual RgbSpectrum toTristimulusSRGB() const = 0;
            virtual XyzSpectrum toTristimulusXYZ() const = 0;
            
            //operators
            
            virtual void add(const ISpectrum& spectrum, ISpectrum& result) const = 0;
            virtual void subtract(const ISpectrum& spectrum, ISpectrum& result) const = 0;
            virtual void multiply(const ISpectrum& spectrum, ISpectrum& result) const = 0;
            virtual void multiply(float scale, ISpectrum& result) const = 0;
            virtual void divide(const ISpectrum& spectrum, ISpectrum& result) const = 0;
            virtual void divide(float invScale, ISpectrum& result) const = 0;
            
            virtual ISpectrum& operator += (const ISpectrum& spectrum) = 0;
            virtual ISpectrum& operator -= (const ISpectrum& spectrum) = 0;
            virtual ISpectrum& operator *= (const ISpectrum& spectrum) = 0;
            virtual ISpectrum& operator *= (float scale) = 0;
            virtual ISpectrum& operator /= (const ISpectrum& spectrum) = 0;
            virtual ISpectrum& operator /= (float invScale) = 0;
            
        };

        using ISpectrumPt = std::unique_ptr<Spectrum::ISpectrum>;
    }
}



#endif	/* ISPECTRUM_H */


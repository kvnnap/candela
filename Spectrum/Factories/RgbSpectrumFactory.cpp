//
// Created by Kevin on 18/12/2017.
//

#include "RgbSpectrumFactory.h"
#include "Mathematics/Vector3.h"
#include "Mathematics/Factories/VectorFactory.h"
#include "Spectrum/RgbSpectrum.h"

using namespace std;
using namespace Candela::Spectrum;
using namespace Candela::Mathematics;
using namespace Candela::Configuration;

std::unique_ptr<ISpectrum> RgbSpectrumFactory::create() const {
    return make_unique<RgbSpectrum>();
}

std::unique_ptr<ISpectrum> RgbSpectrumFactory::create(const ConfigurationNode &config) const {
    unique_ptr<Vector> v = VectorFactory().create();
    v->x = static_cast<float>(config["r"]->read<double>());
    v->y = static_cast<float>(config["g"]->read<double>());
    v->z = static_cast<float>(config["b"]->read<double>());
    return make_unique<RgbSpectrum>(*v);
}



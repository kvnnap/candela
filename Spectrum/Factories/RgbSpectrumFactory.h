//
// Created by Kevin on 18/12/2017.
//

#ifndef CANDELA_RGBSPECTRUMFACTORY_H
#define CANDELA_RGBSPECTRUMFACTORY_H

#include "Factory/Factory.h"
#include "Spectrum/ISpectrum.h"

namespace Candela
{
    namespace Spectrum
    {
        class RgbSpectrumFactory
            : public Factory::Factory<ISpectrum>
        {
        public:
            std::unique_ptr<ISpectrum> create() const override;
            std::unique_ptr<ISpectrum> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}

#endif //CANDELA_RGBSPECTRUMFACTORY_H

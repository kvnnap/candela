//
// Created by kvnna on 12/11/2017.
//

#ifndef CANDELA_FACTORYMANAGER_H
#define CANDELA_FACTORYMANAGER_H

#include <map>
#include <memory>
#include <string>
#include "Factory.h"

namespace Candela
{
    namespace Factory
    {

        template<class T>
        class FactoryManager
        {
        public:

            using FactoryType = Factory<T>;
            using FactoryPt = std::unique_ptr<FactoryType>;

            virtual ~FactoryManager() {};

            template<class U, class ...V>
            void registerFactory(const std::string& factoryName, V && ... args)
            {
                factoryMap[factoryName] = std::make_unique<U>(args...);
            }

            const FactoryType& getFactory(const std::string& factoryName)
            {
                return *factoryMap.at(factoryName);
            }

            bool factoryExists(const std::string& factoryName)
            {
                return factoryMap.find(factoryName) != factoryMap.end();
            }
        private:
            std::map<std::string, FactoryPt> factoryMap;
        };
    }
}

#endif //CANDELA_FACTORYMANAGER_H

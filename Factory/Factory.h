//
// Created by kvnna on 11/11/2017.
//

#ifndef CANDELA_FACTORY_H_H
#define CANDELA_FACTORY_H_H

#include <memory>
#include "Configuration/ConfigurationNode.h"

namespace Candela
{
    namespace Factory
    {
        // May be subclassed once to be used as a factory for a category of products
        // Or may be subclassed more than once for each product
        template<class T>
        class Factory
        {
        public:
            virtual ~Factory() {};
            virtual std::unique_ptr<T> create() const = 0;
            virtual std::unique_ptr<T> create(const Configuration::ConfigurationNode& config) const = 0;
        };
    }
}

#endif //CANDELA_FACTORY_H_H

//
// Created by Kevin on 03/12/2017.
//

#ifndef CANDELA_INSTANCEMANAGER_H
#define CANDELA_INSTANCEMANAGER_H

#include <map>
#include <vector>
#include <memory>
#include "Configuration/ConfigurationNode.h"

namespace Candela
{
    namespace Environment
    {
        template <class T>
        class InstanceManager
        {
        public:
            using InstanceType = T;
            using InstancePt = std::unique_ptr<InstanceType>;

            void registerInstance(const std::string& instanceName, InstancePt instancePt) {
                instanceMap[instanceName] = move(instancePt);
            }

            T& getInstance(const std::string& instanceName) {
                return *instanceMap.at(instanceName);
            }

            bool instanceExists(const std::string& instanceName)
            {
                return instanceMap.find(instanceName) != instanceMap.end();
            }

            // List based
            size_t registerInstance(InstancePt instancePt) {
                instanceList.push_back(move(instancePt));
                return instanceList.size() - 1;
            }

            T& getInstance(size_t id) {
                return *instanceList.at(id);
            }

            bool instanceExists(size_t id)
            {
                return id < instanceList.size();
            }

            T& getInstance(Configuration::ConfigurationNode& config) {
//                union {
                    std::string name;
                    uint64_t index;
//                };
                if (config.read(name)) {
                    return *instanceMap.at(name);
                } else if (config.read(index)) {
                    return *instanceList.at(index);
                }

                throw std::runtime_error("Invalid config argument type - not string nor uint64");
            }

            std::vector<InstanceType*> getInstances() {
                std::vector<InstanceType*> instances;
                for(auto& i : instanceMap) {
                    instances.push_back(i.second.get());
                }
                for(auto& i : instanceList) {
                    instances.push_back(i.get());
                }
                return instances;
            }
        private:
            std::map<std::string, InstancePt> instanceMap;
            std::vector<InstancePt> instanceList;
        };
    }
}

#endif //CANDELA_INSTANCEMANAGER_H

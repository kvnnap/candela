//
// Created by Kevin on 03/12/2017.
//

#ifndef CANDELA_RESOURCEMANAGER_H
#define CANDELA_RESOURCEMANAGER_H

#include "Factory/FactoryManager.h"
#include "InstanceManager.h"

namespace Candela
{
    namespace Environment
    {
        template <class T>
        class ResourceManager
        {
        public:
            using FactoryManagerType = typename Factory::FactoryManager<T>;
            using FactoryType = typename FactoryManagerType::FactoryType;
            using FactoryPt = typename FactoryManagerType::FactoryPt;
            using InstanceType = typename InstanceManager<T>::InstanceType;
            using InstancePt = typename InstanceManager<T>::InstancePt;

            Factory::FactoryManager<T>& getFactoryManager() { return factoryManager; }
            InstanceManager<T>& getInstanceManager() { return instanceManager; }

        private:
            Factory::FactoryManager<T> factoryManager;
            InstanceManager<T> instanceManager;
        };
    }
}

#endif //CANDELA_RESOURCEMANAGER_H

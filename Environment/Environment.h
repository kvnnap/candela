//
// Created by kvnna on 14/11/2017.
//

#ifndef CANDELA_ENVIRONMENT_H
#define CANDELA_ENVIRONMENT_H

#include <iostream>
#include "ResourceManager.h"
#include "Configuration/Parser/Parser.h"
#include "Camera/ICamera.h"
#include "Light/ILight.h"
#include "Spectrum/ISpectrum.h"
#include "Mesh/IMesh.h"
#include "Device/IDevice.h"
#include "Integrator/IIntegrator.h"
#include "Shape/IShape.h"
#include "Texture/ITexture.h"
#include "Material/IMaterial.h"
#include "Scene/IScene.h"
#include "Scene/ISceneLoader.h"
#include "Renderer/IRenderer.h"


namespace Candela
{
    namespace Environment
    {
        using ConfigurationManager = ResourceManager<Configuration::Parser::Parser>;
        using CameraManager = ResourceManager<Camera::ICamera>;
        using LightManager = ResourceManager<Light::ILight>;
        using SpectrumManager = ResourceManager<Spectrum::ISpectrum>;
        using MeshManager = ResourceManager<Mesh::IMesh>;
        using DeviceManager = ResourceManager<Device::IDevice>;
        using IntegratorManager = ResourceManager<Integrator::IIntegrator>;
        using ShapeManager = ResourceManager<Shape::IShape>;
        using TextureManager = ResourceManager<Texture::ITexture>;
        using MaterialManager = ResourceManager<Material::IMaterial>;
        using SceneManager = ResourceManager<Scene::IScene>;
        using SceneLoaderManager = ResourceManager<Scene::ISceneLoader>;
        using RendererManager = ResourceManager<Renderer::IRenderer>;

        class Environment
        {
        public:
            Environment();
            virtual ~Environment();

            // Initialise the environment
            void init(const std::string& configParser, const std::string& parseeFileName);

            void execute();

            ConfigurationManager& getConfigurationManager();
            CameraManager& getCameraManager();
            LightManager& getLightManager();
            SpectrumManager& getSpectrumManager();
            MeshManager& getMeshManager();
            DeviceManager& getDeviceManager();
            IntegratorManager& getIntegratorManager();
            ShapeManager& getShapeManager();
            TextureManager& getTextureManager();
            MaterialManager& getMaterialManager();
            SceneManager& getSceneManager();
            SceneLoaderManager& getSceneLoaderManager();
            RendererManager& getRendererManager();

        private:
            void registerFactories();

            // Load configuration
            template <class T>
            void loadSection(const std::string& sectionName, Configuration::ConfigurationNodePt& configuration, T& manager) {
                // Load cameras
                auto& managerInstance = manager.getFactoryManager();
                // Cast to const to use the operator[] const overload of ConfigurationNode
                for (const auto& pairInstance : *(static_cast<const Configuration::ConfigurationNode&>(*configuration))[sectionName]) {
                    const std::string factoryName = **pairInstance.value["Type"];
                    if (!managerInstance.factoryExists(factoryName)) {
                        throw std::runtime_error(factoryName + " type does not exist");
                    }
                    auto instance = managerInstance.getFactory(factoryName).create(pairInstance.value);
                    // std::cout << *instance << std::endl;
                    if (pairInstance.value.keyExists("Name")) {
                        manager.getInstanceManager().registerInstance(**pairInstance.value["Name"], std::move(instance));
                    } else {
                        manager.getInstanceManager().registerInstance(std::move(instance));
                    }
                }
            }

            // Data
            ConfigurationManager configurationManager;
            CameraManager cameraManager;
            LightManager lightManager;
            SpectrumManager spectrumManager;
            MeshManager meshManager;
            DeviceManager deviceManager;
            IntegratorManager integratorManager;
            ShapeManager shapeManager;
            TextureManager textureManager;
            MaterialManager materialManager;
            SceneManager sceneManager;
            SceneLoaderManager sceneLoaderManager;
            RendererManager rendererManager;
        };
    }
}

#endif //CANDELA_ENVIRONMENT_H

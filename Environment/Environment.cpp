//
// Created by kvnna on 14/11/2017.
//

#include <memory>
#include <chrono>
#include <iostream>
#include "Environment.h"
#include "Configuration/Parser/JsonConfigurationParser.h"
#include "Configuration/ObjectNode.h"
#include "Configuration/LiteralNode.h"
#include "Camera/Factories/PinholeCameraFactory.h"
#include "Camera/Factories/RealPinholeCameraFactory.h"
#include "Camera/Factories/ThinLensCameraFactory.h"
#include "Light/Factories/PointLightFactory.h"
#include "Light/Factories/AreaLightFactory.h"
#include "Spectrum/Factories/RgbSpectrumFactory.h"
#include "Mesh/Factories/LinearMeshFactory.h"
#include "Mesh/Factories/KdTreeMeshFactory.h"
#include "Device/Factories/FileDeviceFactory.h"
#include "Integrator/Factories/SimpleIntegratorFactory.h"
#include "Integrator/Factories/WhittedIntegratorFactory.h"
#include "Integrator/Factories/PathIntegratorFactory.h"
#include "Shape/Factories/SphereFactory.h"
#include "Shape/Factories/TriangleFactory.h"
#include "Texture/Factories/MatteTextureFactory.h"
#include "Material/Factories/BaseMaterialFactory.h"
#include "Scene/Factories/SceneFactory.h"
#include "Scene/Factories/ConfigurationSceneLoaderFactory.h"
#include "Scene/Factories/WavefrontSceneLoaderFactory.h"
#include "Renderer/Factories/SimpleRendererFactory.h"
#include "Renderer/Factories/ProgressiveRendererFactory.h"
#include "Renderer/Factories/VarianceBasedRendererFactory.h"

using namespace std;
using namespace Candela::Environment;
using namespace Candela::Camera;
using namespace Candela::Configuration;
using namespace Candela::Configuration::Parser;
using namespace Candela::Light;
using namespace Candela::Spectrum;
using namespace Candela::Mesh;
using namespace Candela::Device;
using namespace Candela::Integrator;
using namespace Candela::Shape;
using namespace Candela::Texture;
using namespace Candela::Material;
using namespace Candela::Scene;
using namespace Candela::Renderer;

Environment::Environment() {
    // Register all available factories
    registerFactories();
}

Environment::~Environment() {}

ConfigurationManager &Environment::getConfigurationManager() {
    return configurationManager;
}

CameraManager &Environment::getCameraManager() {
    return cameraManager;
}

LightManager &Environment::getLightManager() {
    return lightManager;
}

SpectrumManager &Environment::getSpectrumManager() {
    return spectrumManager;
}

MeshManager &Environment::getMeshManager() {
    return meshManager;
}

DeviceManager &Environment::getDeviceManager() {
    return deviceManager;
}

IntegratorManager &Environment::getIntegratorManager() {
    return integratorManager;
}

ShapeManager &Environment::getShapeManager() {
    return shapeManager;
}

TextureManager &Environment::getTextureManager() {
    return textureManager;
}

MaterialManager &Environment::getMaterialManager() {
    return materialManager;
}

SceneManager &Environment::getSceneManager() {
    return sceneManager;
}

SceneLoaderManager &Environment::getSceneLoaderManager() {
    return sceneLoaderManager;
}

RendererManager &Environment::getRendererManager() {
    return rendererManager;
}

void Environment::init(const string& configParser, const string& parseeFileName) {

    // Get the configuration factory manager
    ConfigurationManager::FactoryManagerType &configurationFactoryManager = configurationManager.getFactoryManager();

    // Load configuration
    if (!configurationFactoryManager.factoryExists(configParser))
        throw runtime_error("Invalid parser name");

    // Get correct factory
    const ConfigurationManager::FactoryType& parserFactory = configurationFactoryManager.getFactory(configParser);

    // Compile configuration node to give a blue-print to the factory
    ObjectNode objectNode;
    objectNode["file"] = LiteralNode::fromValue(parseeFileName);

    // Produce parser - no need to register this instance as we're gonna use it only once
    ParserPt parser = parserFactory.create(objectNode);

    // Parse configuration
    ConfigurationNodePt configuration = parser->loadConfiguration();
//    cout << configuration->toString() << endl;
    //cout << ((*configuration)["decimal"])->read<double>() << endl;

    loadSection("Shapes", configuration, shapeManager);
    loadSection("Cameras", configuration, cameraManager);
    loadSection("Lights", configuration, lightManager);
    loadSection("Meshes", configuration, meshManager);
    loadSection("Devices", configuration, deviceManager);
    loadSection("Integrators", configuration, integratorManager);
    loadSection("Textures", configuration, textureManager);
    loadSection("Materials", configuration, materialManager);
    loadSection("Scenes", configuration, sceneManager);
    loadSection("SceneLoaders", configuration, sceneLoaderManager);
    loadSection("Renderers", configuration, rendererManager);
}

void Environment::registerFactories() {

    // Register Configuration Parsers
    configurationManager.getFactoryManager().registerFactory<JsonConfigurationParserFactory>("JsonConfigurationParser");

    // Register Cameras
    cameraManager.getFactoryManager().registerFactory<PinholeCameraFactory>("PinholeCamera");
    cameraManager.getFactoryManager().registerFactory<RealPinholeCameraFactory>("RealPinholeCamera");
    cameraManager.getFactoryManager().registerFactory<ThinLensCameraFactory>("ThinLensCamera");

    // Register Spectra
    spectrumManager.getFactoryManager().registerFactory<RgbSpectrumFactory>("RgbSpectrum");

    // Register Lights
    lightManager.getFactoryManager().registerFactory<PointLightFactory>("PointLight", *this);
    lightManager.getFactoryManager().registerFactory<AreaLightFactory>("AreaLight", *this);

    // Register Meshes
    meshManager.getFactoryManager().registerFactory<LinearMeshFactory>("LinearMesh");
    meshManager.getFactoryManager().registerFactory<KdTreeMeshFactory>("KdTreeMesh");

    // Register Devices
    deviceManager.getFactoryManager().registerFactory<FileDeviceFactory>("FileDevice");

    // Register Integrators
    integratorManager.getFactoryManager().registerFactory<SimpleIntegratorFactory>("SimpleIntegrator");
    integratorManager.getFactoryManager().registerFactory<WhittedIntegratorFactory>("WhittedIntegrator");
    integratorManager.getFactoryManager().registerFactory<PathIntegratorFactory>("PathIntegrator");

    // Register Shapes
    shapeManager.getFactoryManager().registerFactory<SphereFactory>("Sphere");
    shapeManager.getFactoryManager().registerFactory<TriangleFactory>("Triangle");

    // Register Textures
    textureManager.getFactoryManager().registerFactory<MatteTextureFactory>("MatteTexture", *this);

    // Register Materials
    materialManager.getFactoryManager().registerFactory<BaseMaterialFactory>("BaseMaterial", *this);

    // Register Scenes
    sceneManager.getFactoryManager().registerFactory<SceneFactory>("Scene", *this);

    // Register SceneLoaders
    sceneLoaderManager.getFactoryManager().registerFactory<ConfigurationSceneLoaderFactory>("ConfigurationSceneLoader", *this);
    sceneLoaderManager.getFactoryManager().registerFactory<WavefrontSceneLoaderFactory>("WavefrontSceneLoader", *this);

    // Register Renderers
    rendererManager.getFactoryManager().registerFactory<SimpleRendererFactory>("SimpleRenderer", *this);
    rendererManager.getFactoryManager().registerFactory<ProgressiveRendererFactory>("ProgressiveRenderer", *this);
    rendererManager.getFactoryManager().registerFactory<VarianceBasedRendererFactory>("VarianceBasedRenderer", *this);
}

void Environment::execute() {
    for(Renderer::IRenderer* renderer : rendererManager.getInstanceManager().getInstances()) {
        // Build Meshes
        auto start = chrono::steady_clock::now();
        renderer->getScene()->commitToMesh();
        cout << "Mesh build time: " << chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - start).count() << endl;

        // Render
        start = chrono::steady_clock::now();
        renderer->render();
        cout << "Render time: " << chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - start).count() << endl;
    }
}



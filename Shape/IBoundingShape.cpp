/* 
 * File:   IBoundingShape.cpp
 * Author: Kevin
 * 
 * Created on 26 December 2013, 12:28
 */

#include "Shape/IBoundingShape.h"

#include <sstream>

using namespace Candela::Shape;
using namespace Candela::Mathematics;

std::string IBoundingShape::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "IBoundingShape - Address = " << this;
    return sstr.str();
}

bool IBoundingShape::contains(const Vector &point) const {
    return false;
}




//
// Created by kvnna on 25/12/2017.
//

#include "TriangleFactory.h"
#include "Shape/Triangle.h"
#include "Mathematics/Factories/VectorFactory.h"

using namespace std;
using namespace Candela::Shape;
using namespace Candela::Configuration;
using namespace Candela::Mathematics;

unique_ptr<IShape> TriangleFactory::create() const {
    return make_unique<Triangle>(Vector(), Vector(), Vector());
}

unique_ptr<IShape> TriangleFactory::create(const ConfigurationNode &config) const {
    return make_unique<Triangle>(
            *VectorFactory().create(*config["O"]),
            *VectorFactory().create(*config["A"]),
            *VectorFactory().create(*config["B"])
    );
}
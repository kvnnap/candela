//
// Created by kvnna on 20/12/2017.
//

#include "SphereFactory.h"
#include "Shape/Sphere.h"
#include "Mathematics/Factories/VectorFactory.h"

using namespace std;
using namespace Candela::Shape;
using namespace Candela::Configuration;
using namespace Candela::Mathematics;

unique_ptr<IShape> SphereFactory::create() const {
    return make_unique<Sphere>(Vector(), 0.f);
}

unique_ptr<IShape> SphereFactory::create(const ConfigurationNode &config) const {
    return make_unique<Sphere>(*VectorFactory().create(*config["Centre"]), config["Radius"]->read<double>());
}

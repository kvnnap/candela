//
// Created by kvnna on 25/12/2017.
//

#ifndef CANDELA_TRIANGLEFACTORY_H
#define CANDELA_TRIANGLEFACTORY_H

#include "Factory/Factory.h"
#include "Shape/IShape.h"

namespace Candela
{
    namespace Shape
    {
        class TriangleFactory
                : public Factory::Factory<IShape>
        {
        public:
            std::unique_ptr<IShape> create() const override;
            std::unique_ptr<IShape> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}

#endif //CANDELA_TRIANGLEFACTORY_H

//
// Created by kvnna on 20/12/2017.
//

#ifndef CANDELA_SPHEREFACTORY_H
#define CANDELA_SPHEREFACTORY_H

#include "Factory/Factory.h"
#include "Shape/IShape.h"

namespace Candela
{
    namespace Shape
    {
        class SphereFactory
            : public Factory::Factory<IShape>
        {
        public:
            std::unique_ptr<IShape> create() const override;
            std::unique_ptr<IShape> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}


#endif //CANDELA_SPHEREFACTORY_H

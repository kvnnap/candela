/* 
 * File:   Plane.cpp
 * Author: Kevin
 * 
 * Created on 15 March 2014, 21:02
 */

#include "Shape/Plane.h"
#include "Shape/AxisAlignedBoundingBox.h"
#include "Mathematics/Float.h"
#include <sstream>

using namespace Candela::Shape;
using namespace Candela::Mathematics;

Plane::Plane(const Vector& position, const Vector& normal)
        : position_ ( position ), unitNormal_ ( ~normal )
{
}

bool Plane::intersect(const BoundedRay& ray, Intersection& intersection) const {
    //compute denominator
    const float den = unitNormal_ * ray.getDirection();
    if(den != 0.f) // make this check or else risk of division by zero..
    {
        float temp = - (unitNormal_ * (ray.getPosition() - position_)) / den;
        //make sure intersection is not behind the ray
        if(temp >= ray.getMin())
        {
            if(temp <= ray.getMax())
            {
                intersection.Intersectant = this;
                intersection.Distance = temp;
                intersection.Type = den < 0.f ?
                                    Intersection::External : Intersection::Internal;
                return true;
            }
        }
    }
    return false;
}


float Plane::getSurfaceArea() const {   
    return Float::Infinity;
}


std::string Plane::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "Plane Shape - Address = " << this
         << "\n\tPosition: " << position_  << "\n\tUnitNormal: " << unitNormal_; 
    return sstr.str();
}

Candela::Mathematics::Vector Plane::getUnitNormal(const Intersection& intersection) const {
    return unitNormal_;
}

void Plane::getBounds(AxisAlignedBoundingBox &aabb) const {
    aabb = AxisAlignedBoundingBox();
    // TODO
}

bool Plane::partOf(const AxisAlignedBoundingBox &aabb) const {
    Vector vec[2] = {aabb.getMinPosition(), aabb.getMaxPosition()};
    bool behind = false, further = false;
    for(int i = 0; i < 8; i++)
    {
        if((unitNormal_ * (Vector(vec[i & 1].x, vec[(i >> 1) & 1].y, vec[(i >> 2) & 1].z) - position_)) <= 0)
        {
            behind |= true;
        }else
        {
            further |= true;
        }
        if(behind && further)
        {
            return true;
        }
    }
    return false;
}

Vector Plane::getCentroid() const {
    return Vector();
}




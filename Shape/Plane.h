/* 
 * File:   Plane.h
 * Author: Kevin
 *
 * Created on 15 March 2014, 21:02
 */

#ifndef PLANE_H
#define	PLANE_H

#include "AbstractShape.h"
#include "Mathematics/Vector3.h"

namespace Candela
{
    namespace Shape
    {
        class Plane 
                : public AbstractShape
        {
        public:
            Plane(const Mathematics::Vector& position, const Mathematics::Vector& normal);
            
            /* Inherited from IShape */
            std::string toString() const override;
            bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const override;
            Mathematics::Vector getUnitNormal(const Intersection& intersection) const override;
            float getSurfaceArea() const override;
            void getBounds(AxisAlignedBoundingBox& aabb) const override;
            bool partOf(const AxisAlignedBoundingBox& aabb) const override;
            Mathematics::Vector getCentroid() const override;
            
        private:
            Mathematics::Vector position_, unitNormal_;
        };
    }
}

#endif	/* PLANE_H */


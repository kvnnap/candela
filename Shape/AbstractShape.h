//
// Created by kvnna on 12/05/2018.
//

#ifndef CANDELA_ABSTRACTSHAPE_H
#define CANDELA_ABSTRACTSHAPE_H

#include "IShape.h"

namespace Candela
{
    namespace Shape
    {
        class AbstractShape
                : public IShape
        {
        public:
            AbstractShape();
            void setGroupId(uint32_t groupId) override;
            uint32_t getGroupId() const override;

        private:
            uint32_t groupId;
        };
    }
}


#endif //CANDELA_ABSTRACTSHAPE_H

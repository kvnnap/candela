/* 
 * File:   Triangle.cpp
 * Author: Kevin
 * 
 * Created on 02 March 2014, 18:00
 */

#include <sstream>
#include <cmath>
#include <Mathematics/Float.h>

#include "Shape/Triangle.h"
#include "Shape/AxisAlignedBoundingBox.h"
#include "Mathematics/Vector2.h"
#include "Mathematics/Funtions.h"
#include "Triangle.h"


using namespace Candela::Shape;
using namespace Candela::Mathematics;

Triangle::Triangle(const Vector& O, const Vector& A, const Vector& B) 
{
    vertex_[0] = O;
    vertex_[1] = A;
    vertex_[2] = B;
}

bool Triangle::intersect(const BoundedRay& ray, Intersection& intersection) const
{
    //these below are cache-able
    const Vector Q1 = vertex_[1] - vertex_[0];
    const Vector Q2 = vertex_[2] - vertex_[0];
    const Vector faceUnitNormal = ~(Q1 ^ Q2);
    
    //compute denominator
    const float denominator = faceUnitNormal * ray.getDirection();
    if(denominator != 0.f) // make this check or else risk of division by zero..
    {
        float temp = - (faceUnitNormal * (ray.getPosition() - vertex_[0])) / denominator;
        //make sure intersection is not behind the ray
        if(temp >= ray.getMin())
        {
            //this is closer than previous intersection
            if(temp <= ray.getMax())
            {
                //check whether it lies in the triangle
                const Vector R = ray.getPoint(temp) - vertex_[0];
                
                //below 4 values are cache-able
                const float Q1Sq = Q1 * Q1;
                const float Q2Sq = Q2 * Q2;
                const float Q1Q2 = Q1 * Q2;
                union{
                    float determinant;
                    float oneOverDeterminant;
                };
                
                determinant = Q1Sq * Q2Sq - Q1Q2 * Q1Q2;
                
                if(determinant != 0.f)
                {
                    oneOverDeterminant = 1.f / determinant;
                    const float RQ1 = R * Q1, RQ2 = R * Q2;
                    const float u = ( Q2Sq * RQ1 - Q1Q2 * RQ2 ) * oneOverDeterminant;
                    const float v = ( Q1Sq * RQ2 - Q1Q2 * RQ1 ) * oneOverDeterminant;
                    if( (u >= 0.f) && (v >= 0.f) && ((u + v) <= 1.f) )
                    //it is in triangle!
                    {
                        intersection.Intersectant = this;
                        intersection.Distance = temp;
                        intersection.Type = denominator < 0.f ?
                                    Intersection::External : Intersection::Internal;
                        // Since we already computed them, save them for later!
                        intersection.uv.x = u;
                        intersection.uv.y = v;
                        return true;
                    }
                }
            }
        }
    }
    //no intersection
    return false;
}

float Triangle::getSurfaceArea() const {
    //these below are cache-able
    const Vector Q1 = vertex_[1] - vertex_[0];
    const Vector Q2 = vertex_[2] - vertex_[0];
    const float Q1Q2 = Q1 * Q2;
    
    return 0.5f * !Q1 * !Q2 * Functions::sqrt(1 - (Q1Q2 * Q1Q2 / (( Q1 * Q1 ) * (Q2 * Q2))));
}


std::string Triangle::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "Triangle Shape - Address = " << this
         << "\n\tVertex[0]: " << vertex_[0]
         << "\n\tVertex[1]: " << vertex_[1]
         << "\n\tVertex[2]: " << vertex_[2]
         << "\n\tTexture[0]: " << texture_[0]
         << "\n\tTexture[1]: " << texture_[1]
         << "\n\tTexture[2]: " << texture_[2]
         << "\n\tNormal[0]: " << normal_[0]
         << "\n\tNormal[1]: " << normal_[1]
         << "\n\tNormal[2]: " << normal_[2]
         << "\n\tUnitNormal: " << getUnitNormal()
    ;
    return sstr.str();
}

Vector Triangle::getNormal() const {
    return (( vertex_[1] - vertex_[0] ) ^ ( vertex_[2] - vertex_[0] ));
}

Vector Triangle::getUnitNormal() const {
    return ~getNormal();
}

Vector Triangle::getUnitNormal(const Intersection& intersection) const {
    return normal_[0].isNotSet() ? getUnitNormal() :
         ~(normal_[0] + intersection.uv.x * (normal_[1] - normal_[0]) + intersection.uv.y * (normal_[2] - normal_[0]));
}

void Triangle::setTextureCoordinates(const Vector2<float> &t1, const Vector2<float> &t2,
                                     const Vector2<float> &t3) {
    texture_[0] = t1;
    texture_[1] = t2;
    texture_[2] = t3;
}

Candela::Mathematics::Vector2<float> Triangle::getTextureUv(const Intersection &intersection) const {
    return texture_[0] + intersection.uv.x * (texture_[1] - texture_[0]) + intersection.uv.y * (texture_[2] - texture_[0]);
}

void Triangle::setNormalCoordinates(const Vector &n1, const Vector &n2,
                                    const Vector &n3) {
    normal_[0] = n1;
    normal_[1] = n2;
    normal_[2] = n3;
}

void Triangle::getBounds(AxisAlignedBoundingBox &aabb) const {
    aabb.contain(vertex_[0]);
    aabb.contain(vertex_[1]);
    aabb.contain(vertex_[2]);
}

void Triangle::clipToAabb(const AxisAlignedBoundingBox &aabb, AxisAlignedBoundingBox &clippingAabb) const {
    clippingAabb = AxisAlignedBoundingBox();

    // Get plane
    Vector ext[2] = {aabb.getMinPosition(), aabb.getMaxPosition()};
    Vector Q[3] = { vertex_[1] - vertex_[0],
                    vertex_[2] - vertex_[1],
                    vertex_[0] - vertex_[2] };
    Vector intersection[2];

    // Add contained points so that we only cater for points on
    // the plane of intersection later. Adding vertices here also
    // caters for points that are parallel to the plane and lie on it
    bool allVerticesContained = true;
    for (const auto &vertex : vertex_) {
        if (aabb.contains(vertex)) {
            clippingAabb.contain(vertex);
        } else {
            allVerticesContained = false;
        }
    }

    if (allVerticesContained) {
        return;
    }

    // For each dimension
    for (int d = 0; d < 3; ++d) {

        // For each extreme (min/max)
        for (auto &e : ext) {

            // Expecting max 2 plane intersections
            int i = 0;

            // For each triangle edge
            for (int q = 0; q < 3/* && i < 2*/; ++q) {

                // Intersect Q with plane and generate points
                if (Q[q].xyz[d] != 0.f) {

                    const float t = (e.xyz[d] - vertex_[q].xyz[d]) / Q[q].xyz[d];
                    // did we intersect the plane?
                    if (t > 0.f && t <= 1.f) {
                        if (i == 2) {
                            throw std::runtime_error("Registering 3rd intersection in Triangle clipToAabb");
                        }
                        intersection[i++] = vertex_[q] + Q[q] * t;

                        // Todo: Check whether we need to enforce a constant in the dimension
                        // for numerical stability
                        intersection[i - 1].xyz[d] = e.xyz[d];
                    }

                }

            }

            // this means it was handled already
            if (i < 2) {
                continue;
            }

            // Test whether both points are contained, if so, we're done
            allVerticesContained = true;
            for (const auto& in : intersection) {
                if (aabb.contains(in)) {
                    clippingAabb.contain(in);
                } else {
                    allVerticesContained = false;
                }
            }

            if (allVerticesContained) {
                continue;
            }

            // Points are not contained so draw a line and clip (test 4 edges)
            Vector line = intersection[1] - intersection[0];

            // Constrain qPoint within extremes for this plane
            for (int g = 0; g < 3; ++g) {
                if (g == d) {
                    continue;
                }

                int h = (g + 1) % 3;
                if (h == d) {
                    h = (h + 1) % 3;
                }

                // For each extreme (min/max)
                for (const auto &e : ext) {
                    if (line.xyz[g] != 0.f) {
                        const float t = (e.xyz[g] - intersection[0].xyz[g]) / line.xyz[g];
                        if (t > 0.f && t <= 1.f) {
                            float hVal = intersection[0].xyz[h] + line.xyz[h] * t;
                            if (hVal >= ext[0].xyz[h] && hVal <= ext[1].xyz[h]) {
                                Vector point;
                                // Setting for numerical stability
                                point.xyz[d] = intersection[0].xyz[d];
                                point.xyz[g] = e.xyz[g];
                                point.xyz[h] = hVal;
                                clippingAabb.contain(point);
                            }
                        }
                    }
                }
            }
        }
    }
}

Vector2<float> Triangle::project(const std::vector<Vector> &vertices, const Vector &axis) {
    Vector2<float> bounds(Mathematics::Float::Infinity, Mathematics::Float::NegativeInfinity);
    for(const auto& vertex: vertices) {
        const float dot = axis * vertex;
        bounds.x = Mathematics::Functions::min(bounds.x, dot);
        bounds.y = Mathematics::Functions::max(bounds.y, dot);
    }
    return bounds;
}

// adapted from https://stackoverflow.com/questions/17458562/efficient-aabb-triangle-intersection-in-c-sharp
bool Triangle::partOf(const AxisAlignedBoundingBox &aabb) const {
    // Is any part of this triangle inside the Aabb?
//    AxisAlignedBoundingBox triAabb;
//    getBounds(triAabb);
//    if (!triAabb.partOf(aabb))
//        return false;

    //
    Vector aabbNormals[3] = {Vector::UnitX, Vector::UnitY, Vector::UnitZ};
    std::vector<Vector> boxVertices = aabb.getVertices();
    std::vector<Vector> triVertices = { vertex_[0], vertex_[1], vertex_[2] };

    Vector2<float> boxBounds;
    Vector2<float> triBounds;

    // Check box faces - this should be equivalent to !triAabb.partOf(aabb) logic
    for (int i = 0; i < 3; ++i) {
        triBounds = project(triVertices, aabbNormals[i]);
        if (triBounds.y < aabb.getMinPosition().xyz[i] || triBounds.x > aabb.getMaxPosition().xyz[i]) {
            return false;
        }
    }

    // Check Triangle face
    const Vector faceNormal = getNormal();
    boxBounds = project(boxVertices, faceNormal);
    // projecting all triangle vertices to decrease numerical errors
    triBounds = project(triVertices, faceNormal);
    if (boxBounds.y < triBounds.x || boxBounds.x > triBounds.y) {
        return false;
    }

    // Get triangle edges
    Vector triEdges[3] = { vertex_[2] - vertex_[1],
                           vertex_[1] - vertex_[0],
                           vertex_[0] - vertex_[2] };

    // Test edge cross products
    for (const auto &triEdge : triEdges) {
        for (const auto &aabbNormal : aabbNormals) {
            Vector axis = triEdge ^ aabbNormal;
            boxBounds = project(boxVertices, axis);
            triBounds = project(triVertices, axis);
            if (boxBounds.y < triBounds.x || boxBounds.x > triBounds.y) {
                return false;
            }
        }
    }

    return true;
}

Vector Triangle::samplePoint(float r1, float r2, Vector& surfaceNormal) const {
    // Do not reject sample if outside of triangle, instead transform it.
    if (r1 + r2 > 1.f) {
        r1 = 1 - r1;
        r2 = 1 - r2;
    }
    const Vector Q1 = vertex_[1] - vertex_[0];
    const Vector Q2 = vertex_[2] - vertex_[0];

    surfaceNormal = ~(Q1 ^ Q2);
    return vertex_[0] + r1 * Q1 + r2 * Q2;
}

Vector Triangle::getCentroid() const {
    return (vertex_[0] + vertex_[1] + vertex_[2]) / 3.f ;
}


/* 
 * File:   Cylinder.h
 * Author: Kevin
 *
 * Created on 30 December 2013, 15:37
 */

#ifndef CYLINDER_H
#define	CYLINDER_H

#include "IBoundingShape.h"
#include "Mathematics/BoundedRay.h"
#include "Shape/Intersection.h"

namespace Candela
{
    namespace Shape
    {
        class Cylinder
                : public IBoundingShape
        {
        public:
            Cylinder(const Mathematics::Vector& center,
                     const Mathematics::Vector& direction, float radius);
            
            /* Inherited from IShape */
            std::string toString() const override;
            bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const override;
            Mathematics::Vector getUnitNormal(const Intersection& intersection) const override;
            float getSurfaceArea() const override;
            Mathematics::Vector getCentroid() const override;
            
            /* Inherited from IBoundingShape */
            virtual float getVolume() const;
            virtual void contain(const IShape& shape);
            virtual void contain(const std::vector<const IShape*>& shapes);
            
        private:
            //in this case direction can be interpreted as ending point of cylinder bounds
            Mathematics::Vector centre_, direction_; //S, Q
            float radius_; 
            //store some k bounds? if k is not stored, must check between 0 and 1
        };
    }
}

#endif	/* CYLINDER_H */


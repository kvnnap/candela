/* 
 * File:   IShape.cpp
 * Author: Kevin
 * 
 * Created on 22 December 2013, 21:39
 */

#include <stdexcept>
#include <sstream>

#include "Shape/IShape.h"
#include "IShape.h"


using namespace std;
using namespace Candela::Shape;
using namespace Candela::Mathematics;

IShape::~IShape() {}

std::string IShape::toString() const
{
    using namespace std;
    ostringstream sstr;
    sstr << "IShape - Address = " << this;
    return sstr.str();
}

Vector2<float> IShape::getTextureUv(const Intersection &intersection) const {
    return Mathematics::Vector2<float>();
}

void IShape::getBounds(AxisAlignedBoundingBox &aabb) const {
    throw runtime_error("getBounds Not Implemented: ShapeInfo: " + toString());
}

bool IShape::partOf(const AxisAlignedBoundingBox &aabb) const {
    return false;
}

Candela::Mathematics::Vector IShape::samplePoint(float r1, float r2, Vector& surfaceNormal) const {
    return Vector();
}

float IShape::getPdf(const Intersection &intersection) const {
    return 1.f / getSurfaceArea();
}

void IShape::clipToAabb(const AxisAlignedBoundingBox &aabb, AxisAlignedBoundingBox &clippingAabb) const {
    throw runtime_error("clipToAabb Not Implemented: ShapeInfo: " + toString());
}

std::ostream& Candela::Shape::operator <<(std::ostream& strm, const IShape& ishape)
{
    return strm << ishape.toString();
}
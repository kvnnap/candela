/* 
 * File:   Triangle.h
 * Author: Kevin
 *
 * Created on 02 March 2014, 18:00
 */

#ifndef TRIANGLE_H
#define	TRIANGLE_H

#include <vector>

#include "AbstractShape.h"
#include "Mathematics/Vector3.h"

namespace Candela 
{
    namespace Shape 
    {
        class Triangle 
                : public AbstractShape
        {
        public:
            Triangle(const Mathematics::Vector& O, const Mathematics::Vector& A, const Mathematics::Vector& B);
            
            /* Inherited from IShape */
            std::string toString() const override;
            bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const override;
            Mathematics::Vector getUnitNormal(const Intersection& intersection) const override;
            float getSurfaceArea() const override;
            void getBounds(AxisAlignedBoundingBox& aabb) const override;
            void clipToAabb(const AxisAlignedBoundingBox &aabb, AxisAlignedBoundingBox &clippingAabb) const override;
            static Mathematics::Vector2<float> project(const std::vector<Mathematics::Vector>& vertices,
                    const Mathematics::Vector& axis);
            bool partOf(const AxisAlignedBoundingBox& aabb) const override;

            Mathematics::Vector getNormal() const;
            Mathematics::Vector getUnitNormal() const;

            // Perhaps move this in subclass
            void setTextureCoordinates(const Mathematics::Vector2<float>& t1,
                                       const Mathematics::Vector2<float>& t2,
                                       const Mathematics::Vector2<float>& t3);
            void setNormalCoordinates (const Mathematics::Vector& n1,
                                       const Mathematics::Vector& n2,
                                       const Mathematics::Vector& n3);
            Mathematics::Vector2<float> getTextureUv(const Intersection& intersection) const override;
            Mathematics::Vector samplePoint(float r1, float r2, Mathematics::Vector& surfaceNormal) const override;
            Mathematics::Vector getCentroid() const override;


        private:
            Mathematics::Vector vertex_[3];

            // Perhaps move this in subclass called TexNormTriangle
            Mathematics::Vector2<float> texture_[3];
            Mathematics::Vector normal_[3];
        };
    }
}



#endif	/* TRIANGLE_H */
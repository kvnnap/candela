/* 
 * File:   AxisAlignedBoundingBox.cpp
 * Author: Kevin
 * 
 * Created on 15 March 2014, 22:38
 */

#include "AxisAlignedBoundingBox.h"
#include "Mathematics/Float.h"
#include <sstream>

using namespace Candela::Shape;
using namespace Candela::Mathematics;

AxisAlignedBoundingBox::AxisAlignedBoundingBox() 
{
}

AxisAlignedBoundingBox::AxisAlignedBoundingBox(
        const Vector& minPosition,
        const Vector& maxPosition)
        : minPosition_ ( minPosition ), maxPosition_ ( maxPosition )
{

}

AxisAlignedBoundingBox::AxisAlignedBoundingBox(
        const Vector& centre,
        float radius) 
        : minPosition_ ( centre - radius ), 
        maxPosition_ ( centre + radius )
{

}

bool AxisAlignedBoundingBox::intersect(const BoundedRay& ray, Intersection& intersection) const
{
    //right left - x normal
    const Vector& rPos = ray.getPosition();
    const Vector& rDir = ray.getDirection();
    const Vector& rMin = ray.getMinPoint();
    float tTemp;
    
    if(rMin.x >= minPosition_.x && rMin.x <= maxPosition_.x &&
       rMin.y >= minPosition_.y && rMin.y <= maxPosition_.y &&
       rMin.z >= minPosition_.z && rMin.z <= maxPosition_.z)
    {
        //INSIDE BOX
        float t = Float::Infinity;
        //find the minimum t-Value allowed in order for ray to stay in box
        for(int i = 0; i < 3; i++)
        {
            if(rDir.xyz[i] != 0.f)
            {
                tTemp = rDir.xyz[i] < 0.f ? (minPosition_.xyz[i] - rPos.xyz[i]) / rDir.xyz[i] 
                                   : (maxPosition_.xyz[i] - rPos.xyz[i]) / rDir.xyz[i];
                if(tTemp < t && tTemp >= ray.getMin())
                {
                    t = tTemp;
                }
            }
        }
        if(ray.getMax() < t)
        {
            return false;
        }else{
            intersection.Type = Intersection::Internal;
            intersection.Distance = t;
            intersection.Intersectant = this;
            return true;
        }
    }else
    {
        //loop through faces
        for(int i = 0; i < 3; i++)
        {
            //from outside
            if(rDir.xyz[i] < 0.f)
            {//check right face or up or inward
                tTemp = (maxPosition_.xyz[i] - rPos.xyz[i]) / rDir.xyz[i];
            }else if(rDir.xyz[i] > 0.f)
            {//check left face or down or outward
                tTemp = (minPosition_.xyz[i] - rPos.xyz[i]) / rDir.xyz[i];
            }else
            {//cant perform division by zero
                continue;
            }

            if(tTemp >= ray.getMin() && tTemp <= ray.getMax())
            {
                const int sec = (i + 1) % 3;
                const int third = (i + 2) % 3;
                //create intersection point
                const Vector& rXYZ = ray.getPoint(tTemp);
                //check whether it is contained
                if(rXYZ.xyz[sec] >= minPosition_.xyz[sec] && rXYZ.xyz[sec] <= maxPosition_.xyz[sec] && 
                   rXYZ.xyz[third] >= minPosition_.xyz[third] && rXYZ.xyz[third] <= maxPosition_.xyz[third])
                {
                    //intersected - no other intersections will occur!
                    intersection.Type = Intersection::External;
                    intersection.Distance = tTemp;
                    intersection.Intersectant = this;
                    return true;

                }
            }
        }
    }
    return false;
}


float AxisAlignedBoundingBox::getSurfaceArea() const 
{   
    const Mathematics::Vector size = maxPosition_ - minPosition_;
    return 2 * (size.x * (size.y + size.z) + size.y * size.z);
}


std::string AxisAlignedBoundingBox::toString() const 
{
    using namespace std;
    ostringstream sstr;
    sstr << "Plane Shape - Address = " << this
         << "\n\tMinPosition: " << minPosition_  << "\n\tMaxPosition: " << maxPosition_; 
    return sstr.str();
}

float AxisAlignedBoundingBox::getVolume() const {
    const Mathematics::Vector size = maxPosition_ - minPosition_;
    return size.x * size.y * size.z;
}

void AxisAlignedBoundingBox::contain(const IShape& shape) {
    // Contain this shape into this AABB
    if (minPosition_.isNotSet() && maxPosition_.isNotSet()) {
        shape.getBounds(*this);
    } else {
        AxisAlignedBoundingBox aabb;
        shape.getBounds(aabb);
        minPosition_.min(aabb.minPosition_);
        maxPosition_.max(aabb.maxPosition_);
    }
}

void AxisAlignedBoundingBox::contain(const Vector &point) {
    if (minPosition_.isNotSet() && maxPosition_.isNotSet()) {
        minPosition_ = maxPosition_ = point;
    } else {
        minPosition_.min(point);
        maxPosition_.max(point);
    }
}

void AxisAlignedBoundingBox::contain(const std::vector<const IShape*>& shapes) {
    for (const IShape* shape : shapes) {
        contain(*shape);
    }
}

Vector AxisAlignedBoundingBox::getUnitNormal(const Intersection& intersection) const {
    // TODO
    return Mathematics::Vector();
}

void AxisAlignedBoundingBox::getBounds(AxisAlignedBoundingBox &aabb) const {
    aabb = *this;
}

const Vector &AxisAlignedBoundingBox::getMinPosition() const {
    return minPosition_;
}

const Vector &AxisAlignedBoundingBox::getMaxPosition() const {
    return maxPosition_;
}

bool AxisAlignedBoundingBox::partOf(const AxisAlignedBoundingBox &aabb) const {
    return  maxPosition_.x >= aabb.minPosition_.x && minPosition_.x <= aabb.maxPosition_.x &&
            maxPosition_.y >= aabb.minPosition_.y && minPosition_.y <= aabb.maxPosition_.y &&
            maxPosition_.z >= aabb.minPosition_.z && minPosition_.z <= aabb.maxPosition_.z;
}

void AxisAlignedBoundingBox::splitUsingPartitionInAxis(uint8_t axis, float partition,
                                                       AxisAlignedBoundingBox &left,
                                                       AxisAlignedBoundingBox &right) const {
    left = right = *this;
    left.maxPosition_.xyz[axis] = partition;
    right.minPosition_.xyz[axis] = partition;
}

void AxisAlignedBoundingBox::spaceOut(float amount) {
    Vector dir = ~(maxPosition_ - minPosition_);
    minPosition_ -= dir * amount;
    maxPosition_ += dir * amount;
}

bool AxisAlignedBoundingBox::contains(const Vector &point) const {
    return (point.x >= minPosition_.x && point.x <= maxPosition_.x &&
            point.y >= minPosition_.y && point.y <= maxPosition_.y &&
            point.z >= minPosition_.z && point.z <= maxPosition_.z);
}

void AxisAlignedBoundingBox::setMaxAxis(uint8_t axis, float value) {
    maxPosition_.xyz[axis] = value;
}

void AxisAlignedBoundingBox::setMinAxis(uint8_t axis, float value) {
    minPosition_.xyz[axis] = value;
}

Vector AxisAlignedBoundingBox::getCentroid() const {
    return minPosition_ + 0.5f * (maxPosition_ - minPosition_);
}

std::vector<Vector> AxisAlignedBoundingBox::getVertices() const {
    using namespace std;
    vector<Vector> vertices;
    vertices.reserve(8);
    Vector vec[2] = { minPosition_, maxPosition_};
    for (int i = 0; i < 8; ++i) {
        vertices.emplace_back(vec[i & 1].x, vec[(i >> 1) & 1].y,  vec[(i >> 2) & 1].z);
    }

    return vertices;
}

// Todo: Improve this, with epsilon we might lose some cases
bool AxisAlignedBoundingBox::intersect(const BoundedRay &ray, float &a, float &b) const {
    // Find ray entry and exit point
    if (contains(ray.getMinPoint())) {
        // Inside the bounding box
        Candela::Shape::Intersection localIntersection;
        a = ray.getMin();
        b = intersect(ray, localIntersection) ?
            localIntersection.Distance : ray.getMax();
    } else {
        // Outside the bounding box
        Candela::Shape::Intersection localIntersection;
        if (intersect(ray, localIntersection) ) {
            a = localIntersection.Distance;
            BoundedRay rayBox = ray;
            rayBox.setMinAndAdvanceByEpsilon(a);
            b = intersect(rayBox, localIntersection) ?
                localIntersection.Distance : rayBox.getMax();
        } else {
            return false;
        }
    }

    return true;
}



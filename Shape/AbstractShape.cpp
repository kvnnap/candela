//
// Created by kvnna on 12/05/2018.
//

#include "AbstractShape.h"

using namespace std;
using namespace Candela::Shape;

AbstractShape::AbstractShape()
    : groupId ()
{}

void AbstractShape::setGroupId(uint32_t groupId) {
    this->groupId = groupId;
}

uint32_t AbstractShape::getGroupId() const {
    return groupId;
}

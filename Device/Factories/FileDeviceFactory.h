//
// Created by kvnna on 18/12/2017.
//

#ifndef CANDELA_FILEDEVICEFACTORY_H
#define CANDELA_FILEDEVICEFACTORY_H

#include "Factory/Factory.h"
#include "Device/IDevice.h"

namespace Candela
{
    namespace Device
    {
        class FileDeviceFactory
            : public Factory::Factory<IDevice>
        {
        public:
            std::unique_ptr<IDevice> create() const override;
            std::unique_ptr<IDevice> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}




#endif //CANDELA_FILEDEVICEFACTORY_H

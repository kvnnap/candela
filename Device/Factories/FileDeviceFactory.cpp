//
// Created by kvnna on 18/12/2017.
//

#include "Device/FileDevice.h"
#include "FileDeviceFactory.h"

using namespace std;
using namespace Candela::Device;
using namespace Candela::Configuration;

unique_ptr<IDevice> FileDeviceFactory::create() const {
    return unique_ptr<IDevice>();
}

unique_ptr<IDevice> FileDeviceFactory::create(const ConfigurationNode &config) const {

    string fileTypeStr = **config["FileType"];
    FileDevice::FileType fileType = fileTypeStr == "PPM" ? FileDevice::PPM :
                                    fileTypeStr == "PNG" ? FileDevice::PNG :
                                    throw runtime_error("Unsupported file type for FileDevice: '" + fileTypeStr + "'");

    unique_ptr<FileDevice> device = make_unique<FileDevice>(
            **config["FileName"],
            fileType,
            **config["DirectoryPath"],
            config["Sequence"]->read<bool>()
    );
    device->setPerformAlphaCorrection(config["AlphaCorrection"]->read<bool>());
    device->setName(**config["Name"]);
    return device;
}

//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_FILEDEVICE_H
#define MSC_CANDELA_FILEDEVICE_H

#include <atomic>
#include "IDevice.h"
#include "Renderer/RadianceBuffer.h"

namespace Candela
{
    namespace Device
    {
        class FileDevice
            : public IDevice
        {
        public:
            enum FileType : uint8_t {
                PPM = 0,
                PNG = 1
            };

            FileDevice(const std::string& fileName = "", FileType p_fileType = PPM, const std::string& p_directoryPath = ".", bool p_writeToSeqOfFiles = false);
            void setWriteToSequenceOfFiles(bool p_writeToSeqOfFiles);
            void setDirectoryPath(const std::string &p_directoryPath);
            void setFileName(const std::string &p_fileName);
            void setFileType(FileType p_fileType);
            void resetSeqNum();
            void writePPM(const std::string& filePath, const Renderer::RadianceBuffer& radianceBuffer,
                          Renderer::RadianceContent::RadianceType radianceType);
            void writePNG(const std::string &filePath, const Renderer::RadianceBuffer &buffer,
                          Renderer::RadianceContent::RadianceType type);

            // overridden virtuals
            void write(const Renderer::RadianceBuffer& radianceBuffer, Renderer::RadianceContent::RadianceType radianceType) override;

            std::string toString() const override;
            // The name of this Device
            std::string getName() const override;

            void setName(const std::string& name) override;
        private:
            Spectrum::RgbSpectrum getRgbSpectrum(const Renderer::RadianceContent &radianceContent,
                                                 const Renderer::RadianceContent::RadianceType &radianceType) const;

            std::string name;
            std::string fileName;
            std::string directoryPath;
            std::atomic<size_t> seqNum;

            FileType fileType;

            bool writeToSeqOfFiles;
            bool performAlphaCorrection;
        public:
            bool isPerformAlphaCorrection() const;

            void setPerformAlphaCorrection(bool performAlphaCorrection);
        };
    }
}


#endif //MSC_CANDELA_FILEDEVICE_H

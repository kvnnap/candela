//
// Created by kvnna on 25/06/2016.
//

#include <sstream>
#include "IDevice.h"

using namespace std;
using namespace Candela::Device;

IDevice::~IDevice() {
}

std::string IDevice::toString() const {
    ostringstream sstr;
    sstr << "IDevice - Address = " << this
         << "\n\tName: " << getName();
    return sstr.str();
}

std::ostream& Candela::Device::operator <<(std::ostream& strm, const IDevice& camera)
{
    return strm << camera.toString();
}
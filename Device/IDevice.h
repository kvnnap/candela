//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_IDEVICE_H
#define MSC_CANDELA_IDEVICE_H

#include "Renderer/RadianceBuffer.h"

namespace Candela
{
    namespace Device
    {
        class IDevice {
        public:
            virtual ~IDevice();

            virtual std::string toString() const;

            // The name of this device
            virtual std::string getName() const = 0;
            virtual void setName(const std::string& name) = 0;

            // Write the buffer
            virtual void write(const Renderer::RadianceBuffer& radianceBuffer, Renderer::RadianceContent::RadianceType radianceType) = 0;
        };

        std::ostream& operator<< (std::ostream& strm, const IDevice& camera);
    }
}

#endif //MSC_CANDELA_IDEVICE_H

//
// Created by kvnna on 25/06/2016.
//

#include <cstdio>
#include <sstream>
#include <png.h>

#include "Mathematics/Vector3.h"
#include "Spectrum/RgbSpectrum.h"
#include "FileDevice.h"

using namespace std;
using namespace Candela::Device;
using namespace Candela::Spectrum;
using namespace Candela::Renderer;

FileDevice::FileDevice(const string& fileName, FileType p_fileType, const string& directoryPath, bool writeToSeqOfFiles)
    : fileName (fileName),
      directoryPath ( directoryPath ),
      seqNum (0),
      fileType(p_fileType),
      writeToSeqOfFiles ( writeToSeqOfFiles )
{ }

void FileDevice::writePPM(const string& filePath, const RadianceBuffer &radianceBuffer,
                          RadianceContent::RadianceType radianceType) {
    // Open File
    FILE *fp = fopen(filePath.c_str(), "wb"); /* b - binary mode */
    if (fp == nullptr) {
        throw runtime_error("Could not open file: " + filePath);
    }
    fprintf(fp, "P6\n%zu %zu\n255\n", radianceBuffer.getWidth(), radianceBuffer.getHeight());
    for (size_t y = 0; y < radianceBuffer.getHeight(); ++y) {
        for (size_t x = 0; x < radianceBuffer.getWidth(); ++x) {
            uint8_t color[3];
            RgbSpectrum rgbSpectrum = getRgbSpectrum(radianceBuffer(x, y), radianceType);
            const Mathematics::Vector& rgb = rgbSpectrum.getAbcReference();
            color[0] = static_cast<uint8_t>(rgb.x * 255);
            color[1] = static_cast<uint8_t>(rgb.y * 255);
            color[2] = static_cast<uint8_t>(rgb.z * 255);
            fwrite(color, 1, 3, fp);
        }
    }
    fclose(fp);
}

RgbSpectrum
FileDevice::getRgbSpectrum(const RadianceContent &radianceContent, const RadianceContent::RadianceType &radianceType) const {
    if (radianceType != RadianceContent::FINAL) {
        throw runtime_error("Only RadianceContent::Final is supported");
    }
//    RgbSpectrum rgbSpectrum = radianceType == RadianceContent::FINAL ? radianceContent.radiance.toTristimulusSRGB() :
//                                      radianceType == RadianceContent::DIRECT ? radianceContent.direct.toTristimulusSRGB() :
//                                      radianceContent.indirect.toTristimulusSRGB();

    RgbSpectrum rgbSpectrum = radianceContent.radiance.toTristimulusSRGB();
    if (performAlphaCorrection) {
        rgbSpectrum.alphaCorrect();
    } else {
        rgbSpectrum.toneMap();
    }
    
    return rgbSpectrum;
}

void FileDevice::writePNG(const string &filePath, const RadianceBuffer &buffer,
                          RadianceContent::RadianceType type) {
    // To output errors on
    stringstream sstr;
    bool error = false;

    // Required variables
    FILE *fp = nullptr;
    png_structp png_ptr = nullptr;
    png_infop info_ptr = nullptr;
    png_bytepp row_pointers = nullptr;

    // Open file for writing (binary mode)
    fp = fopen(filePath.c_str(), "wb");
    if (fp == nullptr) {
        error = true;
        sstr << "FileDevice [PNG]: Could not open file '" << filePath << "' for writing";
        goto finalise;
    }

    // Initialise png struct - png_ptr
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    if (png_ptr == nullptr) {
        error = true;
        sstr << "FileDevice [PNG]: Could not allocate png write struct'";
        goto finalise;
    }

    // Initialise info struct - info_ptr
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == nullptr) {
        error = true;
        sstr << "FileDevice [PNG]: Could not allocate png info struct'";
        goto finalise;
    }

    // Setup exception point
    if (setjmp(png_jmpbuf(png_ptr))) {
        error = true;
        sstr << "FileDevice [PNG]: Error exception - longjmp called'";
        goto finalise;
    }

    // Init png io
    png_init_io(png_ptr, fp);

    // Set header (8 bit colour depth)
    png_set_IHDR(png_ptr,
                 info_ptr,
                 static_cast<png_uint_32>(buffer.getWidth()),
                 static_cast<png_uint_32>(buffer.getHeight()),
                 8,
                 PNG_COLOR_TYPE_RGB,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);

    // Write header
    png_write_info(png_ptr, info_ptr);

    // Allocate rows to write image and copy our image data
    row_pointers = static_cast<png_bytepp>(malloc(sizeof(png_bytep) * buffer.getHeight()));
    for (size_t y = 0; y < buffer.getHeight(); ++y) {
        // Allocate whole row
        row_pointers[y] = static_cast<png_bytep>(malloc(png_get_rowbytes(png_ptr, info_ptr)));

        // Copy our row of data
        for (size_t x = 0; x < buffer.getWidth(); ++x) {
            RgbSpectrum rgbSpectrum = getRgbSpectrum(buffer(x, y), type);
            const Mathematics::Vector& rgb = rgbSpectrum.getAbcReference();
            png_bytep pixelPointer = &row_pointers[y][x * 3];
            pixelPointer[0] = static_cast<uint8_t>(rgb.x * 255);
            pixelPointer[1] = static_cast<uint8_t>(rgb.y * 255);
            pixelPointer[2] = static_cast<uint8_t>(rgb.z * 255);
        }
    }

    // Write png image in one go
    png_write_image(png_ptr, row_pointers);

    // Free structures and do error handling if required
    finalise:
    if (row_pointers != nullptr) {
        for (size_t y = 0; y < buffer.getHeight(); ++y) {
            free(row_pointers[y]);
        }
        free(row_pointers);
    }
    if (info_ptr != nullptr) {
        png_destroy_info_struct(png_ptr, &info_ptr);
    }
    if (png_ptr != nullptr) {
        png_destroy_write_struct(&png_ptr, nullptr);
    }
    if (fp != nullptr) {
        fclose(fp);
    }
    if (error) {
        sstr << " - During write to file '" << filePath << "'" << endl;
        throw runtime_error(sstr.str());
    }
}

void FileDevice::write(const RadianceBuffer &radianceBuffer,
                       RadianceContent::RadianceType radianceType) {
    // Check file name
    if (fileName.length() == 0) {
        return;
    }
    // Construct file path
    string constructedPath = (directoryPath.length() == 0 ? "." : directoryPath) + "/" + fileName;
    if (writeToSeqOfFiles) {
        constructedPath += "-";
        constructedPath += to_string(++seqNum);
    }
    switch (fileType) {
        case PPM:
            constructedPath += ".ppm";
            writePPM(constructedPath, radianceBuffer, radianceType);
            break;
        case PNG:
            constructedPath += ".png";
            writePNG(constructedPath, radianceBuffer, radianceType);
            break;
        default:
            throw runtime_error("File Device: Unsupported file type");
    }
}

void FileDevice::setWriteToSequenceOfFiles(bool p_writeToSeqOfFiles) {
    writeToSeqOfFiles = p_writeToSeqOfFiles;
}

void FileDevice::setDirectoryPath(const std::string &p_filePath) {
    directoryPath = p_filePath;
}

void FileDevice::resetSeqNum() {
    seqNum = 0;
}


void FileDevice::setFileType(FileType p_fileType) {
    fileType = p_fileType;
}

void FileDevice::setFileName(const std::string &p_fileName) {
    fileName = p_fileName;
}

string FileDevice::getName() const {
    return name;
}

void FileDevice::setName(const string &name) {
    this->name = name;
}

std::string FileDevice::toString() const {
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << IDevice::toString()
         << "\n\tFile Name: " << fileName
            << "\n\tFile Type: " << fileType
            << "\n\tDirectory Path: " << directoryPath
            << "\n\tOutput in Sequence: " << writeToSeqOfFiles
            ;
    return sstr.str();
}

bool FileDevice::isPerformAlphaCorrection() const {
    return performAlphaCorrection;
}

void FileDevice::setPerformAlphaCorrection(bool performAlphaCorrection) {
    this->performAlphaCorrection = performAlphaCorrection;
}


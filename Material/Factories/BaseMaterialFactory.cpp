//
// Created by kvnna on 21/12/2017.
//

#include "BaseMaterialFactory.h"
#include "Material/BaseMaterial.h"
#include "Environment/Environment.h"

using namespace std;
using namespace Candela::Material;
using namespace Candela::Configuration;
using namespace Candela::Spectrum;

BaseMaterialFactory::BaseMaterialFactory(Candela::Environment::Environment &environment)
    : environment (environment)
{}

unique_ptr<IMaterial> BaseMaterialFactory::create() const {
    return make_unique<BaseMaterial>();
}

unique_ptr<IMaterial> BaseMaterialFactory::create(const ConfigurationNode &config) const {
    unique_ptr<BaseMaterial> material = make_unique<BaseMaterial>();

    auto& textureInstanceManager = environment.getTextureManager().getInstanceManager();
    if (config.keyExists("Ambient")) {
        material->setAmbientTexture(&textureInstanceManager.getInstance(*config["Ambient"]));
    }

    if (config.keyExists("Diffuse")) {
        material->setDiffuseTexture(&textureInstanceManager.getInstance(*config["Diffuse"]));
    }

    if (config.keyExists("Specular")) {
        material->setSpecularTexture(&textureInstanceManager.getInstance(*config["Specular"]));
    }

    if (config.keyExists("Reflection")) {
        material->setReflectionTexture(&textureInstanceManager.getInstance(*config["Reflection"]));
    }

    if (config.keyExists("Refraction")) {
        material->setRefractionTexture(&textureInstanceManager.getInstance(*config["Refraction"]));
    }

    if (config.keyExists("DiffuseCoefficient")) {
        material->setDiffuse(static_cast<float>(config["DiffuseCoefficient"]->read<double>()));
    } else {
        material->setDiffuse(1.f); // default to 1
    }

    if (config.keyExists("SpecularCoefficient")) {
        material->setSpecular(static_cast<float>(config["SpecularCoefficient"]->read<double>()));
    }

    if (config.keyExists("SpecularExponent")) {
        material->setSpecularExponent(static_cast<float>(config["SpecularExponent"]->read<double>()));
    }

    if (config.keyExists("RefractiveIndex")) {
        material->setRefractiveIndex(static_cast<float>(config["RefractiveIndex"]->read<double>()));
    }

    return material;
}

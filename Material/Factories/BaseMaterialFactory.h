//
// Created by kvnna on 21/12/2017.
//

#ifndef CANDELA_MATTEMATERIALFACTORY_H
#define CANDELA_MATTEMATERIALFACTORY_H

#include "Factory/Factory.h"
#include "Material/IMaterial.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Material
    {
        class BaseMaterialFactory
            : public Factory::Factory<IMaterial>
        {
        public:
            BaseMaterialFactory(Environment::Environment& environment);
            std::unique_ptr<IMaterial> create() const override;
            std::unique_ptr<IMaterial> create(const Configuration::ConfigurationNode& config) const override;

        private:
            Environment::Environment& environment;
        };
    }
}

#endif //CANDELA_MATTEMATERIALFACTORY_H

////
//// Created by kvnna on 29/06/2016.
////
//
//#ifndef MSC_CANDELA_WAVEFRONTMATERIAL_H
//#define MSC_CANDELA_WAVEFRONTMATERIAL_H
//
//#include <memory>
//
//#include "Mathematics/Vector3.h"
//#include "Spectrum/RgbSpectrum.h"
//#include "Texture/MemoryTexture.h"
//#include "IMaterial.h"
//
//namespace Candela {
//    namespace Material {
//        class WavefrontMaterial
//            : public IMaterial
//        {
//        public:
//
//            std::string toString() const override;
//            std::string getMaterialName() const override;
//            uint8_t getIlluminationMode() const override;
//            float getSpecularExponent() const override;
//            float getRefractiveIndex() const override;
//
//            const Texture::ITexture& getAmbientTexture() const override;
//            const Texture::ITexture& getDiffuseTexture() const override;
//            const Texture::ITexture& getSpecularTexture() const override;
//            const Spectrum::ISpectrum& getRefractionSpectrum() const override;
//
//            void setMaterialName(const std::string& p_materialName);
//            void setIlluminationModel(uint8_t p_illuminationModel);
//            void setSpecularExponent(float p_specularExponent);
//            void setRefractiveIndex(float p_refractiveIndex);
//            void setRefraction(const Spectrum::RgbSpectrum& p_refraction);
//
//            void setAmbientTexture(std::unique_ptr<Texture::ITexture> p_ambienceTexture);
//            void setDiffuseTexture(std::unique_ptr<Texture::ITexture> p_diffuseTexture);
//            void setSpecularTexture(std::unique_ptr<Texture::ITexture> p_specularTexture);
//
//
//        private:
//            std::string materialName;
//
//            //Illumination model
//            uint8_t illuminationModel;
//
//            // Ns
//            float specularExponent;
//
//            // Ni
//            float refractiveIndex;
//
//            // Refraction Vector - Tf
//            Spectrum::RgbSpectrum refraction;
//
////            // Ambient - Ka
////            Spectrum::RgbSpectrum ambient;
////
////            // Diffuse - Kd
////            Spectrum::RgbSpectrum diffuse;
////
////            // Specular - Ks
////            Spectrum::RgbSpectrum specular;
//
//            // Textures
//            // map_Ka
//            std::unique_ptr<Texture::ITexture> ambienceTexture;
//            // map_Kd
//            std::unique_ptr<Texture::ITexture> diffuseTexture;
//            // map_Ks
//            std::unique_ptr<Texture::ITexture> specularTexture;
//        };
//    }
//}
//
//
//#endif //MSC_CANDELA_WAVEFRONTMATERIAL_H

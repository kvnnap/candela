//
// Created by kvnna on 26/06/2016.
//

#include <stdexcept>
#include "BaseMaterial.h"
#include "Texture/MatteTexture.h"
#include "Spectrum/RgbSpectrum.h"

using namespace std;
using namespace Candela::Material;
using namespace Candela::Texture;

BaseMaterial::BaseMaterial()
    : ambientTexture(),
      diffuseTexture(),
      specularTexture(),
      reflectionTexture(),
      refractionTexture(),
      diffuse(),
      specular(),
      specularExponent(),
      reflection(),
      refractiveIndex(1.f)
{ }

std::string BaseMaterial::toString() const {
    return "Matte Material";
}

void BaseMaterial::setName(const string &name) {
    this->name = name;
}

std::string BaseMaterial::getName() const {
    return name;
}

uint8_t BaseMaterial::getIlluminationMode() const {
    return 0;
}

float BaseMaterial::getSpecularExponent() const {
    return specularExponent;
}

float BaseMaterial::getRefractiveIndex() const {
    return refractiveIndex;
}

const ITexture &BaseMaterial::getTexture(const ITexture * texture) {
    return texture != nullptr ? *texture : MatteTexture::black;
}

const ITexture &BaseMaterial::getAmbientTexture() const {
    return getTexture(ambientTexture);
}

const ITexture &BaseMaterial::getDiffuseTexture() const {
    return getTexture(diffuseTexture);
}

const ITexture &BaseMaterial::getSpecularTexture() const {
    return getTexture(specularTexture);
}

const ITexture &BaseMaterial::getReflectionTexture() const {
    return getTexture(reflectionTexture);
}

const ITexture &BaseMaterial::getRefractionTexture() const {
    return getTexture(refractionTexture);
}

void BaseMaterial::setAmbientTexture(ITexture *ambientTexture) {
    this->ambientTexture = ambientTexture;
}

void BaseMaterial::setDiffuseTexture(ITexture *diffuseTexture) {
    this->diffuseTexture = diffuseTexture;
}

void BaseMaterial::setSpecularTexture(ITexture *specularTexture) {
    this->specularTexture = specularTexture;
}

void BaseMaterial::setRefractionTexture(ITexture *refractionTexture) {
    this->refractionTexture = refractionTexture;
}

void BaseMaterial::setSpecularExponent(float specularExponent) {
    this->specularExponent = specularExponent;
}

void BaseMaterial::setRefractiveIndex(float refractiveIndex) {
    this->refractiveIndex = refractiveIndex;
}

void BaseMaterial::setReflectionTexture(ITexture *reflectionTexture) {
    this->reflectionTexture = reflectionTexture;
}

float BaseMaterial::getDiffuse() const {
    return diffuse;
}

float BaseMaterial::getSpecular() const {
    return specular;
}

void BaseMaterial::setDiffuse(float diffuse) {
    this->diffuse = diffuse;
    validate();
}

void BaseMaterial::setSpecular(float specular) {
    this->specular = specular;
    validate();
}

void BaseMaterial::validate() const {
    if (this->diffuse + this->specular > 1.f) {
        throw runtime_error("Diffuse + Specular > 1. This will create energy in the scene.");
    }
}

//
// Created by kvnna on 25/06/2016.
//

#include "IMaterial.h"

using namespace std;
using namespace Candela::Material;

IMaterial::~IMaterial() {}

ostream& Candela::Material::operator <<(ostream& strm, const IMaterial& material)
{
    return strm << material.toString();
}
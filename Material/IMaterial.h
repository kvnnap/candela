//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_IMATERIAL_H
#define MSC_CANDELA_IMATERIAL_H

#include <string>
#include "Texture/ITexture.h"
#include "Spectrum/ISpectrum.h"

namespace Candela
{
    namespace Material
    {
        class IMaterial {
        public:
            virtual ~IMaterial();

            // The name of this Material
            virtual std::string getName() const = 0;
            virtual void setName(const std::string& name) = 0;

            virtual std::string toString() const = 0;

            virtual uint8_t getIlluminationMode() const = 0;
            virtual float getDiffuse() const = 0;
            virtual float getSpecular() const = 0;
            virtual float getSpecularExponent() const = 0;
            virtual float getRefractiveIndex() const = 0;

            virtual const Texture::ITexture& getAmbientTexture() const = 0;
            virtual const Texture::ITexture& getDiffuseTexture() const = 0;
            virtual const Texture::ITexture& getSpecularTexture() const = 0;
            virtual const Texture::ITexture& getReflectionTexture() const = 0;
            virtual const Texture::ITexture& getRefractionTexture() const = 0;
        };

        std::ostream& operator<< (std::ostream& strm, const IMaterial& material);
    }
}



#endif //MSC_CANDELA_IMATERIAL_H

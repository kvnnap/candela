////
//// Created by kvnna on 29/06/2016.
////
//
//#include "WavefrontMaterial.h"
//
//using namespace std;
//using namespace Candela::Material;
//using namespace Candela::Spectrum;
//using namespace Candela::Texture;
//
//std::string WavefrontMaterial::toString() const {
//    return "Wavefront Material";
//}
//
//std::string WavefrontMaterial::getMaterialName() const {
//    return materialName.length() > 0 ? materialName : "Unnamed";
//}
//
//void WavefrontMaterial::setMaterialName(const string &p_materialName) {
//    materialName = p_materialName;
//}
//
//void WavefrontMaterial::setIlluminationModel(uint8_t p_illuminationModel) {
//    illuminationModel = p_illuminationModel;
//}
//
//void WavefrontMaterial::setSpecularExponent(float p_specularExponent) {
//    specularExponent = p_specularExponent;
//}
//
//void WavefrontMaterial::setRefractiveIndex(float p_refractiveIndex) {
//    refractiveIndex = p_refractiveIndex;
//}
//
//void WavefrontMaterial::setRefraction(const RgbSpectrum &p_refraction) {
//    refraction = p_refraction;
//}
//
///*** Getters ***/
//uint8_t WavefrontMaterial::getIlluminationMode() const {
//    return illuminationModel;
//}
//
//float WavefrontMaterial::getSpecularExponent() const {
//    return specularExponent;
//}
//
//float WavefrontMaterial::getRefractiveIndex() const {
//    return refractiveIndex;
//}
//
//const ITexture &WavefrontMaterial::getAmbientTexture() const {
//    return *ambienceTexture.get();
//}
//
//const ITexture &WavefrontMaterial::getDiffuseTexture() const {
//    return *diffuseTexture.get();
//}
//
//const ITexture &WavefrontMaterial::getSpecularTexture() const {
//    return *specularTexture.get();
//}
//
//void WavefrontMaterial::setAmbientTexture(unique_ptr<ITexture> p_ambienceTexture) {
//    ambienceTexture = move(p_ambienceTexture);
//}
//
//void WavefrontMaterial::setDiffuseTexture(unique_ptr<ITexture> p_diffuseTexture) {
//    diffuseTexture = move(p_diffuseTexture);
//}
//
//void WavefrontMaterial::setSpecularTexture(unique_ptr<ITexture> p_specularTexture) {
//    specularTexture = move(p_specularTexture);
//}
//
//const ISpectrum &WavefrontMaterial::getRefractionSpectrum() const {
//    return refraction;
//}

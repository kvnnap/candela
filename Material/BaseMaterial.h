//
// Created by kvnna on 26/06/2016.
//

#ifndef MSC_CANDELA_BASEMATERIAL_H
#define MSC_CANDELA_BASEMATERIAL_H

#include <memory>
#include "Texture/ITexture.h"
#include "Spectrum/ISpectrum.h"
#include "IMaterial.h"

namespace Candela
{
    namespace Material
    {
        class BaseMaterial
            : public IMaterial
        {
        public:

            BaseMaterial();

            // The name of this Material
            std::string getName() const override ;
            void setName(const std::string& name) override ;


            std::string toString() const override;
            uint8_t getIlluminationMode() const override;
            float getDiffuse() const override;
            float getSpecular() const override;
            float getSpecularExponent() const override;
            float getRefractiveIndex() const override;

            const Texture::ITexture& getAmbientTexture() const override;
            const Texture::ITexture& getDiffuseTexture() const override;
            const Texture::ITexture& getSpecularTexture() const override;
            const Texture::ITexture& getReflectionTexture() const override;
            const Texture::ITexture& getRefractionTexture() const override;

            void setAmbientTexture(Texture::ITexture *ambientTexture);
            void setDiffuseTexture(Texture::ITexture *diffuseTexture);
            void setSpecularTexture(Texture::ITexture *specularTexture);
            void setReflectionTexture(Texture::ITexture *reflectionTexture);
            void setRefractionTexture(Texture::ITexture *refractionTexture);
            void setDiffuse(float diffuse);
            void setSpecular(float specular);
            void setSpecularExponent(float specularExponent);
            void setRefractiveIndex(float refractiveIndex);

        private:
            void validate() const;

        private:
            static const Texture::ITexture& getTexture(const Texture::ITexture* texture);

            std::string name;

            Texture::ITexture * ambientTexture;
            Texture::ITexture * diffuseTexture;
            Texture::ITexture * specularTexture;
            Texture::ITexture * reflectionTexture;
            Texture::ITexture * refractionTexture;
            float diffuse;
            float specular;
            float specularExponent;
            float reflection;
            float refractiveIndex;
        };
    }
}


#endif //MSC_CANDELA_BASEMATERIAL_H

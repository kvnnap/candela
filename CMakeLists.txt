# CMAKE Version and Project Name
cmake_minimum_required(VERSION 3.9)
project(candela)

# Use C++11, enable all warnings and compile in debug mode
add_compile_options(-Wall)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED true)
set(CMAKE_BUILD_TYPE Release)
#set(CMAKE_VERBOSE_MAKEFILE ON)

# Add any definitions here
#set(ZLIB_ROOT "lib/zlib")
#set(PNG_ROOT "lib/libpng")

# Find the system thread library and place in Threads::Threads
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
find_package(PNG REQUIRED)

# Find include and library paths here
#find_path(PNG_INCLUDE_DIR png.h PATHS ${PNG_ROOT}/include NO_DEFAULT_PATH)
#find_library(PNG_LIBRARY png PATHS ${PNG_ROOT}/lib NO_DEFAULT_PATH)

# Global include directories
#include_directories(.)

# Compiler preprocessor definitions
#add_definitons(${PNG_DEFINITIONS})

# candela target
# Find all source files in the below directory automatically
file(GLOB_RECURSE SRC
    Camera/*.cpp
    Concurrency/*.cpp
    Configuration/*.cpp
    Device/*.cpp
    Environment/*.cpp
    Factory/*.cpp
    Integrator/*.cpp
    Light/*.cpp
    Material/*.cpp
    Mathematics/*.cpp
    Mesh/*.cpp
    Renderer/*.cpp
    Sampler/*.cpp
    Scene/*.cpp
    Shape/*.cpp
    Spectrum/*.cpp
    Texture/*.cpp
)
add_executable(candela main.cpp ${SRC})
target_include_directories(candela PUBLIC . ${PNG_INCLUDE_DIRS})
target_link_libraries (candela Threads::Threads ${PNG_LIBRARIES})

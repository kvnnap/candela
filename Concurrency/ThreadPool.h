//
// Created by kvnna on 02/01/2018.
//

#ifndef CANDELA_THREADPOOL_H
#define CANDELA_THREADPOOL_H

#include <queue>
#include <vector>
#include <functional>
#include <thread>
#include <condition_variable>
#include <mutex>

namespace Candela
{
    namespace Concurrency
    {
        class ThreadPool
        {
        public:
            ThreadPool(size_t numWorkers = 0);
            virtual ~ThreadPool();

            size_t getNumberOfWorkers() const;

            void addTask(std::function<void()>&& task);
            void addTasks(std::vector<std::function<void()>>&& tasks);

            // Exceptions
            void throwExceptionSummary(const std::string& exceptionPrefix);

            /**
             * Wait till all tasks are ready without
             * terminating the threads. This allows for
             * this ThreadPool to be re-used
             */
            void wait();
        private:

            void run();

            std::vector<std::thread> workers;
            std::queue<std::function<void()>> tasks;
            std::vector<std::string> exceptions;
            std::condition_variable taskAvailable, tasksFinished;
            std::mutex taskMutex;
            size_t workingWorkers;
            bool running;
        };
    }
}

#endif //CANDELA_THREADPOOL_H

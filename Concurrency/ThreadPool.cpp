//
// Created by kvnna on 02/01/2018.
//

#include "ThreadPool.h"
#include "Mathematics/Funtions.h"

#include <string>

using namespace std;
using namespace Candela::Concurrency;

ThreadPool::ThreadPool(size_t numWorkers)
    : running (true)
{
    if (numWorkers == 0) {
        numWorkers = thread::hardware_concurrency();
    }

    // Stay within range of [1..maxHardwareThreads]
    numWorkers = Mathematics::Functions::clamp<size_t>(numWorkers, 1, thread::hardware_concurrency());
    workingWorkers = numWorkers;

    // Initialise workers
    workers.reserve(numWorkers);
    for (size_t i = 0; i < numWorkers; ++i) {
        workers.emplace_back(&ThreadPool::run, this);
    }

    // At this point, no tasks could have been added
}

ThreadPool::~ThreadPool() {
    {
        // Need to take mutex, so that running is not changed while
        // another thread is in the ThreadPool::run critical section
        unique_lock<mutex> lock(taskMutex);
        running = false;
    }

    // Notify that the thread pool is shutting down
    taskAvailable.notify_all();
    tasksFinished.notify_all();

    // Wait for all threads to end
    for (thread& worker : workers) {
        worker.join();
    }
}

// No one will change the workers size, safe not to use lock
size_t ThreadPool::getNumberOfWorkers() const {
    return workers.size();
}

void ThreadPool::addTask(std::function<void()> &&task) {
    {
        unique_lock<mutex> lock(taskMutex);
        tasks.push(move(task));
    }
    taskAvailable.notify_one();
}

void ThreadPool::addTasks(std::vector<std::function<void()>> &&p_tasks) {
    {
        unique_lock<mutex> lock(taskMutex);
        for (auto& task : p_tasks) {
            tasks.push(move(task));
        }
    }
    //taskAvailable.notify_all();
    for (size_t i = 0; (i < p_tasks.size()) && (i < workers.size()); ++i) {
        taskAvailable.notify_one();
    }
}

void ThreadPool::run() {
    function<void()> task;
    while (true) {
        {
            unique_lock<mutex> lock(taskMutex);

            // This thread is currently idle
            --workingWorkers;

            // If no tasks are available and threadpool is idle, signal
            if (running && tasks.empty() && workingWorkers == 0) {
                tasksFinished.notify_all();
            }

            // ..and wait
            while (running && tasks.empty()) {
                taskAvailable.wait(lock);
            }

            // Finish thread as thread pool is shutting down
            if (!running) {
                return;
            }

            // Get the task
            task = move(tasks.front());
            tasks.pop();

            // This thread is now working
            ++workingWorkers;
        }

        // Run task
        try {
            task();
        } catch(const exception& e) {
            unique_lock<mutex> lock(taskMutex);
            exceptions.emplace_back(e.what());
        } catch(...) {
            unique_lock<mutex> lock(taskMutex);
            exceptions.emplace_back("Exception is not an exception object");
        }
    }
}

void ThreadPool::wait() {
    // Wait
    unique_lock<mutex> lock(taskMutex);
    while (running && (workingWorkers > 0 || !tasks.empty())) {
        tasksFinished.wait(lock);
    }
}

void ThreadPool::throwExceptionSummary(const std::string &exceptionPrefix) {
    unique_lock<mutex> lock(taskMutex);
    if (exceptions.empty())
        return;

    string strExceptions = exceptionPrefix + ": " + to_string(exceptions.size()) + " Exception(s)";
    for(auto& exception : exceptions) {
        strExceptions += "\n-> " + exception;
    }

    exceptions.clear();
    throw runtime_error(strExceptions);
}

/* 
 * File:   main.cpp
 * Author: Kevin
 *
 * Created on 13 December 2013, 20:23
 */

#include <iostream>
#include "Mathematics/Float.h"
#include "Environment/Environment.h"

using namespace std;
using namespace Candela::Mathematics;
using namespace Candela::Environment;

int main(int argc, char** argv)
{
    try {
        // Assumptions Check
        if(!Float::IEEE754)
        {
            cout << "This device cannot properly run this software" << endl;
            return -1;
        }

        // Parse arguments
        string parserName = "JsonConfigurationParser";
        string fileName = "config.json";
        if (argc < 3) {
            cout << "Using default arguments: " << parserName << " " << fileName << endl;
        } else {
            parserName = argv[1];
            fileName = argv[2];
        }

        // Setup the Candela environment
        Environment environment;

        // Initialise the environment using
        // configuration loaded with a relevant parser
        environment.init(parserName, fileName);

        // Start the renders defined in the environment
        environment.execute();

        return 0;
    } catch(const exception& ex) {
        cout << "Exception: " << ex.what() << endl;
        return -1;
    }
}

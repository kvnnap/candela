//
// Created by kvnna on 27/06/2016.
//

#include <sstream>
#include "ISceneLoader.h"

using namespace std;
using namespace Candela::Scene;


ISceneLoader::~ISceneLoader() {}

string ISceneLoader::toString() const {
    ostringstream sstr;
    sstr << "ISceneLoader - Address = " << this;
    return sstr.str();
}

ostream& Candela::Scene::operator <<(ostream& strm, const ISceneLoader& sceneLoader)
{
    return strm << sceneLoader.toString();
}
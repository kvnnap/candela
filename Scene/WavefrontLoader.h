//
// Created by kvnna on 27/06/2016.
//

#ifndef MSC_CANDELA_WAVEFRONTLOADER_H
#define MSC_CANDELA_WAVEFRONTLOADER_H

#include <string>
#include <unordered_map>
#include <memory>

#include "ISceneLoader.h"
#include "Mathematics/Vector2.h"
#include "Mathematics/Vector3.h"
#include "Environment/Environment.h"
#include "Texture/ITexture.h"
#include "Spectrum/RgbSpectrum.h"

namespace Candela {
    namespace Scene {
        class WavefrontLoader
            : public ISceneLoader
        {
        public:

            WavefrontLoader(Environment::Environment& environment);

            // overrides
            void load(IScene& scene) override;

            void setFileName(const std::string& p_fileName);
            const std::string& getFileName() const;

            //
            void loadMtl(IScene& scene, const std::string& p_filename);
        private:

            enum VertexType : uint8_t {
                NONE = 0,
                SPATIAL = (1 << 0),
                TEXTURE = (1 << 1),
                NORMAL = (1 << 2)
            };

            Texture::ITexture* registerMatteTexture(const Spectrum::RgbSpectrum& rgbSpectrum) const;
            Texture::ITexture* registerTexture(std::unique_ptr<Texture::ITexture> texture) const;

            void addTriangle(IScene& scene,
                             VertexType vt,
                             const Mathematics::Vector& v1,
                             const Mathematics::Vector& v2,
                             const Mathematics::Vector& v3,
                             const Mathematics::Vector2<float>& t1,
                             const Mathematics::Vector2<float>& t2,
                             const Mathematics::Vector2<float>& t3,
                             const Mathematics::Vector& n1,
                             const Mathematics::Vector& n2,
                             const Mathematics::Vector& n3
            );

            std::string fileName;

            // Used for data collection
            // v = vertices
            // vn = normals
            // vt = textures
            std::vector<Mathematics::Vector> v , vn;
            std::vector<Mathematics::Vector2<float>> vt;
            // Material map
            std::unordered_map<std::string, size_t> materialMap;

            // Current Material - set by usemtl
            size_t currentMaterialId;
            bool useMaterial;

            // Other
            //Texture::MemoryTexture
            Environment::Environment& environment;
            size_t whiteMaterialId;
        };
    }
}


#endif //MSC_CANDELA_WAVEFRONTLOADER_H

/* 
 * File:   Scene.cpp
 * Author: Kevin
 * 
 * Created on 08 September 2014, 17:22
 */

#include <sstream>
#include "Scene/Scene.h"

using namespace std;
using namespace Candela::Camera;
using namespace Candela::Scene;
using namespace Candela::Light;
using namespace Candela::Material;
using namespace Candela::Shape;
using namespace Candela::Mesh;
using namespace Candela::Mathematics;

Scene::Scene() {
}

void Scene::setCamera(ICamera* p_camera) {
    camera = p_camera;
}

size_t Scene::addLight(ILight* light) {
    lights.push_back(light);
    return lights.size() - 1;
}

size_t Scene::addPrimitive(unique_ptr<Primitive> primitive) {
    primitives.push_back(move(primitive));
    return primitives.size() - 1;
}

void Scene::setMesh(IMesh* p_mesh) {
    mesh = p_mesh;
}

const ICamera *Scene::getCamera() const {
    return camera;
}

const ILight *Scene::getLight(size_t id) const {
    return lights[id];
}

const Primitive *Scene::getPrimitive(size_t id) const {
    return primitives[id].get();
}

const vector<unique_ptr<Primitive>> &Scene::getPrimitives() const {
    return primitives;
}

const vector<ILight*> &Scene::getLights() const {
    return lights;
}

const IMesh *Scene::getMesh() const {
    return mesh;
}

void Scene::commitToMesh() {
    // Commit Primitives to mesh
    if (mesh) {
        std::vector<const IShape *> nakedPointerPrimitives;
        nakedPointerPrimitives.reserve(primitives.size());
        for (size_t i = 0; i < primitives.size(); ++i) {
            IShape * shape = primitives[i]->getShape();
            nakedPointerPrimitives.push_back(shape);
            shape->setGroupId(i);
        }
        mesh->clear();
        mesh->add(nakedPointerPrimitives);
    } else {
        throw runtime_error("Scene: No mesh to commit to");
    }
}

std::string Scene::getName() const {
    return name;
}

void Scene::setName(const std::string &name) {
    this->name = name;
}

string Scene::toString() const {
    ostringstream sstr;
    sstr << IScene::toString() << " - Scene";
    return sstr.str();
}

bool Scene::intersect(const BoundedRay &ray, Candela::Scene::Intersection &intersection) const {
    bool intersected = mesh->intersect(ray, intersection.shapeIntersection);
    if (intersected) {
        intersection.primitive = primitives[intersection.shapeIntersection.Intersectant->getGroupId()].get();
    }
    return intersected;
}

bool Scene::intersect(const BoundedRay &ray, Candela::Scene::Intersection &intersection,
                      const ILight* excludeLight) const {
    return intersect(ray, intersection) && (intersection.primitive->getLight() != excludeLight);
}



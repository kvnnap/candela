//
// Created by kvnna on 27/06/2016.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include "Material/WavefrontMaterial.h"
#include "Mathematics/Matrix.h"
#include "Mathematics/Constants.h"
#include "Mathematics/Float.h"
#include "Shape/Triangle.h"
#include "Material/BaseMaterial.h"
#include "Texture/MatteTexture.h"
#include "Texture/MemoryTexture.h"
#include "WavefrontLoader.h"
#include "Mathematics/Funtions.h"

using namespace std;
using namespace Candela::Scene;
using namespace Candela::Shape;
using namespace Candela::Spectrum;
using namespace Candela::Material;
using namespace Candela::Mathematics;
using namespace Candela::Texture;
using namespace Candela::Environment;

WavefrontLoader::WavefrontLoader(Candela::Environment::Environment& environment)
    : currentMaterialId (), useMaterial (), environment(environment)
{

    // Load white material
    unique_ptr<BaseMaterial> whiteMaterial = make_unique<BaseMaterial>();
    whiteMaterial->setDiffuseTexture(registerMatteTexture(RgbSpectrum(1.f)));
    whiteMaterial->setDiffuse(1.f);
    whiteMaterialId = environment.getMaterialManager().getInstanceManager().registerInstance(move(whiteMaterial));
}

void WavefrontLoader::setFileName(const string &p_fileName) {
    fileName = p_fileName;
}

const string& WavefrontLoader::getFileName() const {
    return fileName;
}

void WavefrontLoader::load(IScene& scene) {
    // Open file
    ifstream objFile (fileName);

    // File exists?
    if (!objFile.good()) {
        throw runtime_error("Invalid Wavefront Obj File Path: " + fileName);
    }

    // Set Buffer size to 8KB
    char buff[8192];
    objFile.rdbuf()->pubsetbuf(buff, sizeof(buff));

    // Start parsing
    string line, type, tempStr;
    Vector tempVec;
    Vector2<float> tempVec2;
    while(getline(objFile, line)) {
        // Clean line
        line.erase(line.find_last_not_of("\r ") + 1);
        // get stream
        istringstream objLineStream (line);

        // Get the type of line
        if (!(objLineStream >> type)) {
            continue;
        }

        // Decide what to do
        if (type == "mtllib") {
            objLineStream >> tempStr; // Mtl path
            // Load Material function call
            loadMtl(scene, tempStr);
        } else if (type == "usemtl") {
            objLineStream >> tempStr; // Mtl Name
            currentMaterialId = materialMap.at(tempStr);
            useMaterial = true;
        } else if (type == "o") {

        } else if (type == "v") {
            objLineStream >> tempVec.x >> tempVec.y >> tempVec.z;
            v.push_back(tempVec);

        } else if (type == "vn") {
            objLineStream >> tempVec.x >> tempVec.y >> tempVec.z;
            vn.push_back(tempVec);

        } else if (type == "vt") {
            objLineStream >> tempVec2.x;
            if (!objLineStream.eof()) {
                objLineStream >> tempVec2.y;
            }

            vt.push_back(tempVec2);
        } else if (type == "f") {

            // Determine face format by reading first chunk
            VertexType vertType = SPATIAL;
            objLineStream >> tempStr;
            size_t slashCount = count(tempStr.begin(), tempStr.end(), '/');
            if (slashCount > 0) {
                vertType = static_cast<VertexType>(vertType | (slashCount == 1 ? TEXTURE : NORMAL));
                if (slashCount == 2 && tempStr.find("//") == string::npos) {
                    // Check whether we have to add TEXTURE
                    vertType = static_cast<VertexType>(vertType | TEXTURE);
                }
            }

            // Reload with previous state
            objLineStream.clear();
            objLineStream.str(line.substr(2, line.size() - 2));

            // We now know complete format, parse accordingly
            int vIndex[3], vtIndex[3], vnIndex[3];
            for (size_t i = 0; !objLineStream.eof(); ++i) {
                // load indices and rebase
                size_t currI = i >= 2 ? 2 : i;
                if (vertType & SPATIAL) {
                    objLineStream >> vIndex[currI];
                    vIndex[currI] = vIndex[currI] < 0 ? v.size() + vIndex[currI] : vIndex[currI] - 1;
                }
                if (vertType & TEXTURE) {
                    objLineStream.ignore(1, '/');
                    objLineStream >> vtIndex[currI];
                    vtIndex[currI] = vtIndex[currI] < 0 ? vt.size() + vtIndex[currI] : vtIndex[currI] - 1;
                }
                if (vertType & NORMAL) {
                    if (!(vertType & TEXTURE)) {
                        objLineStream.ignore(1, '/');
                    }
                    objLineStream.ignore(1, '/');
                    objLineStream >> vnIndex[currI];
                    vnIndex[currI] = vnIndex[currI] < 0 ? vn.size() + vnIndex[currI] : vnIndex[currI] - 1;
                }

                if (i >= 2) {
                    // Create actual shapes - assuming Triangle Fan
                    addTriangle(scene, vertType,
                                v[vIndex[0]], v[vIndex[1]], v[vIndex[2]],
                                vertType & TEXTURE ? vt[vtIndex[0]] : Vector2<float>(),
								vertType & TEXTURE ? vt[vtIndex[1]] : Vector2<float>(),
								vertType & TEXTURE ? vt[vtIndex[2]] : Vector2<float>(),
								vertType & NORMAL ? vn[vnIndex[0]] : Vector(),
								vertType & NORMAL ? vn[vnIndex[1]] : Vector(),
								vertType & NORMAL ? vn[vnIndex[2]] : Vector()
                    );
                    // move
                    vIndex[1] = vIndex[2];
                    vtIndex[1] = vtIndex[2];
                    vnIndex[1] = vnIndex[2];
                }
            }
        }
    }
}

void WavefrontLoader::loadMtl(IScene& scene, const string &p_filename) {

    // Get instance managers
    InstanceManager<IMaterial>& materialInstanceManager = environment.getMaterialManager().getInstanceManager();

    // Open file
    ifstream mtlFile (p_filename);

    // File exists?
    if (!mtlFile.good()) {
        throw runtime_error("Invalid Wavefront Obj File Path: " + fileName);
    }

    // Parse Material
    string line, type, temp;
    RgbSpectrum rgbTemp;
    Vector& rgbRef = rgbTemp.getAbcReference();
    unique_ptr<Material::BaseMaterial> material;
    while(getline(mtlFile, line)) {
        // Clean line
        line.erase(line.find_last_not_of("\r ") + 1);
        // get stream
        istringstream mtlLineStream (line);

        // Get the type of line
        if (!(mtlLineStream >> type)) {
            continue;
        }

        if(type == "newmtl")
        {
            // extract name
            mtlLineStream >> temp;
            // Add previous material to scene and to map
            if (material) {
                string materialName = material->getName();
                materialMap[materialName] = materialInstanceManager.registerInstance(move(material));
            }
            material.reset(new BaseMaterial());
            material->setName(temp);
        }else if(type == "illum")
        {
            uint8_t illuminationModel;
            mtlLineStream >> illuminationModel;
            //material->setIlluminationModel(illuminationModel);
        }else if(type == "Ns")
        {
            float specExp;
            mtlLineStream >> specExp;
            material->setSpecularExponent(specExp);
        }else if(type == "Ni")
        {
            float refractiveIndex;
            mtlLineStream >> refractiveIndex;
            material->setRefractiveIndex(refractiveIndex);
        }else if(type == "Tf")
        {
            mtlLineStream >> rgbRef.x >> rgbRef.y >> rgbRef.z;
            material->setRefractionTexture(registerMatteTexture(rgbRef));
        }
        else if(type == "map_Ka")
        {
            mtlLineStream >> temp;
            unique_ptr<MemoryTexture> texture(new MemoryTexture());
            texture->loadFromFile(temp);
            material->setAmbientTexture(registerTexture(move(texture)));
        }else if(type == "Ka")
        {
            mtlLineStream >> rgbRef.x >> rgbRef.y >> rgbRef.z;
            material->setAmbientTexture(registerMatteTexture(rgbRef));
        }else if(type == "map_Kd")
        {
            mtlLineStream >> temp;
            unique_ptr<MemoryTexture> texture(new MemoryTexture());
            texture->loadFromFile(temp);
            material->setDiffuseTexture(registerTexture(move(texture)));
            material->setDiffuse(1.f);
        }else if(type == "Kd")
        {
            mtlLineStream >> rgbRef.x >> rgbRef.y >> rgbRef.z;
            material->setDiffuseTexture(registerMatteTexture(rgbRef));
            material->setDiffuse(1.f);
        }else if(type == "map_Ks")
        {
            mtlLineStream >> temp;
            unique_ptr<MemoryTexture> texture(new MemoryTexture());
            texture->loadFromFile(temp);
            material->setSpecularTexture(registerTexture(move(texture)));
        }else if(type == "Ks")
        {
            mtlLineStream >> rgbRef.x >> rgbRef.y >> rgbRef.z;
            material->setSpecularTexture(registerMatteTexture(rgbRef));
        }
    }
    if (material) {
        string materialName = material->getName();
        materialMap[materialName] = materialInstanceManager.registerInstance(move(material));
    }
}

void WavefrontLoader::addTriangle(IScene &scene, VertexType vt,
                                  const Vector         &v1, const Vector         &v2, const Vector         &v3,
                                  const Vector2<float> &t1, const Vector2<float> &t2, const Vector2<float> &t3,
                                  const Vector         &n1, const Vector         &n2, const Vector         &n3
) {
    size_t triMatId = useMaterial ? currentMaterialId : whiteMaterialId;
    // rotate
    // Convert to Left-Hand side coordinate system
    Triangle * tri = new Triangle(v1, v2, v3);
    if (vt & TEXTURE) {
        tri->setTextureCoordinates(t1, t2, t3);
    }
    if (vt & NORMAL) {
        tri->setNormalCoordinates(n1, n2, n3);
    }

    size_t triId = environment.getShapeManager().getInstanceManager().registerInstance(unique_ptr<IShape>(tri));
    scene.addPrimitive(
            make_unique<Primitive>(&environment.getShapeManager().getInstanceManager().getInstance(triId),
                    &environment.getMaterialManager().getInstanceManager().getInstance(triMatId)));

//    cout << "Material: " << scene.getMaterial(triMatId)->getMaterialName() << endl;
//    cout << "Shape: " << *tri << endl;
}

ITexture *WavefrontLoader::registerMatteTexture(const RgbSpectrum &rgbSpectrum) const {
    InstanceManager<ITexture>& textureInstanceManager = environment.getTextureManager().getInstanceManager();
    size_t ref = textureInstanceManager
            .registerInstance(make_unique<MatteTexture>(make_unique<RgbSpectrum>(rgbSpectrum)));
    return &textureInstanceManager.getInstance(ref);
}

ITexture *WavefrontLoader::registerTexture(unique_ptr<ITexture> texture) const {
    InstanceManager<ITexture>& textureInstanceManager = environment.getTextureManager().getInstanceManager();
    size_t ref = textureInstanceManager.registerInstance(move(texture));
    return &textureInstanceManager.getInstance(ref);
}

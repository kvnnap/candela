//
// Created by kvnna on 16/05/2018.
//

#ifndef CANDELA_WAVEFRONTSCENELOADERFACTORY_H
#define CANDELA_WAVEFRONTSCENELOADERFACTORY_H

#include "Factory/Factory.h"
#include "Scene/ISceneLoader.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Scene
    {
        class WavefrontSceneLoaderFactory
                : public Factory::Factory<ISceneLoader>
        {
        public:
            WavefrontSceneLoaderFactory(Environment::Environment& environment);

            std::unique_ptr<ISceneLoader> create() const override;
            std::unique_ptr<ISceneLoader> create(const Configuration::ConfigurationNode& config) const override;

        private:
            Environment::Environment& environment;
        };
    }
}

#endif //CANDELA_WAVEFRONTSCENELOADERFACTORY_H

//
// Created by kvnna on 24/12/2017.
//

#ifndef CANDELA_CONFIGURATIONSCENELOADERFACTORY_H
#define CANDELA_CONFIGURATIONSCENELOADERFACTORY_H

#include "Factory/Factory.h"
#include "Scene/ISceneLoader.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Scene
    {
        class ConfigurationSceneLoaderFactory
                : public Factory::Factory<ISceneLoader>
        {
        public:
            ConfigurationSceneLoaderFactory(Environment::Environment& environment);

            std::unique_ptr<ISceneLoader> create() const override;
            std::unique_ptr<ISceneLoader> create(const Configuration::ConfigurationNode& config) const override;

        private:
            Environment::Environment& environment;
        };
    }
}

#endif //CANDELA_CONFIGURATIONSCENELOADERFACTORY_H

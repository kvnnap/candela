//
// Created by kvnna on 24/12/2017.
//

#include "ConfigurationSceneLoaderFactory.h"
#include "Scene/ConfigurationSceneLoader.h"
#include "Environment/Environment.h"
#include "Light/AreaLight.h"

using namespace std;
using namespace Candela::Scene;
using namespace Candela::Configuration;
using namespace Candela::Environment;
using namespace Candela::Light;

ConfigurationSceneLoaderFactory::ConfigurationSceneLoaderFactory(Candela::Environment::Environment &environment)
        : environment(environment)
{}

unique_ptr<ISceneLoader> ConfigurationSceneLoaderFactory::create() const {
    return make_unique<ConfigurationSceneLoader>();
}

unique_ptr<ISceneLoader> ConfigurationSceneLoaderFactory::create(const ConfigurationNode &config) const {
    unique_ptr<ConfigurationSceneLoader> sceneLoader = make_unique<ConfigurationSceneLoader>();

    IScene& scene = environment.getSceneManager().getInstanceManager().getInstance(*config["Scene"]);

    for(const auto& primNode : *config["Primitives"]){
        if (primNode.value.keyExists("Light")) {
            AreaLight * areaLight = dynamic_cast<AreaLight*>(&environment.getLightManager().getInstanceManager().getInstance(*primNode.value["Light"]));
            if (areaLight == nullptr) {
                throw runtime_error("Light added to primitive is not an AreaLight");
            }

            sceneLoader->addPrimitive(areaLight);
        } else {
            sceneLoader->addPrimitive(
                    &environment.getShapeManager().getInstanceManager().getInstance(*primNode.value["Shape"]),
                    &environment.getMaterialManager().getInstanceManager().getInstance(*primNode.value["Material"])
            );
        }
    }

    sceneLoader->load(scene);

    return sceneLoader;
}

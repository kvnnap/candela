//
// Created by kvnna on 24/12/2017.
//

#ifndef CANDELA_SCENEFACTORY_H
#define CANDELA_SCENEFACTORY_H

#include "Factory/Factory.h"
#include "Scene/IScene.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Scene
    {
        class SceneFactory
            : public Factory::Factory<IScene>
        {
        public:
            SceneFactory(Environment::Environment& environment);

            std::unique_ptr<IScene> create() const override;
            std::unique_ptr<IScene> create(const Configuration::ConfigurationNode& config) const override;

        private:
            Environment::Environment& environment;
        };
    }
}



#endif //CANDELA_SCENEFACTORY_H

//
// Created by kvnna on 16/05/2018.
//

#include "WavefrontSceneLoaderFactory.h"
#include "Scene/WavefrontLoader.h"
#include "Environment/Environment.h"

using namespace std;
using namespace Candela::Scene;
using namespace Candela::Configuration;
using namespace Candela::Environment;
using namespace Candela::Light;

WavefrontSceneLoaderFactory::WavefrontSceneLoaderFactory(Candela::Environment::Environment &environment)
        : environment(environment)
{}

unique_ptr<ISceneLoader> WavefrontSceneLoaderFactory::create() const {
    return unique_ptr<ISceneLoader>();
}

unique_ptr<ISceneLoader> WavefrontSceneLoaderFactory::create(const ConfigurationNode &config) const {
    unique_ptr<WavefrontLoader> sceneLoader = make_unique<WavefrontLoader>(environment);

    IScene& scene = environment.getSceneManager().getInstanceManager().getInstance(*config["Scene"]);
    sceneLoader->setFileName(**config["FileName"]);
    sceneLoader->load(scene);

    return sceneLoader;
}

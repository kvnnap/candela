//
// Created by kvnna on 24/12/2017.
//

#include "SceneFactory.h"
#include "Scene/Scene.h"
#include "Environment/Environment.h"

using namespace std;
using namespace Candela::Scene;
using namespace Candela::Configuration;
using namespace Candela::Environment;

SceneFactory::SceneFactory(Candela::Environment::Environment &environment)
    : environment(environment)
{}

unique_ptr<IScene> SceneFactory::create() const {
    return make_unique<Scene>();
}

unique_ptr<IScene> SceneFactory::create(const ConfigurationNode &config) const {
    unique_ptr<IScene> scene = make_unique<Scene>();
    scene->setName(**config["Name"]);

    scene->setCamera(&environment.getCameraManager().getInstanceManager().getInstance(*config["Camera"]));
    scene->setMesh(&environment.getMeshManager().getInstanceManager().getInstance(*config["Mesh"]));

    for (const auto& lightNode : *config["Lights"]){
        scene->addLight(&environment.getLightManager().getInstanceManager().getInstance(*lightNode.value));
    }

    return scene;
}

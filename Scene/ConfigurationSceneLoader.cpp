//
// Created by kvnna on 29/06/2016.
//

#include <memory>


#include "Shape/Plane.h"
#include "Shape/Sphere.h"
#include "ConfigurationSceneLoader.h"
#include "Camera/PinholeCamera.h"
#include "Light/AreaLight.h"
#include "Spectrum/RgbSpectrum.h"
#include "Material/BaseMaterial.h"

using namespace std;
using namespace Candela::Scene;
using namespace Candela::Camera;
using namespace Candela::Light;
using namespace Candela::Shape;
using namespace Candela::Spectrum;
using namespace Candela::Mathematics;
using namespace Candela::Material;

void ConfigurationSceneLoader::load(IScene& scene) {
    for (const auto& prim : primitives) {
        if (prim.isEmissive()) {
            scene.addPrimitive(make_unique<Primitive>(prim.getLight(), prim.getMaterial()));
        } else {
            scene.addPrimitive(make_unique<Primitive>(prim.getShape(), prim.getMaterial()));
        }
    }
    primitives.clear();
}

string ConfigurationSceneLoader::toString() const {
    ostringstream sstr;
    sstr << ISceneLoader::toString() << " - ConfigurationSceneLoader";
    return sstr.str();
}

void ConfigurationSceneLoader::addPrimitive(IShape *shape, IMaterial *material) {
    primitives.emplace_back(Primitive(shape, material));
}

void ConfigurationSceneLoader::addPrimitive(AreaLight *light, IMaterial *material) {
    primitives.emplace_back(Primitive(light, material));
}

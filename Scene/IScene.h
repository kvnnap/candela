/* 
 * File:   IScene.h
 * Author: Kevin
 *
 * Created on 08 September 2014, 16:16
 */

#ifndef ISCENE_H
#define	ISCENE_H

#include "Mathematics/BoundedRay.h"
#include "Shape/Intersection.h"

#include "Mesh/IMesh.h"
#include "Light/ILight.h"
#include "Primitive.h"
#include "Shape/IShape.h"
#include "Camera/ICamera.h"

#include <vector>
#include <memory>

namespace Candela
{
    namespace Scene
    {
        class IScene {
        public:
            virtual ~IScene();

            virtual std::string toString() const;

            // The name of this scene
            virtual std::string getName() const = 0;
            virtual void setName(const std::string& name) = 0;

            virtual void setCamera(Camera::ICamera* camera) = 0;
            virtual size_t addLight(Light::ILight* light) = 0;
            virtual size_t addPrimitive(std::unique_ptr<Primitive> primitive) = 0;
            virtual void setMesh(Mesh::IMesh* mesh) = 0;

            virtual const Camera::ICamera * getCamera() const = 0;
            virtual const Light::ILight * getLight(size_t id) const = 0;
            virtual const Primitive * getPrimitive(size_t id) const = 0;
            virtual const Mesh::IMesh * getMesh() const = 0;

            virtual const std::vector<std::unique_ptr<Primitive>>& getPrimitives() const = 0;
            virtual const std::vector<Light::ILight*>& getLights() const = 0;

            virtual void commitToMesh() = 0;
            virtual bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const = 0;
            virtual bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection,  const Light::ILight* excludeLight) const = 0;
        };

        std::ostream& operator<< (std::ostream& strm, const IScene& scene);
    }
}



#endif	/* ISCENE_H */


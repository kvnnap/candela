//
// Created by kvnna on 26/06/2016.
//

#ifndef MSC_CANDELA_INTERSECTION_H
#define MSC_CANDELA_INTERSECTION_H



namespace Candela
{
    namespace Scene
    {
        class Primitive;

        class Intersection {
        public:
            Intersection() : primitive(nullptr) {};
            Shape::Intersection shapeIntersection;
            const Primitive * primitive;
        };
    }
}

#endif //MSC_CANDELA_INTERSECTION_H

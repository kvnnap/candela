//
// Created by kvnna on 27/06/2016.
//

#ifndef MSC_CANDELA_ISCENELOADER_H
#define MSC_CANDELA_ISCENELOADER_H

#include "Scene/IScene.h"

namespace Candela {
    namespace Scene {
        class ISceneLoader {
        public:
            virtual ~ISceneLoader();

            // Load
            virtual void load(IScene& scene) = 0;

            virtual std::string toString() const;
        };

        std::ostream& operator<< (std::ostream& strm, const ISceneLoader& sceneLoader);
    }
}

#endif //MSC_CANDELA_ISCENELOADER_H

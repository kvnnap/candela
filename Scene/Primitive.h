//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_PRIMITIVE_H
#define MSC_CANDELA_PRIMITIVE_H

#include "Shape/IShape.h"
#include "Material/IMaterial.h"
#include "Light/AreaLight.h"
#include "Intersection.h"

namespace Candela
{
    namespace Scene
    {
        class Primitive
        {
        public:

            Primitive(Shape::IShape * p_shape = nullptr, const Material::IMaterial * p_material = nullptr);
            Primitive(Light::AreaLight * p_light = nullptr, const Material::IMaterial * p_material = nullptr);

            const Material::IMaterial* getMaterial() const;
            Shape::IShape* getShape() const;

            void setMaterial(Material::IMaterial * p_material);
            void setShape(Shape::IShape * p_shape);
            void setLight(Light::AreaLight* areaLight);

            bool hasMaterial() const;
            bool hasShape() const;
            Light::AreaLight* getLight() const;

            bool isEmissive() const;

            // Primitive Intersect
            bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const;

        private:
            // And it owns a shape as well
            Shape::IShape * shape;

            Light::AreaLight * areaLight;

            // A primitive owns the material
            const Material::IMaterial * material;
        };
    }
}



#endif //MSC_CANDELA_PRIMITIVE_H

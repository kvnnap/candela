//
// Created by kvnna on 29/06/2016.
//

#ifndef MSC_CANDELA_SIMPLELOADER_H
#define MSC_CANDELA_SIMPLELOADER_H

#include "ISceneLoader.h"
#include "Light/AreaLight.h"
#include "Shape/IShape.h"
#include "Material/IMaterial.h"
#include "Primitive.h"
#include <utility>

namespace Candela
{
    namespace Scene
    {
        class ConfigurationSceneLoader
            : public ISceneLoader
        {
        public:
            void load(IScene& scene) override;

            std::string toString() const override;

            void addPrimitive(Shape::IShape* shape, Material::IMaterial* material);
            void addPrimitive(Light::AreaLight* light, Material::IMaterial* material = nullptr);
        private:

            std::vector<Primitive> primitives;
        };
    }
}



#endif //MSC_CANDELA_SIMPLELOADER_H

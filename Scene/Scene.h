/* 
 * File:   Scene.h
 * Author: Kevin
 *
 * Created on 08 September 2014, 17:22
 */

#ifndef SCENE_H
#define	SCENE_H

#include "IScene.h"
#include "Mesh/IMesh.h"
#include "Primitive.h"
#include "Shape/IShape.h"
#include "Camera/BaseCamera.h"
#include "Intersection.h"

#include <vector>
#include <memory>

namespace Candela
{
    namespace Scene
    {
        class Scene 
         : public IScene{
        public:
            Scene();

            std::string toString() const override;

            // The name of this scene
            std::string getName() const override;
            void setName(const std::string& name) override;

            void setCamera(Camera::ICamera* camera) override;
            size_t addLight(Light::ILight* light) override;
            size_t addPrimitive(std::unique_ptr<Primitive> primitive) override;
            void setMesh(Mesh::IMesh* mesh) override;

            const Camera::ICamera * getCamera() const override;
            const Light::ILight * getLight(size_t id) const override;
            const Primitive * getPrimitive(size_t id) const override;
            const Mesh::IMesh * getMesh() const override;

            const std::vector<std::unique_ptr<Primitive>>& getPrimitives() const override;
            const std::vector<Light::ILight*>& getLights() const override;

            void commitToMesh() override;

            bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const override;
            bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection,  const Light::ILight* excludeLight) const override;
            
        private:

            std::string name;

            // Set the scene camera
            Camera::ICamera* camera;

            //lights don't need to be accelerated with a structure - Check
            std::vector<Light::ILight*> lights;

            // All the primitives contained in the scene
            // Note that a primitive could contain a mesh as a shape
            std::vector<std::unique_ptr<Primitive>> primitives;

            // Need acceleration structure over primitives here!
            Mesh::IMesh* mesh;

        };
    }
}

#endif	/* SCENE_H */


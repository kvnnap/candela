/* 
 * File:   IScene.cpp
 * Author: Kevin
 * 
 * Created on 08 September 2014, 16:16
 */

#include <sstream>

#include "Scene/IScene.h"

using namespace std;
using namespace Candela::Scene;

IScene::~IScene() {
}

string IScene::toString() const {
    ostringstream sstr;
    sstr << "IScene - Address = " << this
         << "\n\tName: " << getName();
    return sstr.str();
}

ostream& Candela::Scene::operator <<(ostream& strm, const IScene& scene)
{
    return strm << scene.toString();
}
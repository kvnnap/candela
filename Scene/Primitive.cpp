//
// Created by kvnna on 25/06/2016.
//

#include "Primitive.h"

using namespace std;
using namespace Candela::Shape;
using namespace Candela::Material;
using namespace Candela::Scene;
using namespace Candela::Mathematics;
using namespace Candela::Light;

Primitive::Primitive(IShape *p_shape, const IMaterial *p_material)
        : shape (p_shape), areaLight(),  material (p_material)
{
}

Primitive::Primitive(AreaLight *p_light, const IMaterial *p_material)
    : shape (p_light->getShape()), areaLight(p_light), material(p_material)
{}

const IMaterial* Primitive::getMaterial() const {
    return material;
}

IShape* Primitive::getShape() const {
    return shape;
}

bool Primitive::hasMaterial() const {
    return material == nullptr;
}

bool Primitive::hasShape() const {
    return shape == nullptr;
}

void Primitive::setMaterial(IMaterial * p_material){
    material = p_material;
}

void Primitive::setShape(IShape * p_shape) {
    shape = p_shape;
}

bool Primitive::intersect(const BoundedRay &ray, Candela::Scene::Intersection &intersection) const {
    if (shape->intersect(ray, intersection.shapeIntersection))
    {
        // Associate primitive
        intersection.primitive = this;
        return true;
    }
    return false;
}

bool Primitive::isEmissive() const {
    return areaLight != nullptr;
}

void Primitive::setLight(AreaLight *areaLight) {
    this->areaLight = areaLight;
    shape = areaLight->getShape();
    material = nullptr;
}

AreaLight *Primitive::getLight() const {
    return areaLight;
}


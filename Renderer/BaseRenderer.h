//
// Created by kvnna on 23/06/2016.
//

#ifndef MSC_CANDELA_BASERENDERER_H
#define MSC_CANDELA_BASERENDERER_H

#include "Renderer/IRenderer.h"

namespace Candela
{
    namespace Renderer
    {

        /**
         * A renderer renders a scene. Therefore:
         * 1) Requires a scene object.
         * 2) Requires an integrator, that gives back radiance for each pixel
         * 3) Requires a buffer/device to output this data on
         */
        class BaseRenderer
                : public IRenderer
        {
        public:
            std::string toString() const override;

            virtual void setScene(Scene::IScene * p_scene);
            void setDevice(Device::IDevice * p_device);
            void setIntegrator(Integrator::IIntegrator * p_integrator);

            Scene::IScene * getScene() override;
            const Scene::IScene * getScene() const override;
            const Device::IDevice * getDevice() const override;
            const Integrator::IIntegrator * getIntegrator() const override;

        protected:
            // Concrete scene
            Scene::IScene * scene;

            // Device to output the result to
            Device::IDevice * device;

            // Integrator
            Integrator::IIntegrator * integrator;

            // Buffer
            std::unique_ptr<RadianceBuffer> radianceBuffer;
        };

    }
}

#endif //MSC_CANDELA_BASERENDERER_H

//
// Created by kvnna on 23/06/2016.
//

#include "BaseRenderer.h"
#include "Mathematics/Rectangle.h"
#include <sstream>

using namespace std;
using namespace Candela::Renderer;
using namespace Candela::Scene;
using namespace Candela::Device;
using namespace Candela::Integrator;

void BaseRenderer::setScene(IScene *p_scene) {
    scene = p_scene;
    // Set appropriate Radiance Buffer dimensions
    const Mathematics::Rectangle_I& rect = p_scene->getCamera()->getSensorPixelDimensions();
    radianceBuffer = make_unique<RadianceBuffer>(rect.getWidth(), rect.getHeight());
}

void BaseRenderer::setDevice(IDevice *p_device) {
    device = p_device;
}

IScene *BaseRenderer::getScene() {
    return scene;
}

const IScene *BaseRenderer::getScene() const{
    return scene;
}

const IDevice *BaseRenderer::getDevice() const {
    return device;
}

void BaseRenderer::setIntegrator(IIntegrator *p_integrator) {
    integrator = p_integrator;
}

const IIntegrator *BaseRenderer::getIntegrator() const {
    return integrator;
}

std::string BaseRenderer::toString() const {
    ostringstream sstr;
    sstr    << "Renderer - Address: " << this
            << "\r\nScene: " << scene
            << "\r\nIntegrator: " << integrator
            << "\r\nDevice: " << device
            ;
    return sstr.str();
}






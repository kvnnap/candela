//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_Buffer2D_H
#define MSC_CANDELA_Buffer2D_H

#include <stddef.h>
#include <vector>

namespace Candela {
    namespace Renderer {

        template <class T>
        class Buffer2D {
        public:
            virtual ~Buffer2D();
            Buffer2D();
            Buffer2D(size_t width, size_t height);
            //Buffer2D(Buffer2D && other);

            size_t getWidth() const;
            size_t getHeight() const;

            void clearAndReset(size_t width, size_t height);

            T& operator() (size_t x, size_t y);
            const T& operator() (size_t x, size_t y) const;


        protected:

            Buffer2D& operator=(Buffer2D&& other);
            size_t getIndex(size_t x, size_t y) const;

            size_t width, height;
            std::vector<T> buffer;
        };
    }
}


#endif //MSC_CANDELA_Buffer2D_H

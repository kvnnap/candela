//
// Created by kvnna on 31/12/2017.
//

#ifndef CANDELA_VARIANCEBASEDRENDERER_H
#define CANDELA_VARIANCEBASEDRENDERER_H

#include <atomic>
#include "ConcurrentTiledRenderer.h"

namespace Candela
{
    namespace Renderer
    {
        class VarianceBasedRenderer
                : public ConcurrentTiledRenderer
        {
        public:

            VarianceBasedRenderer(size_t minIterationsPerPixel,
                                  size_t maxIterationsPerPixel,
                                  float stdDeviationConfidence,
                                  float confidenceInterval
            );

            std::string toString() const override;

            void concurrentRender(Integrator::IIntegrator& threadLocalIntegrator) override;

            void onFrameRendered() override;

        private:
            size_t minIterationsPerPixel;
            size_t maxIterationsPerPixel;
            float stdDeviationConfidence;
            float confidenceInterval;

            std::atomic<size_t> numberOfNonConfidentPixels;
        };
    }
}

#endif //CANDELA_VARIANCEBASEDRENDERER_H

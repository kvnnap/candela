//
// Created by kvnna on 03/01/2018.
//

#ifndef CANDELA_CONCURRENTTILEDRENDERERFACTORY_H
#define CANDELA_CONCURRENTTILEDRENDERERFACTORY_H

#include "BaseRendererFactory.h"
#include "Renderer/ConcurrentTiledRenderer.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Renderer
    {
        class ConcurrentTiledRendererFactory
                : public BaseRendererFactory
        {
        public:

            ConcurrentTiledRendererFactory(Environment::Environment& environment);

            void setupRenderer(ConcurrentTiledRenderer* renderer, const Configuration::ConfigurationNode& config) const;
        };
    }
}


#endif //CANDELA_CONCURRENTTILEDRENDERERFACTORY_H

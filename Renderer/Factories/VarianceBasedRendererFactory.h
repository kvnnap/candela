//
// Created by kvnna on 31/12/2017.
//

#ifndef CANDELA_VARIANCEBASEDRENDERERFACTORY_H
#define CANDELA_VARIANCEBASEDRENDERERFACTORY_H

#include "ConcurrentTiledRendererFactory.h"
#include "Renderer/BaseRenderer.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Renderer
    {
        class VarianceBasedRendererFactory
                : public ConcurrentTiledRendererFactory
        {
        public:

            VarianceBasedRendererFactory(Environment::Environment& environment);
            std::unique_ptr<IRenderer> create() const override;
            std::unique_ptr<IRenderer> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}

#endif //CANDELA_VARIANCEBASEDRENDERERFACTORY_H

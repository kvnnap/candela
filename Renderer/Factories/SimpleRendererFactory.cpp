//
// Created by kvnna on 24/12/2017.
//

#include "SimpleRendererFactory.h"
#include "Environment/Environment.h"
#include "Renderer/SimpleRenderer.h"

using namespace std;
using namespace Candela::Configuration;
using namespace Candela::Renderer;
using namespace Candela::Environment;

SimpleRendererFactory::SimpleRendererFactory(Candela::Environment::Environment &environment)
    : ConcurrentTiledRendererFactory (environment)
{}

unique_ptr<IRenderer> SimpleRendererFactory::create() const {
    return make_unique<SimpleRenderer>();
}

unique_ptr<IRenderer> SimpleRendererFactory::create(const ConfigurationNode &config) const {
    unique_ptr<SimpleRenderer> renderer = make_unique<SimpleRenderer>();
    setupRenderer(renderer.get(), config);
    return renderer;
}

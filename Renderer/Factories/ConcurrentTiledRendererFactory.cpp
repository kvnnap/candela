//
// Created by kvnna on 03/01/2018.
//

#include "ConcurrentTiledRendererFactory.h"
#include "Mathematics/Factories/RectangleFactory.h"

using namespace std;
using namespace Candela::Environment;
using namespace Candela::Renderer;
using namespace Candela::Mathematics;
using namespace Candela::Configuration;

ConcurrentTiledRendererFactory::ConcurrentTiledRendererFactory(Candela::Environment::Environment &environment)
        : BaseRendererFactory(environment) {}

void ConcurrentTiledRendererFactory::setupRenderer(ConcurrentTiledRenderer *renderer,
                                                   const ConfigurationNode &config) const {
    BaseRendererFactory::setupRenderer(renderer, config);

    if (config.keyExists("TileDimensions")) {
        const Rectangle_I tileDimensions = *Rectangle_I_Factory().create(*config["TileDimensions"]);
        renderer->setTileDimensions(tileDimensions);
    }

    if (config.keyExists("Workers")) {
        renderer->setNumberOfWorkers(static_cast<size_t>(config["Workers"]->read<uint64_t>()));
    }
}

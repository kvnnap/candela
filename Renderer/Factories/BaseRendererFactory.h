//
// Created by kvnna on 28/12/2017.
//

#ifndef CANDELA_BASERENDERERFACTORY_H
#define CANDELA_BASERENDERERFACTORY_H


#include "Factory/Factory.h"
#include "Renderer/IRenderer.h"
#include "Renderer/BaseRenderer.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Renderer
    {
        class BaseRendererFactory
            : public Factory::Factory<IRenderer>
        {
        protected:
            BaseRendererFactory(Environment::Environment& environment);

            void setupRenderer(BaseRenderer* renderer, const Configuration::ConfigurationNode& config) const;

            Environment::Environment& environment;
        };
    }
}

#endif //CANDELA_BASERENDERERFACTORY_H

//
// Created by kvnna on 28/12/2017.
//

#ifndef CANDELA_PROGRESSIVERENDERERFACTORY_H
#define CANDELA_PROGRESSIVERENDERERFACTORY_H

#include "ConcurrentTiledRendererFactory.h"
#include "Renderer/BaseRenderer.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Renderer
    {
        class ProgressiveRendererFactory
                : public ConcurrentTiledRendererFactory
        {
        public:

            ProgressiveRendererFactory(Environment::Environment& environment);
            std::unique_ptr<IRenderer> create() const override;
            std::unique_ptr<IRenderer> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}


#endif //CANDELA_PROGRESSIVERENDERERFACTORY_H

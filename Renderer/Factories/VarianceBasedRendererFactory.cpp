//
// Created by kvnna on 31/12/2017.
//

#include "VarianceBasedRendererFactory.h"
#include "Environment/Environment.h"
#include "Renderer/VarianceBasedRenderer.h"

using namespace std;
using namespace Candela::Configuration;
using namespace Candela::Renderer;
using namespace Candela::Environment;

VarianceBasedRendererFactory::VarianceBasedRendererFactory(Candela::Environment::Environment &environment)
        : ConcurrentTiledRendererFactory (environment)
{}

std::unique_ptr<IRenderer> VarianceBasedRendererFactory::create() const {
    return unique_ptr<IRenderer>();
}

std::unique_ptr<IRenderer> VarianceBasedRendererFactory::create(const ConfigurationNode &config) const {
    unique_ptr<VarianceBasedRenderer> renderer = make_unique<VarianceBasedRenderer>(
            static_cast<size_t>(config["MinimumIterations"]->read<uint64_t>()),
            static_cast<size_t>(config["Iterations"]->read<uint64_t>()),
            static_cast<float>(config["StandardDeviationConfidence"]->read<double>()),
            static_cast<float>(config["ConfidenceInterval"]->read<double>())
    );
    setupRenderer(renderer.get(), config);
    return renderer;
}
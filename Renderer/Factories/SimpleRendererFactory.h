//
// Created by kvnna on 24/12/2017.
//

#ifndef CANDELA_SIMPLERENDERERFACTORY_H
#define CANDELA_SIMPLERENDERERFACTORY_H

#include "ConcurrentTiledRendererFactory.h"

namespace Candela
{
    namespace Environment {
        class Environment;
    }

    namespace Renderer
    {
        class SimpleRendererFactory
            : public ConcurrentTiledRendererFactory
        {
        public:

            SimpleRendererFactory(Environment::Environment& environment);
            std::unique_ptr<IRenderer> create() const override;
            std::unique_ptr<IRenderer> create(const Configuration::ConfigurationNode& config) const override;
        };
    }
}




#endif //CANDELA_SIMPLERENDERERFACTORY_H

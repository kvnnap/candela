//
// Created by kvnna on 28/12/2017.
//

#include "BaseRendererFactory.h"
#include "Environment/Environment.h"

using namespace std;
using namespace Candela::Environment;
using namespace Candela::Configuration;
using namespace Candela::Renderer;

BaseRendererFactory::BaseRendererFactory(Candela::Environment::Environment &environment)
    : environment (environment)
{}

void BaseRendererFactory::setupRenderer(BaseRenderer* renderer, const ConfigurationNode &config) const {
    renderer->setScene(&environment.getSceneManager().getInstanceManager().getInstance(*config["Scene"]));
    renderer->setIntegrator(&environment.getIntegratorManager().getInstanceManager().getInstance(*config["Integrator"]));
    renderer->setDevice(&environment.getDeviceManager().getInstanceManager().getInstance(*config["Device"]));
}

//
// Created by kvnna on 28/12/2017.
//

#include "ProgressiveRendererFactory.h"
#include "Environment/Environment.h"
#include "Renderer/ProgressiveRenderer.h"

using namespace std;
using namespace Candela::Configuration;
using namespace Candela::Renderer;
using namespace Candela::Environment;

ProgressiveRendererFactory::ProgressiveRendererFactory(Candela::Environment::Environment &environment)
    : ConcurrentTiledRendererFactory (environment)
{}

std::unique_ptr<IRenderer> ProgressiveRendererFactory::create() const {
    return unique_ptr<IRenderer>();
}

std::unique_ptr<IRenderer> ProgressiveRendererFactory::create(const ConfigurationNode &config) const {
    unique_ptr<ProgressiveRenderer> renderer = make_unique<ProgressiveRenderer>();
    setupRenderer(renderer.get(), config);
    renderer->setIterations(static_cast<size_t>(config["Iterations"]->read<uint64_t>()));
    return renderer;
}

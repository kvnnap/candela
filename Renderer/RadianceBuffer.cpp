//
// Created by kvnna on 25/06/2016.
//

#include "RadianceBuffer.h"

using namespace std;
using namespace Candela::Renderer;

RadianceContent::RadianceContent() {

}

RadianceContent& RadianceContent::operator=(RadianceContent &&other) {
//    direct = move(other.direct);
//    indirect = move(other.indirect);
    radiance = move(other.radiance);

    return *this;
}

RadianceContent::RadianceContent(RadianceContent &&other)
    :
//      direct ( move(other.direct) ),
//      indirect (move(other.indirect)),
      radiance ( move(other.radiance) )
{

}


RadianceBuffer::RadianceBuffer(size_t width, size_t height)
        : Buffer2D(width, height)
{

}

//RadianceBuffer::RadianceBuffer(RadianceBuffer &&other)
//    : Buffer2D ( move(other) )
//{
//}

RadianceBuffer& RadianceBuffer::operator=(RadianceBuffer &&other) {
    Buffer2D::operator=(move(other));
    return *this;
}











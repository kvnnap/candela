//
// Created by kvnna on 28/06/2016.
//

#ifndef MSC_CANDELA_PROGRESSIVERENDERER_H
#define MSC_CANDELA_PROGRESSIVERENDERER_H


#include "ConcurrentTiledRenderer.h"

namespace Candela
{
    namespace Renderer
    {
        class ProgressiveRenderer
                : public ConcurrentTiledRenderer
        {
        public:

            ProgressiveRenderer();

            std::string toString() const override;

            void setIterations(size_t p_iterations);
            size_t getIterations() const;

            void concurrentRender(Integrator::IIntegrator& threadLocalIntegrator) override;
            void onFrameRendered() override;
            bool nextFrameRequired() const override;
        private:
            size_t iterations;
            size_t currentIteration;
        };
    }
}

#endif //MSC_CANDELA_PROGRESSIVERENDERER_H

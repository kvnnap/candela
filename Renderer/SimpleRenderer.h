//
// Created by kvnna on 23/06/2016.
//

#ifndef MSC_CANDELA_SIMPLERENDERER_H
#define MSC_CANDELA_SIMPLERENDERER_H

#include "ConcurrentTiledRenderer.h"

namespace Candela
{
    namespace Renderer
    {
        class SimpleRenderer
            : public ConcurrentTiledRenderer
        {
        public:
            std::string toString() const override;

            void concurrentRender(Integrator::IIntegrator& threadLocalIntegrator) override;

            void onFrameRendered() override;
        };
    }
}


#endif //MSC_CANDELA_SIMPLERENDERER_H

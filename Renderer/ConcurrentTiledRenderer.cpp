//
// Created by kvnna on 03/01/2018.
//

#include "ConcurrentTiledRenderer.h"
#include "Mathematics/Funtions.h"

using namespace std;
using namespace Candela::Renderer;
using namespace Candela::Mathematics;
using namespace Candela::Integrator;
using namespace Candela::Concurrency;

size_t ConcurrentTiledRenderer::getNumTiles() const {
    return getTilesRect().getArea();
}

void ConcurrentTiledRenderer::setScene(Candela::Scene::IScene *p_scene) {
    BaseRenderer::setScene(p_scene);
    if (tileDimensions.getArea() == 0) {
        tileDimensions = Rectangle_I(0, radianceBuffer->getWidth(), radianceBuffer->getHeight(), 0);
    }
}

const Rectangle_I &ConcurrentTiledRenderer::getTileDimensions() const {
    return tileDimensions;
}

void ConcurrentTiledRenderer::setTileDimensions(const Rectangle_I& rect) {
    if (rect.getArea() > 0) {
        tileDimensions = rect;
    }
}

Rectangle_I ConcurrentTiledRenderer::getTilesRect() const {
    if (tileDimensions.getArea() == 0 || !radianceBuffer) {
        return Rectangle_I();
    }
    return Rectangle_I(0,
                       1 + (radianceBuffer->getWidth() - 1) / tileDimensions.getWidth(),
                       1 + (radianceBuffer->getHeight() - 1) / tileDimensions.getHeight(),
                       0);
}

Rectangle_I ConcurrentTiledRenderer::getTilePixelRect(size_t tileNumber) const {
    Rectangle_I tilesRect = getTilesRect();
    if (tilesRect.getArea() == 0 || tileNumber >= tilesRect.getArea()) {
        throw runtime_error("Requesting tile number which is out of range");
    }

    // Valid tile number, calculate position
    size_t xPosition = tileNumber % tilesRect.getWidth();
    size_t yPosition = tileNumber / tilesRect.getWidth();

    // Calculate the actual location of this tile onto the camera sensor plane
    return Rectangle_I(
            xPosition * getTileDimensions().getWidth(),
            Functions::min((xPosition + 1) * getTileDimensions().getWidth(), radianceBuffer->getWidth()),
            Functions::min((yPosition + 1) * getTileDimensions().getHeight(), radianceBuffer->getHeight()),
            yPosition * getTileDimensions().getHeight()
    );
}

// Threading part
void ConcurrentTiledRenderer::setNumberOfWorkers(size_t numberOfWorkers) {
    // This will call ~ThreadPool() first (if instance exists),
    // and thus will lock appropriately
    threadPool = make_unique<ThreadPool>(numberOfWorkers);
}

bool ConcurrentTiledRenderer::nextFrameRequired() const {
    return false;
}

void ConcurrentTiledRenderer::render() {
    if (threadPool) {
        size_t numOfWorkers = threadPool->getNumberOfWorkers();
        do {
            currentTileNumber.store(0, memory_order_relaxed);
            threadPool->addTasks(
                    vector<function<void()>>(
                            numOfWorkers,
                            [this] {
                                unique_ptr<IIntegrator> integratorClone = integrator->clone();
                                integratorClone->setName(integratorClone->getName() + " Thread Local Clone");
                                concurrentRender(*integratorClone);
                            }
                    )
            );
            threadPool->wait(); // wait uses mutexes, all the memory writes should be visible after wait
            onFrameRendered();

            // Report any exceptions that occurred during this render
            threadPool->throwExceptionSummary("ConcurrentTiledRenderer");
        } while (nextFrameRequired());
    } else {
        do {
            currentTileNumber.store(0, memory_order_relaxed);
            concurrentRender(*integrator);
            onFrameRendered();
        } while (nextFrameRequired());
    }
}

// Will be called by multiple threads
bool ConcurrentTiledRenderer::getNextTile(Rectangle_I &tileRect) {
    size_t tileNumber = currentTileNumber.fetch_add(1, memory_order_relaxed);
    bool workAvailable = tileNumber < getNumTiles();
    if (workAvailable) {
        tileRect = getTilePixelRect(tileNumber);
    }
    return workAvailable;
}

//
// Created by kvnna on 03/01/2018.
//

#ifndef CANDELA_CONCURRENTTILEDRENDERER_H
#define CANDELA_CONCURRENTTILEDRENDERER_H

#include <memory>
#include <atomic>
#include "BaseRenderer.h"
#include "Mathematics/Rectangle.h"
#include "Concurrency/ThreadPool.h"

namespace Candela
{
    namespace Renderer
    {
        class ConcurrentTiledRenderer
                : public BaseRenderer
        {
        public:

            void setScene(Scene::IScene * p_scene) override;
            void render() override final;

            /**
             * The number of tiles covering the image sensor
             * @return The number of tiles covering the image sensor
             */
            size_t getNumTiles() const;

            /**
             * The dimensions of a tile, in pixels
             * @return The dimensions of a tile, in pixels
             */
            const Mathematics::Rectangle_I& getTileDimensions() const;

            /**
             * Returns the Pixel rectangle mapped onto the image sensor in pixels
             * @param tileNumber The index of the tile to be mapped to the pixel plane
             * @return The rectangle, in pixels, that is mapped onto the image sensor
             */
            Mathematics::Rectangle_I getTilePixelRect(size_t tileNumber) const;

            /**
             * Set the dimensions in pixels of each tile. A tile is the basic unit of work
             * for concurrent workers. Varying this parameter will vary the granularity
             * of the rendering
             * @param tileRect The dimensions of the tile
             */
            void setTileDimensions(const Mathematics::Rectangle_I& tileRect);

            /**
             * Sets the amount of workers that will take part in the render.
             * A value of zero means using all hardware threads available on this system.
             * The input value will automatically be clamped within 1 and the hardware maximum
             * @param numberOfWorkers The number of workers involved in rendering
             */
            void setNumberOfWorkers(size_t numberOfWorkers);


        protected:

            /**
             * Called when a frame has been rendered. This call will
             * be performed by the main thread. Threads from the threadpool
             * are all idle at this point.
             */
            virtual void onFrameRendered() = 0;

            /**
             * Should return true if another concurrently rendered
             * frame is required. Should return false if rendering
             * is complete
             * @return See description
             */
            virtual bool nextFrameRequired() const;

            /**
             * If another unit of work is available, returns true
             * and fills in tileRect, otherwise returns false
             * @param tileRect The tile that the calling thread should render
             * @return True if a unit of work was assigned, false otherwise
             */
            bool getNextTile(Mathematics::Rectangle_I& tileRect);

            /**
             * All threads in this thread pool will call this function
             * in order to render a frame. Each thread must call
             * getNextTile to reserve a slot for work.
             */
            virtual void concurrentRender(Integrator::IIntegrator& threadLocalIntegrator) = 0;
        private:
            /**
             * Returns the rectangle representing the image sensor
             * in units of tiles
             * @return The rectangle representing the image sensor
             * in units of tiles
             */
            Mathematics::Rectangle_I getTilesRect() const;

            // Data
            Mathematics::Rectangle_I tileDimensions;
            std::unique_ptr<Concurrency::ThreadPool> threadPool;
            std::atomic<size_t> currentTileNumber;
        };
    }
}


#endif //CANDELA_CONCURRENTTILEDRENDERER_H

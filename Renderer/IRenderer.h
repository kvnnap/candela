//
// Created by kvnna on 03/01/2018.
//

#ifndef CANDELA_IRENDERER_H
#define CANDELA_IRENDERER_H

#include <string>
#include "Scene/IScene.h"
#include "Device/IDevice.h"
#include "Integrator/IIntegrator.h"

namespace Candela
{
    namespace Renderer
    {
        /**
         * A renderer renders a scene. Therefore:
         * 1) Requires a scene object.
         * 2) Requires an integrator, that gives back radiance for each pixel
         * 3) Requires a buffer/device to output this data on
         */
        class IRenderer {
        public:
            virtual ~IRenderer();

            virtual std::string toString() const = 0;

            virtual Scene::IScene * getScene() = 0;
            virtual const Scene::IScene * getScene() const = 0;
            virtual const Device::IDevice * getDevice() const = 0;
            virtual const Integrator::IIntegrator * getIntegrator() const = 0;

            virtual void render() = 0;
        };

        std::ostream& operator<< (std::ostream& strm, const IRenderer& renderer);
    }
}

#endif //CANDELA_IRENDERER_H

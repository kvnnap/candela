//
// Created by kvnna on 31/12/2017.
//

#include <iostream>
#include <sstream>
#include <array>
#include "VarianceBasedRenderer.h"
#include "Sampler/UniformSampler.h"
#include "Mathematics/Statistics.h"

using namespace std;
using namespace Candela::Renderer;
using namespace Candela::Mathematics;
using namespace Candela::Sampler;
using namespace Candela::Integrator;

VarianceBasedRenderer::VarianceBasedRenderer(size_t minIterationsPerPixel, size_t maxIterationsPerPixel,
                                             float stdDeviationConfidence, float confidenceInterval)
    : minIterationsPerPixel (minIterationsPerPixel),
      maxIterationsPerPixel (maxIterationsPerPixel),
      stdDeviationConfidence (stdDeviationConfidence),
      confidenceInterval (confidenceInterval),
      numberOfNonConfidentPixels(0)
{}

std::string VarianceBasedRenderer::toString() const {
    stringstream sstr;
    sstr << BaseRenderer::toString()
         << " - VarianceBasedRenderer" << endl
         << "\tMinimum Iterations Per Pixel: " << minIterationsPerPixel << endl
         << "\tMaximum Iterations Per Pixel: " << maxIterationsPerPixel << endl
         << "\tStandard Confidence Deviation (2.0 is around 95%): " << stdDeviationConfidence<< endl
         << "\tConfidence Interval Value: " << confidenceInterval << endl
            ;
    return sstr.str();
}

void VarianceBasedRenderer::concurrentRender(IIntegrator &threadLocalIntegrator) {
    BoundedRay primaryRay;
    UniformSampler sampler;

    array<vector<float>, 3> pixelSamples;
    size_t numNotConfident = 0;

    Rectangle_I tileRect;

    while(getNextTile(tileRect)) {
        for (size_t y = tileRect.top; y < tileRect.bottom; ++y) {
            for (size_t x = tileRect.left; x < tileRect.right; ++x) {
                // Get Radiance Content
                RadianceContent &radianceContent = (*radianceBuffer)(x, y);
                Vector &radVector = radianceContent.radiance.getAbcReference();

                bool confident = false;
                size_t minNumIterations = minIterationsPerPixel;
                for (size_t i = 0; (!confident && (i < maxIterationsPerPixel)) || (i < minIterationsPerPixel); ++i) {

                    // compute ray using Camera
                    scene->getCamera()->getRayPixel(x, y,
                                                    sampler.nextSample(), sampler.nextSample(),
                                                    sampler.nextSample(), sampler.nextSample(),
                                                    primaryRay
                    );

                    // Feed into integrator
                    threadLocalIntegrator.radiance(radianceContent, *scene, primaryRay);

                    // Fill pixel values
                    pixelSamples[0].push_back(radVector.xyz[0]);
                    pixelSamples[1].push_back(radVector.xyz[1]);
                    pixelSamples[2].push_back(radVector.xyz[2]);

                    if (i >= minNumIterations) {
                        minNumIterations *= 2;
                        confident = true;
                        for (auto &pixelSample : pixelSamples) {
                            if (!confident) {
                                break;
                            }
                            const float sampleStdDeviation = Statistics::SampleStdDeviation(pixelSample);
                            const float stdDeviationOfTheMean = Statistics::StdDeviationOfTheMean(sampleStdDeviation,
                                                                                                  pixelSample.size());
                            const float confidenceInterval = stdDeviationOfTheMean * stdDeviationConfidence;
                            confident &= confidenceInterval <= this->confidenceInterval;
                        }
                    }
                }

                // Confident or reached maximum
                radVector.xyz[0] = Statistics::Mean(pixelSamples[0]);
                radVector.xyz[1] = Statistics::Mean(pixelSamples[1]);
                radVector.xyz[2] = Statistics::Mean(pixelSamples[2]);

                pixelSamples[0].clear();
                pixelSamples[1].clear();
                pixelSamples[2].clear();

                if (!confident) {
                    ++numNotConfident;
                }
            }
        }
    }

    // Add statistics
    numberOfNonConfidentPixels.fetch_add(numNotConfident, memory_order_relaxed);
}

void VarianceBasedRenderer::onFrameRendered() {
    size_t numNotConfident = numberOfNonConfidentPixels.exchange(0, memory_order_relaxed);
    cout << "Number of pixels not reaching the required confidence interval: " << numNotConfident
         << ", " << (100 * static_cast<double>(numNotConfident) / (radianceBuffer->getWidth() * radianceBuffer->getHeight()))
         << "%" << endl;

    device->write(*radianceBuffer, RadianceContent::FINAL);
}

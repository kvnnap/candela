//
// Created by kvnna on 23/06/2016.
//

#include <sstream>
#include "SimpleRenderer.h"

using namespace std;
using namespace Candela::Renderer;
using namespace Candela::Mathematics;
using namespace Candela::Integrator;

void SimpleRenderer::concurrentRender(IIntegrator& threadLocalIntegrator) {
    // Data here for thread locality
    BoundedRay primaryRay;
    Rectangle_I tileRect;

    while (getNextTile(tileRect)) {
        for (size_t y = tileRect.top; y < tileRect.bottom; ++y) {
            for (size_t x = tileRect.left; x < tileRect.right; ++x) {
                // compute ray using Camera
                scene->getCamera()->getRayPixel(x, y, primaryRay);

                // Get Radiance Content
                RadianceContent &radianceContent = (*radianceBuffer)(x, y);

                // Feed into integrator
                threadLocalIntegrator.radiance(radianceContent, *scene, primaryRay);
            }
        }
    }
}

void SimpleRenderer::onFrameRendered() {
    device->write(*radianceBuffer, RadianceContent::FINAL);
}

string SimpleRenderer::toString() const {
    ostringstream sstr;
    sstr << BaseRenderer::toString()
         << " - SimpleRenderer";
    return sstr.str();
}

//
// Created by kvnna on 03/01/2018.
//

#include "IRenderer.h"

using namespace std;
using namespace Candela::Renderer;

IRenderer::~IRenderer() {}

ostream& Candela::Renderer::operator <<(ostream& strm, const IRenderer& renderer)
{
    return strm << renderer.toString();
}

//
// Created by kvnna on 25/06/2016.
//

#include "Buffer2D.h"

using namespace std;
using namespace Candela::Renderer;


template <class T>
Buffer2D<T>::~Buffer2D() { }

template <class T>
Buffer2D<T>::Buffer2D()
        : Buffer2D(0, 0)
{ }

template <class T>
Buffer2D<T>::Buffer2D(size_t width, size_t height)
    : width (width), height (height), buffer(width * height)
{ }

//template <class T>
//Buffer2D<T>::Buffer2D(Buffer2D && other)
//    : width ( other.width ), height ( other.height ), buffer ( move(other.buffer) )
//{
//
//}

template <class T>
size_t Buffer2D<T>::getWidth() const {
    return width;
}

template <class T>
size_t Buffer2D<T>::getHeight() const {
    return height;
}

template <class T>
T &Buffer2D<T>::operator()(size_t x, size_t y) {
    return buffer[getIndex(x, y)];
}

template <class T>
const T& Buffer2D<T>::operator()(size_t x, size_t y) const {
    return buffer[getIndex(x, y)];
}

template <class T>
size_t Buffer2D<T>::getIndex(size_t x, size_t y) const {
    return y * width + x;
}

template <class T>
Buffer2D<T>& Buffer2D<T>::operator=(Buffer2D &&other) {
    width = other.width;
    height = other.height;
    buffer = move(other.buffer);
    return *this;
}

template <class T>
void Buffer2D<T>::clearAndReset(size_t p_width, size_t p_height) {
    width = p_width;
    height = p_height;
    buffer = vector<T>(width * height);
}


#include "RadianceBuffer.h"
template class Buffer2D<RadianceContent>;
#include "Spectrum/RgbSpectrum.h"
template class Buffer2D<Candela::Spectrum::RgbSpectrum>;






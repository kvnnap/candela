//
// Created by kvnna on 25/06/2016.
//

#ifndef MSC_CANDELA_RADIANCEBUFFER_H
#define MSC_CANDELA_RADIANCEBUFFER_H

#include <memory>
#include <Spectrum/RgbSpectrum.h>
#include "Spectrum/ISpectrum.h"
#include "Buffer2D.h"

namespace Candela
{
    namespace Renderer
    {
        class RadianceContent {

        public:

            enum RadianceType {
                DIRECT,
                INDIRECT,
                FINAL
            };

            RadianceContent();

            RadianceContent(RadianceContent&& other);
            RadianceContent& operator=(RadianceContent&& other);

//            Spectrum::RgbSpectrum direct;
//            Spectrum::RgbSpectrum indirect;
            Spectrum::RgbSpectrum radiance;
        };

        class RadianceBuffer
            : public Buffer2D<RadianceContent>
        {
        public:
            RadianceBuffer(size_t width, size_t height);
//            RadianceBuffer(RadianceBuffer && other);
            RadianceBuffer& operator=(RadianceBuffer&& other);
        };
    }
}



#endif //MSC_CANDELA_RADIANCEBUFFER_H

//
// Created by kvnna on 28/06/2016.
//

#include <iostream>
#include <sstream>
#include "ProgressiveRenderer.h"
#include "Sampler/UniformSampler.h"

using namespace std;
using namespace Candela::Renderer;
using namespace Candela::Mathematics;
using namespace Candela::Spectrum;
using namespace Candela::Sampler;
using namespace Candela::Integrator;

ProgressiveRenderer::ProgressiveRenderer()
        : iterations (1), currentIteration(0)
{ }

void ProgressiveRenderer::concurrentRender(IIntegrator& threadLocalIntegrator) {
    BoundedRay primaryRay;
    UniformSampler sampler;

    Rectangle_I tileRect;

    while(getNextTile(tileRect)) {
        for (size_t y = tileRect.top; y < tileRect.bottom; ++y) {
            for (size_t x = tileRect.left; x < tileRect.right; ++x) {
                // compute ray using Camera
                scene->getCamera()->getRayPixel(x, y,
                                                sampler.nextSample(), sampler.nextSample(),
                                                sampler.nextSample(), sampler.nextSample(),
                                                primaryRay
                );

                // Get Radiance Content
                RadianceContent &radianceContent = (*radianceBuffer)(x, y);

                // Save previous value so that we can accumulate
//                RgbSpectrum rgbDirect(radianceContent.direct);
//                RgbSpectrum rgbIndirect(radianceContent.indirect);
                RgbSpectrum rgbFinal(radianceContent.radiance);

                // Feed into integrator
                threadLocalIntegrator.radiance(radianceContent, *scene, primaryRay);

//                radianceContent.direct += rgbDirect;
//                radianceContent.indirect += rgbIndirect;
                radianceContent.radiance += rgbFinal;
            }
        }
    }
}

// Called by main thread
void ProgressiveRenderer::onFrameRendered() {
    // temp buffer
    unique_ptr<RadianceBuffer> tempRad = make_unique<RadianceBuffer>(radianceBuffer->getWidth(), radianceBuffer->getHeight());

    for (size_t y = 0; y < radianceBuffer->getHeight(); ++y) {
        for (size_t x = 0; x < radianceBuffer->getWidth(); ++x) {
//            (*radianceBuffer)(x, y).direct.divide(currentIteration + 1, (*tempRad)(x, y).direct);
//            (*radianceBuffer)(x, y).indirect.divide(currentIteration + 1, (*tempRad)(x, y).indirect);
            (*radianceBuffer)(x, y).radiance.divide(currentIteration + 1, (*tempRad)(x, y).radiance);
        }
    }
    device->write(*tempRad, RadianceContent::FINAL);
//    device->write(*radianceBuffer, RadianceContent::DIRECT);
//    device->write(*radianceBuffer, RadianceContent::INDIRECT);
    cout << (currentIteration + 1) << endl;
    ++currentIteration;
}

// Called by main thread
bool ProgressiveRenderer::nextFrameRequired() const {
    return currentIteration < iterations;
}

void ProgressiveRenderer::setIterations(size_t p_iterations) {
    iterations = p_iterations;
}

size_t ProgressiveRenderer::getIterations() const {
    return iterations;
}

string ProgressiveRenderer::toString() const {
    ostringstream sstr;
    sstr << BaseRenderer::toString()
         << " - ProgressiveRenderer:"
         << "Iterations: " << iterations;
    return sstr.str();
}
